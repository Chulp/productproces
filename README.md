# gsmoffm  / Productproces

 Lepton base for product handling as part of the gsmoff set of applications.

This document is relevant for `gsmoffm` / `Productproces` version 1.01.0 and up.

## Download
The released stable `gsmoffm` / `Productproces` application is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
 `gsmoffm` / `Productproces` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

### You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
for any purpose, even commercially. 

### Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

## Pre-conditions
The minimum requirements to get this application running on your LEPTON installation are as follows:

- LEPTON *** 7.0 *** of higher depending on the version
- the module uses Twig support
- the module uses Fomantic
- the taxonomy module installed.

The module is tested in combination with the Office Tegel template

## Installation

This description assumes you have a standard installation at the at least at level Lepton 7.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupc/instellingen and optionally activate one of the following functions where needed 

  * IMAGE 	image directory copied from frontend
  * LOGGING 	empty the logging
  * DETAIL 	display detailed data
  * INSTALL 	frontend files are created: customized values are overwritten bij default values !!
or enter ? also for not (yet) documented functions

The function can be started by selecting d_...._ where ... is the function name. A number of the function names can be concatenated.

The system will automatically install the file upon the first use of this module. 

### application area's

The application areas are to be set properly in the templates_Dir/frontend/gsmoffm/SET.php file !!

The modules in this package cover the following application area's

	- Articles with 
		+ prodct groups
		+ article information
		+ prices
		+ stock
		+ assortiment display
		+ ordering
		+ invoicing	
		
		modules setupm (to setup / ),
			group,
			product,
			prices, 
			stock,
			bestel,
			overzicht
			and some droplets
			and the class gsmofft_pro
		
	- Events calendar
		+ Events registration
		+ week display
		+ month display
		+ next n events display
		
		modules setupm (to setup), 
			events
			and some droplets
			and the class gsmofft_kal
		
	- address (social card type)
		+ Address registration
		+ Group display
		+ Search on keywords
		
		modules setupm (to setup), 
			social
			and some droplets
			and the class gsmofft_soc
	

### the backend functions

 - setupm / instellingen	to setup and manitenace of the data
 - group|product|prices|stock|bestel|overzicht|events|social';
 
  Depending on the contents of the SET file modules are made available. Correct access rights and the template's SET file is to contain selected entries to get the desired functionality

### frontend menu

 - 'group|product|prices|stock|overzicht|events|social|zoek'; 

 Depending on the contents of the SET file modules are made available. Correct access rights and the template's SET file is to contain selected entries to get the desired functionality

  
### frontend menu  SET_menu

/* if not logged in */
$FC_SET [ 'SET_function' ] 	= 'setupm|dummy'; 		// backend menu ';
if ( $section_id == 15 ) { 
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
} elseif ( $section_id == 22 ) {	
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
} else { 
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
}

/* for the administrator and the editor */
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
//	$FC_SET [ 'SET_function' ] 		= 'setupm|group|product|prices|stock|bestel|overzicht|events|social';
//	$FC_SET [ 'SET_menu' ] 			= 'group|product|prices|stock|overzicht|events|social|zoek'; 
	if ( $section_id == 19 ) { 
		$FC_SET [ 'SET_menu' ] 		= 'product|prices|group|stock|overzicht'; 	
	} elseif ( $section_id == 60 ) {	
		$FC_SET [ 'SET_menu' ] 		= 'events';  // frontend menu ';
		$FC_SET [ 'SET_function' ] 	= 'events';
	} elseif ( $section_id == 71 ) {	
		$FC_SET [ 'SET_menu' ] 		= 'social';  // frontend menu ';
		$FC_SET [ 'SET_function' ] 	= 'social';	
	} else { 
		$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';	 
		$FC_SET [ 'SET_function' ] 	= 'setupm|dummy';
	
	}	
}

/* ------------------------------------
 * adapt in above section ID to a suitable value
 * -----------------------------------*/

// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupm' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';

$FC_SET [ 'SET_txt_menu' ] [ 'xproduct' ]	= 'Assortiment';
$FC_SET [ 'SET_txt_menu' ] [ 'vproduct' ]	= 'Assortiment';

$FC_SET [ 'SET_txt_menu' ] [ 'xgroup' ]		= 'Artikel groepen';
$FC_SET [ 'SET_txt_menu' ] [ 'vgroup' ]		= 'Artikel groepen';

$FC_SET [ 'SET_txt_menu' ] [ 'xprices' ]	= 'Prijzen';
$FC_SET [ 'SET_txt_menu' ] [ 'vprices' ]	= 'Prijzen';

$FC_SET [ 'SET_txt_menu' ] [ 'xstock' ]		= 'Voorraad';
$FC_SET [ 'SET_txt_menu' ] [ 'vstock' ]		= 'Voorraad';

$FC_SET [ 'SET_txt_menu' ] [ 'xevents' ]	= 'Evenementen';
$FC_SET [ 'SET_txt_menu' ] [ 'vevents' ]	= 'Evenementen';

$FC_SET [ 'SET_txt_menu' ] [ 'xagenda' ]	= 'Agenda';
$FC_SET [ 'SET_txt_menu' ] [ 'vagenda' ]	= 'Agenda';

$FC_SET [ 'SET_txt_menu' ] [ 'xoverzicht' ]	= 'Periode Overzicht';
$FC_SET [ 'SET_txt_menu' ] [ 'voverzicht' ]	= 'Periode Overzicht';

$FC_SET [ 'SET_txt_menu' ] [ 'xbestel' ]	= 'Facturering';
$FC_SET [ 'SET_txt_menu' ] [ 'vbestel' ]	= 'Facturering';

$FC_SET [ 'SET_txt_menu' ] [ 'xsocial' ]	= 'Sociale kaart';
$FC_SET [ 'SET_txt_menu' ] [ 'vscocial' ]	= 'Sociale kaart';

//$FC_SET [ 'SET_txt_menu' ] [ 'vzoek' ]	= 'Adres zoeken';


### SET_function

The set function arranges which functions will be displayed on a front end and which will be displayed on the backend page
standard set up 
- menu
initial default often dummy (backend install is needed to change )
- functions (backend)
initial default often setupm|dummy

setupm/instellingen with D_INSTALL_ will create a frontend file in the template directory with different values 
The frontend SET file gives hints what options are possible. 
Parameters which can be used to determine the functions:
- section_id
- usergroups: the standard setup assumes that GROUPS_ID == 1 ( administrator ) and GROUPS_ID == 4 ( assisten editor ) get additional functionality but this can changed
The mentioned functions if not present will default to dummy
 

## The functions

- install / setupm - installation functions. This function is to install or reinstall the required functions as well as the database tables rquired for the version. 
- xgroep : maintenance of product goups

### install / setupm function
This function creates the required tables or updates the required table to contain the minimum functions. The function does not modify the fontend files. 
The SET file is to be adapted or the frontend SET file is to be installed and modified where needed.
use D_install_ to (re-) install the front end settings directory with initial contents.

Predefinced ( demo ) content for some of the tables is contained in the main directory. 
	- setup_mod_go_group.php
	- setup_mod_go_product.php
	- setup_mod_go_proces.php
	- setup_mod_go_events.php
	- setup_mod_go_agenda.php	

n.b. Some entries are added to the taxonomy table

### xgroup / vgroup
maintenance of product goups
function: grouping of products 
features : vat% is defined per group (so every group has only one VAT percentage )
active: 4 levels: not active ( fading out / memorial ), active ( current product ), extra ( action article ), lease ( verhuur ) 
description and illustratio are for documentation use.
note: keys are made unique by adding a one or more full stops (.)

print function parameters all and or debug. Others are a selection in the field zoekstring 



### xproduct / vproduct
maintenance of product description
function: maintenance of product description and details excluding pricing.
active: 4 levels: not active ( fading out / memorial ), active ( current product ), extra ( action article ), lease ( verhuur ) 
note: keys are made unique by adding a one or more full stops (.)

print function parameters all and or debug. Others are a selection in the field zoekstring 

### xprices / vprices
maintenance of product prices
function: maintenance of product prices (various price columns).
prices: costprice (ex vat), sales price ( excl vat ), recommended list price (incl vat) 
note: vat% comes from the product group.

print function parameters all and or debug. Others are a selection in the field zoekstring 

### xstock / vstock
maintenance of product stock
function: maintenance of product stock

print function parameters all and or debug. Others are a selection in the field zoekstring 

### xzoek / vzoek
Zoek sociale gegevens
function: maintenance of product stock

### xocial / vsocial
Onderhoud adres gegevens
Referenties to wijken en vakgebieden (Taxonomy swijk:  swijk KA Kerkdorp Acht and sgroep: sgroep 	G001 	Alarmnummers)
The defaults for wijk is AA the default for groep is determined by  taxonomy setting (groep 	G020 description) G020 is an example

### vbestel
handling of the making the invoice an the other steps in the primary process.

case 1 access without rights
precondition some products are selected and shoppung chart is filled
This results in a order button to appear.
The link in the mail is the (only) access to the status next to follow-up mails. 

case 2 access with editor rights
access to not completed orders with the possibilities to change the article lines, quantity and the prices
status of the order  can be controled

case 3 access with limited rights
same features as case 1 however the all not completed orders can be accessed

case 4 / 5 / 6
precondition some products are selected and shoppung chart is filled
This results in a order button to appear. depending on the button cash payment, bnk payment or  invoicing is made easy
possibilities are available to change the article lines, quantity and the prices
error condition : access problems process file: => remedy: initialise database

## the display of the products 
see the gsmoffn module and the shopping chart module;
and the relevant droplets

The result is a file in the indicated (logging) directory  
```html
Logging directory. 
subdirectory date
subdirectory session_id
filename bestel.html
entries
XX|1058|Bavaria Pils 4x6/33 Blik|1|21.11|0.00|2024-05-03 00:35|user:1
XX|1080|Jupiler Pils 4x6/33 Blik|1|19.9|0.00|2024-05-03 00:36|user:1
XX|1115|Amstel Pils 4x6/33 Blik|1|22.18|0.00|2024-05-03 00:36|user:1
XX|1116|Brand Pils 4x6/33 Blik|1|21.11|0.00|2024-05-03 00:36|user:1
XX|1080|Jupiler Pils 4x6/33 Blik|1|19.9|0.00|2024-05-03 00:36|user:1
XX|1115|Amstel Pils 4x6/33 Blik|1|22.18|0.00|2024-05-03 00:36|user:1
 where 
XX| just fixed
--|1115| article number 
-------|Amstel Pils 4x6/33 Blik|  article description 
-------------------------------|1| quantity  ( records add up  )
---------------------------------|22.18| price 
---------------------------------------|0.00| emballage 
--------------------------------------------|2024-05-03 00:36| time stamp 
-------------------------------------------------------------|user:1 user id added
```
## Droplets

The the following droplets are relevant.
Some of these droplets require the taxonomy class: gsmofft_pro.php 


### The droplet Gsm_product

Provuides the display of a product group
usage [[Gsm_product?data=1&display=1&message=assortiment groep1 display1]]

 - data = 1  		(array) type to be displayed
 - display = 1 		display mode (delivered 1,2,3,4 and 99 (cheat sheet)
 - active = 1		(array) 0,1,2,3
 - log = no			(array) 0,1,2,3,4,5 yes | no
 - set = type	 	interpretation of data type  | ref
 - sort = zoek  	sort on the field zoek  | ref | name
 - seq = 1  		sorting sequence 1= ASC 2= DESC  no
 - message = -  	table tekst
 
 Illustration can be provided for the product 

examples

[[Gsm_product?data=10&display=1&active=0,1,2&message=assortiment groep10 display1]]
[[Gsm_product?data=10&display=2&active=1,2&message=assortiment groep10 display2]]
[[Gsm_product?data=grolsch,heineken&set=zoek&display=4&active=0,1,2&message=assortiment groep merk display3]]
[[Gsm_product?data=huur&set=zoek&display=99&active=3&message=assortiment huur display99]]
[[Gsm_product?data=23&display=3&active=3&message=groep23 display3]]
[[Gsm_product?data=10,23&display=4&active=3&message=groep23 display4]]

 display
 - 1 one line per product with all the fields
 - 2 simple one column assortiments list
 - 3 simple two column assortiments list
 - 4 assortiments list using one tile per product 
 - 99 cheat mode showing all the fields available 
 
 the display templates are included in the gsmofft module (templates) . you can use the exiting templates as a base for the reuired layout 

### The droplet Gsm_ordercollect

Display of the shopping chart. 

Usage [[Gsm_ordercollect?mode=3$message=voorbeeld]] 

parameters
 - mode 
	- 1 login alleen met prijzen en totaal
	- 2 met prijzen en totaal
	- 3 zonder prijzen en totaal
 
 - Message
	
	header text. default = Winkel wagen


Note: the orders are collected and maintaned as session data (few hours valid) unless the data is "ordered / besteld"	 

other parameters for this set of droplets droplets and the taxonomy class: gsmofft_pro.php 
fileref=artikel
filename=bestel
dirname=media/logging

 
### The droplet Gsm_order

Create a button to place a product in the shopping chart when selected e.g as part of the running text

Usage [[Gsm_order?artikel=]]  

Variable parameters
 - artikel=--  	// artikel number
 - name=--		// artikel name
 - prijs =0		// artikel price
 - statiegeld=0	// deposit

Specific stable paramters for this droplet
 - product=product

### The droplet Gsm_bestel

Transferring a shopping car to an order, wih the act of invoicing with cashpayment or banktransfer.

Usage [[Gsm_bestel?section=22&page=afrekenen&command=bet0]]

Basically this function jumps to a module vbestel which picks up the shopping chart / winkelwagen

example
[[Gsm_bestel?section=22&command=bet0]]
[[Gsm_bestel?section=22&command=bet1]]
[[Gsm_bestel?section=22&command=bet2]]
[[Gsm_bestel?section=22&command=bet3]]

when you use all of the above all options are available

parmeters

 - command ( this refers to the payment method.
	- command=bet0 // self invoicing function (default)
	- command=bet1 // cash payment 
	- command=bet2 // bank transfer e.g. pin transaction
	- command=bet3 // invoicing

 - page	= afrekenen 	// the page name to go to. adapt this in the dropletcall when the pagename is different
 - section = 22			// the section id to go to

Specific stable paramters for this droplet

module=vbestel; 

Note. in the officeTegel template template you may activate this droplet
ensure that the default value in the droplet is adapted or the template. 

Note. when not logged in only command bet0 is functional.

When the set file is not adpted with the correct page_name you will get a dummy module showing the section_id
example assuming section 22 
	} elseif ( $section_id == 22 ) {	
		$FC_SET [ 'SET_menu' ] 		= 'bestel';  // frontend menu ';

## Error messages
 * When started for the first time an error message :*Oeps system not initialised and/or empty database* can be seen.
 The database tables are to be initialised:-> backend -> setup
 The menusystem is to be installed: -> backend -> setup with parameter d_install detailed_
 Next to that the SET file is to be adapted.