<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
//-------------------------------------------
// Pdf generator definitions
//-------------------------------------------
$pdfclass = ( dirname( __FILE__ ) ) . '/class.fpdf.php';
require_once( $pdfclass );
class PDF extends Fpdf {
	function LabelBlock( $data, $cols = array( 5, 42, 79, 116, 153, 190, 227, 264 ) , $rows = array( 5, 73, 142 )  ) {
		$this->SetAutoPageBreak ( 0, 5 );
		foreach ( $data as $row ) {
			// start Position
			$label24 = intval ( $row [ 0 ] % 24 );
			$labelV = intval ( $label24 / 3);
			$labelH = intval ( $label24 % 3);
			$PY = $cols [ $labelV ];
			$PX = $rows [ $labelH ];
			// Label data
			$this->SetXY ( $PX, $PY );
			$this->SetFont( 'Arial', 'B', 10 );
			$this->MultiCell ( 200-$PX, 5, $row [ 1 ] );
			if (isset ( $row [ 2 ] ) ) {
				$this->SetFont( 'Arial', 'B', 11 );
				$PY = $PY +5;
				$this->SetXY( $PX, $PY );
				$this->MultiCell( 60, 6, $row [ 2 ], 0, "R" );
				$PY = $PY + 1;
			}
			$this->SetFont( 'Arial', '', 10 );
			if (isset ( $row [ 3 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 3 ] );
			}
			if (isset ( $row [ 4 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 4 ] );
			}
			if (isset ( $row [ 5 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 5 ], 0, "R" );
			}
		} 	//$data as $row
	}
	function LabelVeel( $data, $cols = array( 5, 42, 79, 116, 153, 190, 227, 264 ) , $rows = array( 5, 73, 142 )  ) {
		$this->SetAutoPageBreak ( 0, 5 );
		foreach ( $data as $row ) {
			// start Position
			$label24 = intval ( $row [ 0 ] % 24 );
			$labelV = intval ( $label24 / 3);
			$labelH = intval ( $label24 % 3);
			$PY = $cols [ $labelV ];
			$PX = $rows [ $labelH ];
			// Label data
			$this->SetXY ( $PX, $PY );
			$this->SetFont( 'Arial', '', 10 );
			$this->MultiCell ( 200-$PX, 5, $row [ 1 ] );
			if (isset ( $row [ 2 ] ) ) {
				$PY = $PY +5;
				$this->SetXY( $PX, $PY );
				$this->MultiCell( 60, 6, $row [ 2 ] );
				$PY = $PY + 1;
			}
			if (isset ( $row [ 3 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 3 ] );
			}
			if (isset ( $row [ 4 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 4 ] );
			}
			if (isset ( $row [ 5 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 5 ] );
			}
		} 	//$data as $row
	}
	function LabelCenter( $data, $cols = array( 5, 42, 79, 116, 153, 190, 227, 264 ) , $rows = array( 5, 73, 142 )  ) {
		$this->SetAutoPageBreak ( 0, 5 );
		foreach ( $data as $row ) {
			// start Position
			$label24 = intval ( $row [ 0 ] % 24 );
			$labelV = intval ( $label24 / 3);
			$labelH = intval ( $label24 % 3);
			$PY = $cols [ $labelV ];
			$PX = $rows [ $labelH ];
			// Label data
			$this->SetXY ( $PX, $PY );
			$this->SetFont( 'Arial', 'B', 10 );
			$this->MultiCell ( 60, 5, $row [ 1 ], 0, "C" );
			if (isset ( $row [ 2 ] ) ) {
				$this->SetFont( 'Arial', 'B', 11 );
				$PY = $PY +5;
				$this->SetXY( $PX, $PY );
				$this->MultiCell( 60, 6, $row [ 2 ], 0, "C" );
				$PY = $PY + 1;
			}
			$this->SetFont( 'Arial', '', 10 );
			if (isset ( $row [ 3 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 3 ], 0, "C" );
			}
			if (isset ( $row [ 4 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 4 ], 0, "C" );
			}
			if (isset ( $row [ 5 ] ) ) {
				$PY = $PY +5;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 5 ], 0, "C" );
			}
		} 	//$data as $row
	}
		function LabelGroot( $data, $cols = array( 5, 42, 79, 116, 153, 190, 227, 264 ) , $rows = array( 5, 73, 142 )  ) {
		$this->SetAutoPageBreak ( 0, 5 );
		foreach ( $data as $row ) {
			// start Position
			$label24 = intval ( $row [ 0 ] % 24 );
			$labelV = intval ( $label24 / 3);
			$labelH = intval ( $label24 % 3);
			$PY = $cols [ $labelV ];
			$PX = $rows [ $labelH ];
			// Label data
			$this->SetXY ( $PX, $PY );
			$this->SetFont( 'Arial', 'B', 10 );
			$this->MultiCell ( 60, 5, $row [ 1 ], 0, "C" );
			if (isset ( $row [ 2 ] ) ) {
				$this->SetFont( 'Arial', 'B', 16 );
				$PY = $PY +5;
				$this->SetXY( $PX, $PY );
				$this->MultiCell( 60, 6, $row [ 2 ], 0, "C" );
				$PY = $PY + 3;
			}
			$this->SetFont( 'Arial', '', 10 );
			if (isset ( $row [ 3 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 3 ], 0, "C" );
			}
			if (isset ( $row [ 4 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 4 ], 0, "C" );
			}
			if (isset ( $row [ 5 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 60, 5, $row [ 5 ], 0, "C" );
			}
		} 	//$data as $row
	}
	function LabelAll( $data, $cols = array( 5, 42, 79, 116, 153, 190, 227, 264 ) , $rows = array( 5, 73, 142 )  ) {
		$this->SetAutoPageBreak ( 0, 5 );
		foreach ( $data as $row ) {
			// start Position
			$label24 = intval ( $row [ 0 ] % 24 );
			$labelV = intval ( $label24 / 3);
			$labelH = intval ( $label24 % 3);
			$PY = $cols [ $labelV ];
			$PX = $rows [ $labelH ];
			// Label data
			$this->SetXY ( $PX, $PY );
			$this->SetFont( 'Arial', 'B', 10 );
			$this->MultiCell ( 200-$PX, 5, $row [ 1 ]);
			if (isset ( $row [ 2 ] ) ) {
				$this->SetFont( 'Arial', 'B', 16 );
				$PY = $PY +5;
				$this->SetXY( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 2 ] );
				$PY = $PY + 3;
			}
			$this->SetFont( 'Arial', '', 16 );
			if (isset ( $row [ 3 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 10, $row [ 3 ] );
			}
			$this->SetFont( 'Arial', '', 12 );
			if (isset ( $row [ 4 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 4 ] );
			}
			if (isset ( $row [ 5 ] ) ) {
				$PY = $PY +4;
				$this->SetXY ( $PX, $PY );
				$this->MultiCell( 200-$PX, 5, $row [ 5 ] );
			}
		} 	//$data as $row
	}
}