<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$FC_SET [ 'version' ] 		= "frontend 20240828";

/* ------------------------------------
 * adapt in below section ID to a suitable value
 * -----------------------------------*/

/* if not logged in */
$FC_SET [ 'SET_function' ] 	= 'setupm|dummy'; 		// backend menu ';
if ( $section_id == 15 ) { 
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
} elseif ( $section_id == 22 ) {	
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
} else { 
	$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';
}


/* for the administrator and the editor */
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
	$FC_SET [ 'SET_function' ] 		= 'setupm|dummy';
	$FC_SET [ 'SET_menu' ] 			= 'dummy';	
//	$FC_SET [ 'SET_function' ] 		= 'setupm|group|product|prices|stock|bestel|overzicht|events|social';
//	$FC_SET [ 'SET_menu' ] 			= 'group|product|prices|stock|overzicht|events|social|zoek'; 
	if ( $section_id == 59 ) { 
		$FC_SET [ 'SET_menu' ] 		= 'product|prices|group|stock|overzicht|events|social'; 	
	} elseif ( $section_id == 60 ) {	
		$FC_SET [ 'SET_menu' ] 		= 'events';  // frontend menu ';
		$FC_SET [ 'SET_function' ] 	= 'events';
	} elseif ( $section_id == 71 ) {	
		$FC_SET [ 'SET_menu' ] 		= 'social';  // frontend menu ';
		$FC_SET [ 'SET_function' ] 	= 'social';	
	} else { 
		$FC_SET [ 'SET_menu' ] 		= 'dummy';  // frontend menu ';	 
		$FC_SET [ 'SET_function' ] 	= 'setupm|dummy';
	
	}	
}

/* ------------------------------------
 * adapt in above section ID to a suitable value
 * -----------------------------------*/

// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupm' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';

$FC_SET [ 'SET_txt_menu' ] [ 'xproduct' ]	= 'Assortiment';
$FC_SET [ 'SET_txt_menu' ] [ 'vproduct' ]	= 'Assortiment';

$FC_SET [ 'SET_txt_menu' ] [ 'xgroup' ]		= 'Artikel groepen';
$FC_SET [ 'SET_txt_menu' ] [ 'vgroup' ]		= 'Artikel groepen';

$FC_SET [ 'SET_txt_menu' ] [ 'xprices' ]	= 'Prijzen';
$FC_SET [ 'SET_txt_menu' ] [ 'vprices' ]	= 'Prijzen';

$FC_SET [ 'SET_txt_menu' ] [ 'xstock' ]		= 'Voorraad';
$FC_SET [ 'SET_txt_menu' ] [ 'vstock' ]		= 'Voorraad';

$FC_SET [ 'SET_txt_menu' ] [ 'xevents' ]	= 'Evenementen';
$FC_SET [ 'SET_txt_menu' ] [ 'vevents' ]	= 'Evenementen';

$FC_SET [ 'SET_txt_menu' ] [ 'xagenda' ]	= 'Agenda';
$FC_SET [ 'SET_txt_menu' ] [ 'vagenda' ]	= 'Agenda';

$FC_SET [ 'SET_txt_menu' ] [ 'xoverzicht' ]	= 'Periode Overzicht';
$FC_SET [ 'SET_txt_menu' ] [ 'voverzicht' ]	= 'Periode Overzicht';

$FC_SET [ 'SET_txt_menu' ] [ 'xbestel' ]	= 'Facturering';
$FC_SET [ 'SET_txt_menu' ] [ 'vbestel' ]	= 'Facturering';

$FC_SET [ 'SET_txt_menu' ] [ 'xsocial' ]	= 'Sociale kaart';
$FC_SET [ 'SET_txt_menu' ] [ 'vsocial' ]	= 'Sociale kaart';
$FC_SET [ 'SET_txt_menu' ] [ 'vzoek' ]	= 'Adres zoeken';

?>