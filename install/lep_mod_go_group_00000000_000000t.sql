# 
# Office tools  Database Backup
# Run 20240428_103456
# 

# Drop table lep_mod_go_group if exists
DROP TABLE IF EXISTS `lep_mod_go_group`;
# Create table lep_mod_go_group

CREATE TABLE `lep_mod_go_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(63) NOT NULL DEFAULT '',
  `ref` varchar(63) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `content_short` varchar(255) NOT NULL DEFAULT '',
  `amt1` decimal(9,2) NOT NULL DEFAULT 0.00,
  `zoek` varchar(255) NOT NULL DEFAULT '',
  `active` int(3) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=COMPACT;

# Dump data for lep_mod_go_group

INSERT INTO lep_mod_go_group SET `id`='1',`type`='bier',`ref`='bier in fles',`name`='Bier in fles',`content_short`='',`amt1`='21.00',`zoek`='bierbier in fles',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='2',`type`='bier',`ref`='bier in blik',`name`='Bier in blik',`content_short`='',`amt1`='21.00',`zoek`='bierbier in blik',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='3',`type`='bier',`ref`='bier op fust',`name`='Bier op fust',`content_short`='',`amt1`='21.00',`zoek`='bierbier op fust',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='4',`type`='bier',`ref`='bier alcohol vry',`name`='Bier alcohol vrij',`content_short`='',`amt1`='9.00',`zoek`='bierbier alcohol vry',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='5',`type`='frisdrank',`ref`='frisdrank in blik',`name`='Frisdrank in blik en petfles',`content_short`='',`amt1`='9.00',`zoek`='frisdrankfrisdrank in blik',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='6',`type`='frisdrank',`ref`='frisdrank in fles',`name`='Frisdrank in fles',`content_short`='',`amt1`='9.00',`zoek`='frisdrankfrisdrank in fles',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='7',`type`='frisdrank',`ref`='mineraalwater',`name`='Mineraalwater met en zonder koolzuur',`content_short`='',`amt1`='9.00',`zoek`='frisdrankmineraalwater',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='8',`type`='wijn',`ref`='wijn standaard',`name`='Wijn standaard horeca kwaliteit',`content_short`='Wijn standaard horeca kwaliteit',`amt1`='21.00',`zoek`='wijnwijn standaard',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='9',`type`='wijn',`ref`='wijn selected',`name`='Wijn geselecteerde kwaliteitswijn',`content_short`='Wijn geselecteerde kwaliteitswijn',`amt1`='21.00',`zoek`='wijnwijn selected',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='10',`type`='wijn',`ref`='wijn alcohol vry',`name`='Alcoholvrije wijn',`content_short`='Alcohol vrije drank op basis van wijn',`amt1`='9.00',`zoek`='wijnwijn alcohol vry',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='11',`type`='overig drank',`ref`='cider',`name`='Cider',`content_short`='',`amt1`='21.00',`zoek`='overig drankcider',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='12',`type`='sterke drank',`ref`='vodka',`name`='Wodka',`content_short`='',`amt1`='21.00',`zoek`='sterke drankvodka',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='13',`type`='sterke drank',`ref`='wisky',`name`='Wisky',`content_short`='',`amt1`='21.00',`zoek`='sterke drankwisky',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='14',`type`='sterke drank',`ref`='gedestilleerd',`name`='Gedestilleerd',`content_short`='',`amt1`='21.00',`zoek`='sterke drankgedestilleerd',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='15',`type`='overig drank',`ref`='postmixen',`name`='Postmixen',`content_short`='',`amt1`='21.00',`zoek`='overig drankpostmixen',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='16',`type`='overig drank',`ref`='overig',`name`='Overig',`content_short`='',`amt1`='21.00',`zoek`='overig drankoverig',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='17',`type`='overige',`ref`='koolzuur',`name`='Koolzuur',`content_short`='',`amt1`='21.00',`zoek`='overigekoolzuur',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='18',`type`='verhuur',`ref`='verhuurartikelen',`name`='Verhuur artikelen',`content_short`='',`amt1`='21.00',`zoek`='verhuurverhuurartikelen',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='19',`type`='emballage',`ref`='emballage',`name`='Emballage',`content_short`='',`amt1`='0.00',`zoek`='emballageemballage',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='20',`type`='emballage',`ref`='pallet',`name`='Pallet',`content_short`='',`amt1`='0.00',`zoek`='emballagepallet',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='21',`type`='transport',`ref`='transport kosten',`name`='Transportkosten',`content_short`='',`amt1`='21.00',`zoek`='transporttransport kosten',`active`='1',`updated`='2023-12-12 11:48:46';
INSERT INTO lep_mod_go_group SET `id`='22',`type`='xtrah',`ref`='toeslagen 21',`name`='Toeslag btw hoog',`content_short`='',`amt1`='21.00',`zoek`='xtrahtoeslagen 21',`active`='1',`updated`='2024-01-10 17:45:39';
INSERT INTO lep_mod_go_group SET `id`='23',`type`='xtral',`ref`='toeslagen 9',`name`='Toeslag btw laag',`content_short`='',`amt1`='9.00',`zoek`='xtraltoeslagen 9',`active`='1',`updated`='2024-01-10 17:46:19';
INSERT INTO lep_mod_go_group SET `id`='24',`type`='xtra',`ref`='toeslag',`name`='Toeslag btw vrij',`content_short`='',`amt1`='0.00',`zoek`='xtratoeslag',`active`='1',`updated`='2024-01-10 17:45:58';
