<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

 
$MOD_GSMOFFM = array(
	'OWN' => "MOD_GSMOFFM",
	'LANG' => "EN",
	'VERS' => "20250215",
		
	'active' => array ( 
		'0' => 'not active', 
		'1' => 'active',
		'2' => 'extra',
		'3' => 'lease'),
		
	'DUMMY' => array (
		'0' => 'No functionality. Database not initialised, no access rights or settings incorrect',
		'1' => 'Dummy module without functionality launched',
		'2' => 'Ensure initial functionality setting is performed'),
		

	'TXT_ERROR_DATA' 	=> ' Oeps geen data ',  
	'TXT_ERROR_INIT'	=> ' Oeps systeem niet geinitialiseerd en/of lege database ',
	'TXT_CONSISTENCY'	=> ' Oeps consistency controle ',	
	'TXT_ERROR_SIPS'	=> ' Oeps sips active ',
	'TXT_LOGIN_SETT'	=> ' Correct Login Settings  ',
	'TXT_REC_CHANGE'	=> ' Aantal records aangepast : ',	
	'TXT_NO_ACCESS'		=> '(Partner) Access not available ',
	'TXT_ACTIVE_DATA'	=> ' Actief record gevonden ' ,
	'TXT_LOGIN' 		=> ' Login ',
	'TXT_LOGIN_REGISTER' => ' Register / Change Password ',
	'TXT_LOGIN_VERIFY' => ' Verificatie ',
	'TXT_MAINTENANCE' 	=> ' Onderhoud ', 
	'MSG_DIR_CREATION' => ' Directory aangemaakt',
	'LOGIN_ERROR_SHORT' => ' Not a valid e-mail address or password too short. ',
	'LOGIN_NOW' => ' Uw login data is aangepast. Login met uw nieuwe gegevens. ',

	'def_table_prefix'	=> 'lep_',
	
	'eventstatus' => array ( 
		'0' => 'niet actief', 
		'1' => 'afgewezen',
		'2' => 'aanvraag',
		'3' => 'inbehandeling',
		'6' => 'actief',
		'7' => 'voorbij',	
		'9' => 'memorial'),
	
	'agendastatus' => array ( 
		'0' => 'niet actief', 
		'1' => 'actief..',
		'2' => 'actief', // bij eenmalig of herhaling		
		'9' => 'memorial'),	
		
	'eventaantal' => array ( 
		'0' => '1-5', 
		'1' => '6-10',
		'2' => '11-20',
		'3' => '21-50',
		'4' => '51-100',
		'5' => '>100'),
	
	'line_color' => array( 
		0 => '', 
		1 => 'bgcolor="#eeeeee"', 
		2 => 'bgcolor="#dddddd"', 
		3 => 'bgcolor="#cccccc"', 
		4 => 'bgcolor="#bbbbbb"'),


	'TEMPL' => array (
		'0' => '<button class="%1$s ui button" name="command" value="%2$s" type="submit">%3$s</button>',
		'1' => '<div class="ui action input">
					<button class="%1$s ui button" name="command" type="submit" value="%2$s" type="submit">%3$s</button>
					<input type="text" name="selection" value="%4$s" placeholder="Parameter..." /><i class="info icon" data-tooltip="%5$s"><i class="info circle icon"></i></i>
				</div>',

		'2' => '<a target="_blank" href="%2$s"><img src="%3$s" alt="pdf document">%1$s</a>',
		'3' => '<div class="inline">
					<button class="ui button" name="command" value="down"><i class="angle left icon"></i></button>
					<input type="hidden" name="n0" value="%1$s" />
					<input type="text" name="n1" size="3" value="%1$s" />
					- %2$s of %3$s
					<button class="ui button" name="command" value="up"><i class="angle right icon"></i></button>
				</div>'	,
		'4' => '<div class="fields">
					<div class="field">
						<button class="ui button" name="command" value="down"><i class="angle left icon"></i></button>
					</div>
					<div class="field">
						<input type="hidden" name="n0" value="%1$s" />
						<input type="text" name="n1" size="3" value="%1$s" />
					</div>
					<div class="field">
						<button class="ui tertiary button">tot %2$s van %3$s</button>
					</div>
					<div class="field">
						<button class="ui button" name="command" value="up"><i class="angle right icon"></i></button>
					</div>
				</div>'),	
	
	'tbl_icon' => array ( 
		1 =>'View', 
		2 =>'Return', 
		3 =>'Add',
		4 =>'Save',  
		5 =>'Save (as new)', 
		6 =>'Remove', 
//		7 =>'Calculate',
//		8 =>'Check',
		9 =>'Select', 
//		10 =>'+',
		11 =>'Print', 
//		12 =>'Set',
//		14 =>'Next',
//		15 =>'Test',
//		16 =>'Mail',
//		16 =>'Mail', // 20
		17 =>'Process', //21
//		18 =>'Invoicing', //24
//		19 =>'Balans', //27 
		20 =>'Bevestigen',
		21 =>'Verwerkt'
), 
	
	'pdf'	=> array ( 
		'0' => "Document gemaakt op: ", 
		'1' => "Aantal groepen verwerkt: ",
		'2' => "Geselecteerde opties: ",
		'3' => "Module versies: ",
		'9' => "Geen relevante data gevonden: "),
		
		
		
	'stappen' => array (   // status
		0 => "Opgave benodigdheden",
		1 => "Open",
		2 => "Bevestigd",
		3 => "Contante betaling",
		4 => "Bank betaling (Pin/Tikkie)",
		5 => "Factuur : pro forma",
		6 => "Wacht op betaling",
		7 => "Factuur",
		8 => "Levering",		
		9 => "Duplicaat factuur",
		10 => "Duplicaat factuur",	
		11 => "Nota: Contante betaling",
		12 => "Nota: Bank betaling ",	
		13 => "Nota: Gefactureerd en geleverd",
		14 => "Notitie: Te factureren in ander systeem",		
		15 => "Verwijderen"), 

		
	'0stap'  =>  array (	
		0 => 'Initiele bestelling'."\n" ),	
	'1stap'  =>  array (
		0 => 'De bestelling is nog niet bevestigd'."\n" ),
	'2stap'  =>  array ( 
		0 => 'De (gewijzigde) bestelling is bevestigd en verstuurd'."\n" ),	
	'3stap'  =>  array (
		0 => '--'."\n",
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'4stap'  =>  array (	
		0 => 'Details : ',
		1 => 'Na ontvangst van %4$s zullen wij uw bestelling  uitleveren'."\n" ,
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'5stap'  =>  array (
		0=> 'Details : ',
		1 => 'Na ontvangst van %4$s  op bankrekening %1$s  t.n.v. %2$s  onder vermelding van: %3$s zullen wij Uw bestelling verwerken en uitleveren'."\n" ,
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'6stap'  =>  array (
		0=> 'Details : ',
		1 => 'Na ontvangst van %4$s  op bankrekening %1$s  t.n.v. %2$s  onder vermelding van: %3$s zullen wij Uw bestelling verwerken en uitleveren'."\n" ,
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'7stap'  => array ( 
		0=> 'Details : ',
		1 => 'Wij verzoeken u binnen 14 dagen te betalen  %4$s  op bankrekening %1$s  t.n.v. %2$s  onder vermelding van: %3$s '."\n" ,
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'6stap'  =>  array (	
		0=> 'Details : ',
		1 => 'Na ontvangst van %4$s  op bankrekening %1$s  t.n.v. %2$s  onder vermelding van: %3$s zullen wij Uw bestelling verwerken en uitleveren'."\n" ,
		2 => 'Op al onze leveringen zijn onze algemene voorwaarden van toepassing.'),
	'8stap'  => array ( 
		0 =>' Deze bestelling / levering wordt verwerkt'."\n" ),
	'9stap'  => array ( 
		0 =>' Deze bestelling / levering is verwerkt'."\n" ),
	'10stap'  => array ( 
		0 =>' Deze bestelling / levering is verwerkt'."\n" ),
	'11stap'  => array ( 
		0 =>' ---'."\n" ),
	'12stap'  => array ( 
		0 =>' Deze bestelling / levering is verwerkt'."\n" ),
	'13stap'  => array ( 
		0 =>' Deze bestelling / levering is verwerkt'."\n" ),
	'14stap'  => array ( 
		0 =>' Deze bestelling / levering is verwerkt'."\n" ),
	'15stap'  => array ( 
		0 =>' Gearchiveerd'."\n" ),		

/*	
	'lever'  => array (
		1 => "Mennen Handelsonderneming b.v.", 
		2 => "Mijelseweg 23A",
		3 => "5725BA Asten Heusden
		4 => "tel: 085-2733613",
		5 => "e-mail : info@mibadranken.nl"),
		
	'lever1'  => "NL54 INGB 0004 9347 96",
	'lever2'  => "Miba Dranken",
*/
);				

?>