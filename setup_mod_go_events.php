<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php


/* 1 The table involved */
$product = "events";
$oFC->file_ref [ 0 ] = LOAD_DBBASE . "_" . $product;
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function started ' . $oFC->file_ref [ 0 ] .  NL; 

/* 1B database creation if database does not exist */
$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 0 ] );

/* 1C Modifications needed will be stored at this place */
$job = array ();
	
/* 1D which fields are present in the main file */	
$result = array ( );
$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 0 ] ),
	true, 
	$result );
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 0 ] );
	
/* 1E_add /change fields not present  */
$localHulpA = array();
foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];


/* 1F wijzigen * /
if ( isset ( $localHulpA [ 'reference' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", 
		$oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_short' ] = true;
}

/* 1G toevoegen */
if ( !isset ( $localHulpA['refroom' ] ) ) {  // room
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `refroom` varchar(128) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['reftoegang' ] ) ) {  // toegang
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `reftoegang` varchar(128) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['refdrukte' ] ) ) {  //  name
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `refdrukte` int(3) NOT NULL DEFAULT 0 AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['link' ] ) ) {  // datum + herhaling
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `link` varchar(128) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}	
if ( !isset ( $localHulpA['email' ] ) ) {  // datum + herhaling
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `email` varchar(128) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['content_long' ] ) ) {  // de lange tekst
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(2000) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['datend' ] ) ) {  
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `datend` DATE NOT NULL DEFAULT '0000-00-00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA ['refherhaling' ] ) ) {  // datum + herhaling
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `refherhaling` varchar(128) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['timend' ] ) ) {  
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `timend` TIME NOT NULL DEFAULT '00:00:00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['timstart' ] ) ) {  
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `timstart` TIME NOT NULL DEFAULT '00:00:00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['datstart' ] ) ) {  
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `datstart` DATE NOT NULL DEFAULT '0000-00-00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}

/* 1H achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query );
}	}
/* 1I upgraded */

/* 1J other entries needed ? */
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
$job = array ();

if ( $oFC->setting [ 'debug' ] == "yes" ) 	Gsm_debug (array ("setting"=>  $oFC->setting ), __LINE__ . $module_name ); 

if ( !isset ( $oFC->setting [ 'zoek' ] [ $product ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('zoek', '%s', '%s', '1' )",
		$product, '|type|name|');
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $product . NL;
}

if ( !isset ( $oFC->setting [ 'datadir' ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		'datadir|gsmoffm', '/media/kassa');
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'datadir' . NL;
}

if ( !isset ( $oFC->setting [ 'mediadir'  ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		'mediadir|gsmoffm', '/media/product' );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
}

if ( !isset ( $oFC->setting [ 'eventdir'  ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		'eventdir|gsmoffm', '/media/events' );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'eventdir' . NL;
}

if ( !isset ( $oFC->setting [ 'room' ] ) ) {
	$main_parameter = 'groot';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('room', '%s', '%s', '1' )",
		TABLE_PREFIX, 
		'groot', 
		$main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' room setting added ' . NL;
	$oFC->setting [ 'room' ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'toegang' ] ) ) {
	$main_parameter = 'besloten';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('toegang', '%s', '%s', '1' )",
		TABLE_PREFIX, 
		$main_parameter, 
		'besloten');
	$oFC->setting [ 'toegang' ] [ $main_parameter ] = "besloten";
	$main_file = 'openbaar';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('toegang', '%s', '%s', '1' )",
		TABLE_PREFIX, 
		$main_file, 
		'openbaar');
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' toegang setting added  ' . NL;
	$oFC->setting [ 'toegang' ] [ $main_file ] = "openbaar";
}

/* 1K achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query ); 
	} 
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
}
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 0 ] .  NL; 
?> 