<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php


/* 1 The table involved */
$product = "product";
$oFC->file_ref [ 0 ] = LOAD_DBBASE . "_" . $product;
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function started ' . $oFC->file_ref [ 0 ] .  NL; 

/* 1B database creation if database does not exist */
$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 0 ] );

/* 1C Modifications needed will be stored at this place */
$job = array ();
	
/* 1D which fields are present in the main file */	
$result = array ( );
$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 0 ] ),
	true, 
	$result );
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 0 ] );
	
/* 1E_add /change fields not present  */
$localHulpA = array();
foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

/* 1F wijzigen */
if ( isset ( $localHulpA [ 'reference' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", 
		$oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_short' ] = true;
}
	
/* 1G toevoegen */
if ( !isset ( $localHulpA['amt5' ] ) ) {  // voor voorraad admin
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt5` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['amt4' ] ) ) {  // voor het statiegeld bedrag
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt4` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['amt3' ] ) ) {  //  voor de costprijs ex btw
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt3` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['amt2' ] ) ) {  // onze prijs / verhuurprijs  ex btw
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt2` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['amt1' ] ) ) {  // voor de advies verkoopprijs incl btw 
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt1` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA['amt0' ] ) ) {  // voor het btw percentage
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt0` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 0 ] );}	
if ( !isset ( $localHulpA['content_long' ] ) ) {  // de lange tekst
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(2000) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}

/* 1H achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query );
}	}

/* 1I upgraded */

/* 1J other entries needed ? */
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
$job = array ();

if ( !isset ( $oFC->setting [ 'zoek' ] [ $product ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('zoek', '%s', '%s', '1' )",
		$product, '|type|ref|name|');
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $product . NL;
}

if ( !isset ( $oFC->setting [ 'mediadir'  ] ) ) {
	$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		'mediadir|gsmoffm', '/media/product' );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
}

/* 1K achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query ); 
	} 
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
}
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 0 ] .  NL; 
?> 