<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php


/* 1 The table involved */
$product = "social";
$oFC->file_ref [ 0 ] = LOAD_DBBASE . "_" . $product;
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Create / Upgrade function started ' . $oFC->file_ref [ 0 ] .  NL; 

/* 1B database creation if database does not exist */
$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 0 ] );

/* 1C Modifications needed will be stored at this place */
$job = array ();
	
/* 1D which fields are present in the main file */	
$result = array ( );
$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 0 ] ),
	true, 
	$result );
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 0 ] );
	
/* 1E_add /change fields not present  */
$localHulpA = array();
foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

/* 1F wijzigen */
if ( isset ( $localHulpA [ 'content_url' ] ) ) {  // wijzigen
	if ( !strstr ( $localHulpA [ 'content_url' ], "127" ) )
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `content_url` `content_url`  VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_url' ] = true;
}	
if ( isset ( $localHulpA [ 'content_url' ] ) ) {  // wijzigen
	if ( !strstr ( $localHulpA [ 'zoek' ], "2047" ) )
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `zoek` `zoek`  VARCHAR(2047) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'zoek' ] = true;
}
	
/* 1G toevoegen */
	if ( !isset ( $localHulpA [ 'comment' ] ) ) {  // interne tekst
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `comment` varchar(2047) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_long' ] ) ) {  // lange omschrjving
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(4095) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_tel' ] ) ) {  // telefoonnummer
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_tel` varchar(63) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_email' ] ) ) {  // e-mail adres 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_email` varchar(63) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_url' ] ) ) {  // website adres adres 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_url` varchar(127) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_adres' ] ) ) {  // website adres adres 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_adres` varchar(127) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_wijk' ] ) ) {  // voor welke wijken dit is 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_wijk` varchar(511) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}
	if ( !isset ( $localHulpA [ 'content_groep' ] ) ) {  // in welke groepen dit getoond moet worden
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_groep` varchar(511) NOT NULL DEFAULT '' AFTER `content_long`", $oFC->file_ref [ 0 ] );}

/* 1H achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query );
}	}
/* 1I upgraded */

/* 1J other entries needed ? */
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
$job = array ();

/* check */

if ( !isset ( $oFC->setting [ 'zoek' ] [ $product ] ) ) {
	$main_parameter = '|type|ref|name|id|content_short|content_groep|content_wijk|content_adres|content_url|content_email|content_tel|';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('zoek', '%s', '%s', '1' )",
		TABLE_PREFIX, $product, $main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $product . NL;
	$oFC->setting [ 'zoek' ] [ $product ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'sgroep' ] ) ) {
	$main_parameter = 'Alarmnummers';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('sgroep', '%s', '%s', '1' )",
		TABLE_PREFIX, 
		'G001', 
		$main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' sgroep setting added ' . NL;
}

if ( !isset ( $oFC->setting [ 'swijk' ] ) ) {
	$main_parameter = '-- Geheel gebied --';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('swijk', '%s', '%s', '1' )",
		TABLE_PREFIX, 
		'AA', 
		$main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' swijk setting added ' . NL;
}

/* 1K achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query ); 
	} 
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
}
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 0 ] .  NL; 
?> 