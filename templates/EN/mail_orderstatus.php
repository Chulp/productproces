<?php
/*
 * @module     mail template
 */
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

// end include class.secure.php
/* change history
 * v20250528
 * 
 */
    $mail_subject = 'Status van uw bestelling : {GSM_BESTELLING}';
    $mail_content = 
	'Hallo {GSM_NAME},
	<br/><br/>We zijn {WEB_TIMESTAMP} bezig met uw bestelling.
	<br/><br/>Als U de status van uw bestelling wilt weten volg dan deze link <a target="_blank" href="{GSM_LINK}">Status van uw bestelling {GSM_BESTELLING}</a>.<br/>
	<br/><br/>Met vriendelijke groet, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>