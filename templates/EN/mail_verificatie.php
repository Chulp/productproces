<?php
/*
 * @module     mail template
 */
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

// end include class.secure.php
/* change history
 * v20250528
 * 
 */
    $mail_subject = 'Controle van Uw e-mail adres';
    $mail_content = 
	'Hallo {GSM_NAME},
	<br/><br/>U bent {WEB_TIMESTAMP} bezig met een bestelling.
	<br/><br/>Door Uw invoer van de onderstaande verificatiecode bij de bestelling bevestigd U dat het gebruikte e-mail adres correct is.
	<br/>{GSM_LINK}<br/><br/>Bovenstaande verificatie code dient U in te voeren bij uw bestelling om verder te gaan.<br/>Deze code is slechts een beperkte tijd bruikbaar.<br/>Indien u niet bezig bent met een bestelling op de website {WEB_SITE} negeer dan deze mail. 
	<br/>Als U de status van uw bestelling in de komende tijd wilt weten volg dan deze link <a target="_blank" href="{GSM_DLINK}">Status document</a>. (bewaar deze link) <br/>
	<br/><br/>Met vriendelijke groet, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>