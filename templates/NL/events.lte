{#
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2017-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 #}
{% autoescape false %}
<div id="top"><button class="ui right floated button" ><a href="#bottom"><i class="caret square down icon"></i>BOTTOM</a></button></div>
<h2 class="ui blue header">
<i class="{{ICON}}"></i>
<div class="content">
	{{FORMULIER}}
	<div class="sub header">{{SUB_HEADER}}</div>
</div></h2>
<div class="container">
	<div class="{{MESSAGE_CLASS}}">{{STATUS_MESSAGE}}</div>
	{{MESSAGE}}
</div>
<div class="ui segment">
	<form class="ui form" name="{{PAGE_ID}}|{{SECTION_ID}}|{{MODE}}|{{OWNER}}"  enctype="multipart/form-data" method="post" action="{{RETURN}}">
		<input name="module" type="hidden" value="{{MODULE}}" />
		<input name="timestamp" type="hidden" value="{{DATE}} {{TIME}}"/>
		<input name="sips" type="hidden" value="{{HASH}}" />
		<input name="recid" type="hidden"  value="{{RECID}}" />
		<input name="memory" type="hidden" value="{{MEMORY}}" />
		{{DESCRIPTION}}
		{{SELECTIONB}}	
		{{SELECTIONA}}
		{{SELECTIONC}}	
{% if MODE == 9 %}
	{% if LOAD == "v" %} <div class="ui styled fluid accordion">{% endif %}
		{% for item in RESULTS %}
			<div class="title">
				<i class="dropdown icon"></i>
				{% for item in item %}
					{% if loop.first %} 
						{{item.activetxt}}
					{% endif %}
				{% endfor %}
			</div>
			<div class="ui large form content">
			<table class="ui celled striped table">
				<thead>
					<tr>
						<th class="right aligned">Ref</th>
						<th>Titel</th>
						<th>Datum</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			{% for item in item %}
				<tr>
					<td class="single line">
						<a href="{{ RETURN }}&command=select&module={{ MODULE }}&recid={{ item.id }}" target="_125"><span class="ui tiny text">{{ item.id}}</span>&nbsp;<i class="edit icon"></i></a>&nbsp;
						{% if item.active < 1 %}<a href="{{ RETURN }}&command=remove&module={{ MODULE }}&recid={{ item.id }}" target="_125"><i class="trash alternate icon"></i></a>&nbsp;{% endif %}
						{{item.ref}}
						<i class="info icon" data-tooltip="{{item.email}}"><i class="info circle icon"></i></i>
						</td>
					<td>{{ item.name }}</td>
					<td>{{ item.datetxt }}
					</td>
					<td>{{ item.activetxt }}</td>	
				</tr>
			{% endfor %}
				<tbody>
			</table>
			</div>
		{% endfor %}
	{% if LOAD == "v" %} </div>{% endif %}
{% endif %}

{% if MODE == 8 %}
{#  record edit basis data  #}
<input name="gsm_id" type="hidden" value="{{RESULT.id}}" />
{% if LOAD == "v" %} <div class="ui styled fluid accordion">{% endif %}
	<div class="title {{REFERENCE_ACTIVE1}} "><i class="dropdown icon"></i>{{RESULT.name}} <span class="ui tiny text">(  {{RESULT.id}}  )</span> </div>
	<div class="ui large form {{REFERENCE_ACTIVE1 }} content">
		<div class="ui fluid secondary segment">
			<div class="inline fields">
				<div class="three wide field">
					<label>Referentie</label>
				</div>
				<div class="five wide field">
					{{RESULT.ref}}
					<input name="gsm_ref" type="hidden" value="{{RESULT.ref}}" />
				</div>
			</div>
			<div class="inline fields">
				<div class="three wide field">
					<p>Titel Activiteit</p>
				</div>
				<div class="thirteen wide field">
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<input type="text" 
						name="gsm_name" 
						value="{{RESULT.name}}" 
						tabindex="{{tabindex}}" />
				</div>
			</div>
			<div class="inline fields">
				<div class="three wide field">
					<label>Datum</label>
				</div>
				<div class="ui calendar five wide field" id="date_calendar">
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<div class="ui input left icon input">
						 <i class="calendar icon"></i>
						 <input class="{{ EVENTDATE_ERROR }}" 
							type="text" 
							name="gsm_datstart"
							value="{{RESULT.datstart}}"
							tabindex="{{tabindex}}"
							placeholder="Date" />
					 </div>
				</div>
			</div>	
			<div class="inline fields">
				<div class="three wide field">
					<label>Tijd</label>
				</div>
				<label>Vanaf</label>
				<div class="ui calendar four wide field" id="time_calendar">
					<div class="ui input left icon input">
						<i class="time icon"></i>
						<input  class="{{ EVENTTIME_ERROR }}"
							type="text"
							name="gsm_timstart"
								value="{{RESULT.timstart}}"
								tabindex="{{tabindex}}"
							placeholder="Time">
					 </div>
				</div>
				<label>Tot</label>
				<div class="ui calendar four wide field" id="time2_calendar">
					<div class="ui input left icon input">
						<i class="time icon"></i>
						<input  class="{{ ENDTIME_ERROR }}"
							type="text"
							name="gsm_timend"
								value="{{RESULT.timend}}"
								tabindex="{{tabindex}}"
							placeholder="Time">
					 </div>
				</div>
			</div>
			<div class="inline fields">			
				<div class="three wide field">
					<label>Ruimtes nodig</label>
				</div>
				<div class="thirteen wide field">
					<div class="ui fluid multiple search selection dropdown">
						<input type="hidden" 
						name="gsm_refroom"
						value="{{RESULT.refroom}}" 
						tabindex="{{tabindex}}"  />
						<i class="dropdown icon"></i>
						<div class="default text">Selecteer Ruimtes</div>
						<div class="menu">
							{{ ROOM }}
						</div>
					</div>
				</div>
			</div>
			<div class="inline fields">	
				<div class="three wide field">
					<p>Personen</p>
				</div>
				<div class="five wide field">
					<select class="ui fluid search dropdown" name="gsm_refdrukte" >{{RESULT.REFDRUKTE}}</select>
				</div>
			</div>
			<div class="inline fields">	
				<div class="three wide field">
					<p>Status Event</p>
				</div>
				<div class="five wide field">
					<select class="ui fluid search dropdown" name="gsm_active" >{{RESULT.ACTIVE}}</select>
				</div>
			</div>
		</div>
	</div>
	<div class="title {{REFERENCE_ACTIVE2 }}"><i class="dropdown icon"></i>Herhaling</div>
	<div class="ui large form {{REFERENCE_ACTIVE2}} content">
		<div class="ui fluid secondary segment">
{% for item in RESULT.agenda %}		
{% if loop.first %} 
		<table class="ui inverted table">
			<thead>
				<tr>
					<th><i class="plus square outline icon"></th>
					<th><i class="calendar icon"></i>Datum</th>
					<th><i class="time icon"></i>Vanaf</th>
					<th><i class="time icon"></i>Tot</th>
					<th><i class="trash alternate outline icon"></th>
				</tr>
			</thead>
			<tbody>
{% endif %}
{% set nxt = ( nxt | default(0) ) + 1 %}
{% if item.active < 2 %}
	<tr class="passive">
{% elseif item.active < 4 %}	
	<tr class="positive">
{% elseif item.active < 7 %}	
	<tr class="active">
{% else %}
	<tr class="passive">
{% endif %}		
		<td>{#<input type="checkbox" name="y|{{item.id}}|add">#}{{nxt}} <span class="ui small text">({{ item.id}})</span></td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="a|{{item.id}}|{{item.datstart}}" value="{{item.datstart}}" />
		</td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="b|{{item.id}}|{{item.timstart}}" value="{{item.timstart|trim ("0")|trim (":")}}" />
		</td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="c|{{item.id}}|{{item.timend}}" value="{{item.timend|trim ("0")|trim (":")}}" />
		</td>
		<td><input type="checkbox" name="x|{{item.id}}|delete"></td>
    </tr>
{% if item.dubbel|length > 7 %}
	<tr class="passive">
		<td><i class="icon close"></i></td>
		<td>{{item.type}}</td>
		<td colspan= "3">{{item.dubbel}}</td>
	</tr>
{% endif %}	
{% if loop.last %} 
{% if RESULT.refherhaling|length > 2 %}
{% for i in 1..5 %}	
{% set nxt = ( nxt | default(0) ) + 1 %}
	<tr class="active">
		<td><input type="checkbox" name="p|{{i}}|add"><span class="ui tiny text">({{nxt}})</span></td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="q|{{i}}|{{item.datstart}}" value="{{item.datstart}}" />
		</td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="r|{{i}}|{{item.timstart}}" value="{{item.timstart|trim ("0")|trim (":")}}" />
		</td>
		<td>
			<input class="ui {% if item.datstart < 2 %} transparent {% endif %} input" type="text" name="s|{{i}}|{{item.timend}}" value="{{item.timend|trim ("0")|trim (":")}}" />
		</td>
    </tr>
{% endfor %}	
{% endif %}
			</tbody>
		</table>
{% endif %}
{% endfor %}	
			<div class="inline fields">			
				<div class="three wide field">
					<label>Herhaling</label>
				</div>
				<div class="thirteen wide field">
					<div class="ui fluid multiple search selection dropdown">
						<input type="hidden" 
						name="gsm_refherhaling"
						value="{{RESULT.refherhaling}}" 
						tabindex="{{tabindex}}"  />
						<i class="dropdown icon"></i>
						<div class="default text">selecteer</div>
						<div class="menu">
							<div class="item" data-value="eenmalig">eenmalig</div>	
							<div class="item" data-value="mixed">herhaling</div>
{#							<div class="item" data-value="dag">dag</div>							
							<div class="item" data-value="week">week</div>
							<div class="item" data-value="maand">maand</div>
							<div class="item" data-value="jaar">jaar</div>
							<div class="item" data-value="2">2e</div>	
							<div class="item" data-value="3">3e</div>	
							<div class="item" data-value="4">4e</div>
							<div class="item" data-value="new">Aanpassen</div>#}
						</div>
					</div>
				</div>
			</div>
			<div class="inline fields">			
				<div class="three wide field">
					<label>Eind datum</label>
				</div>
				{% set tabindex = ( tabindex | default(0) ) + 1 %}
				<div class="ui input left icon input">
					 <i class="calendar icon"></i>
					 <input class="{{ ENDDATE_ERROR }}" 
						type="text" 
						name="gsm_datend"
						value="{{RESULT.datend}}"
						tabindex="{{tabindex}}"
						placeholder="yyyy-mm-dd" />
				</div>
			</div>	
		
	
		</div>
	</div>
	<div class="title {{REFERENCE_ACTIVE3}} "><i class="dropdown icon"></i>Details</div>
	<div class="ui large form   {{REFERENCE_ACTIVE3}} content">
		<div class="ui fluid secondary segment">
			<div class="inline fields">	
				<div class="three wide field">
					<label>E-Mail contact</label>
				</div>
				<div class="thirteen wide field">
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<input type="text" 
						name="gsm_email" 
						value="{{RESULT.email}}" 
						tabindex="{{tabindex}}" />
				</div>
			</div>
			<div class="fields">
				<div class="sixteen wide field">
					<label>Adres gegevens organisatie</label>
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<textarea rows="3" cols="80" name="gsm_content_short" tabindex="{{tabindex}}" >{{RESULT.content_short}} </textarea> 
				</div>
			</div>
			<div class="fields">
				<div class="sixteen wide field">
					<label>Omschrijving Event</label>
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<textarea rows="3" cols="80" name="gsm_content_long" tabindex="{{tabindex}}" >{{RESULT.content_long}} </textarea> 
				</div>
			</div>
			<div class="inline fields">	
				<div class="three wide field">
					<label>Externe link</label>
				</div>
				<div class="thirteen wide field">
					{% set tabindex = ( tabindex | default(0) ) + 1 %}
					<input type="text" 
						name="gsm_link" 
						value="{{RESULT.link}}" 
						tabindex="{{tabindex}}" />
				</div>
			</div>
			<div class="inline fields">	
				<div class="three wide field">
					<label>Toegang</label>
				</div>
				<div class="thirteen wide field">
					<div class="ui fluid multiple search selection dropdown">
						<input type="hidden" 
						name="gsm_reftoegang"
						value="{{RESULT.reftoegang}}" 
						tabindex="{{tabindex}}"  />
						<i class="dropdown icon"></i>
						<div class="default text">Toegang</div>
						<div class="menu"> 
							{{ TOEGANG }}
						</div>
					</div>
				</div>
			</div>
			<div class="fields">
				<div class="three wide field"></div>
				<div class="ten wide field">
					{{RESULT.IMAGE}}
				</div>
			</div>
			<div class="inline fields">
				<div class="ten wide field"><label>Illustratie</label></div>
			</div>
			<div class="inline fields">
				<div class="twelve wide field">
					<input type="file" name="doc_uploaded" />
				</div>
			</div>
		</div>
	</div>
{% if LOAD == "v" %} </div>{% endif %}

{% endif %}
		{{SELECTIONC}}	
{#  tail of the form  #}
	</form>
	{{RAPPORTAGE}}
	{{TOEGIFT}}
</div>
<div id="bottom"><button class="ui right floated button" ><a href="#top"><i class="caret square up icon"></i>TOP</a></button></div>
{% endautoescape %}