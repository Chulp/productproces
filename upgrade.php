<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* hisory
 * 2025-02-03 add zip and product droplet
 */

/* install droplets */
$droplet_names = array();
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_bestel.zip'))	$droplet_names [] = 'droplet_Gsm_bestel';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_zip.zip'))	$droplet_names [] = 'droplet_Gsm_zip';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_product.zip'))	$droplet_names [] = 'droplet_Gsm_product';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_order.zip'))	$droplet_names [] = 'droplet_Gsm_order';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_ordercollect.zip'))	$droplet_names [] = 'droplet_Gsm_ordercollect';

if ( count ( $droplet_names ) >0 ) {
  echo (LEPTON_tools::display('(re-)install droplets</b>','pre','ui positive message')); 
  LEPTON_handle::install_droplets ( 'gsmoffm', $droplet_names);
}


?>