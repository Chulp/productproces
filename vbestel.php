<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
/* 1 module id */ 
$module_name = 'vbestel';
$version = '20240504';
$main_file= "proces";
$sub_file= "product";
$sub_file2 = "adres";
$unique = 'ref';
$project= "Bestel afhandeling";
$default_template = '/bestel.lte';
 
/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* 4 file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = LOAD_DBBASE . "_" .$sub_file2;

/* 5 get settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
//$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
//$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 8 Gebruik limited door rechthebbenden ?? */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("setting"=>$oFC->setting ), __LINE__ . $module_name ); 
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page"=>$oFC->page_content ), __LINE__ . $module_name ); 
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ( "user"=>$oFC->user ), __LINE__ . $module_name ); 

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 get saved values */ 
$oFC->gsm_memorySaved ( );

if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , "xmode"=>$xmode ), __LINE__ . $module_name ); 

/* 14 Print function requested  ?? */
if ( isset ( $xmode ) && strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_group_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );

/* 15 intended sips on GET  */
$oFC->page_content [ 'OPTION' ] = 1;
if ( $oFC->page_content[ 'PRIVILEGED' ] == 3 ) $oFC->page_content [ 'OPTION' ] = 2;
$oFC->page_content [ 'FORMULIER' ] = "Bestelling";
if ( isset ( $_GET [ 'option' ] ) ) {
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page"=>$oFC->page_content ), __LINE__ . $module_name ); 
	switch (  $_GET [ 'option' ] ) {
		case "bet1": //open
			$oFC->page_content [ 'OPTION' ] = 3;
			$oFC->page_content [ 'FORMULIER' ] = "Contante betaling winkelwagen";
			break;
		case "bet2": //open
			$oFC->page_content [ 'OPTION' ] = 4;
			$oFC->page_content [ 'FORMULIER' ] = "Pin betaling winkelwagen";
			break;
		case "bet3": //open
			$oFC->page_content [ 'OPTION' ] = 5;
			$oFC->page_content [ 'FORMULIER' ] = "Facturering winkelwagen";
			break;
		default: //list
			$oFC->page_content [ 'OPTION' ] = 1;
			$oFC->page_content [ 'FORMULIER' ] = "Bestelling";
			break;
	} 
}

if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page"=>$oFC->page_content ), __LINE__ . $module_name ); 

/* 19 sips test before job */ 
if ( isset ( $_POST[ 'command' ] ) && $oFC->sips ) { 
	unset ($_POST[ 'command' ]); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "" ), __LINE__ . $module_name ); 
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page"=>$oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name ); 

/* 20 some job to do */
$changed = false;
if ( isset( $_POST[ 'command' ] ) ) {
	$changed = true;
	$afgewerkt = false;
	switch ( $_POST[ 'command' ] ) {
		case 'VerwerkenB':  //opslaan
			if ( strlen ( $_POST [ 'gsm_email' ] ) < 8 ) { 
				$oFC->description .= "missing e-mail adres ";
			} else {
				$oFC->page_content [ 'voortgang' ] = 2;
				$parseArr = array ( );
				$parseArr [ 'GSM_NAME' ] 	= $page_content [ 'name' ] ?? "";
				$parseArr [ 'WEB_WEBMASTER' ] = $oFC->user [ 'name' ] ?? "webmaster" ;
				$parseArr [ 'GSM_BESTELLING' ] = $oFC->page_content [ 'proces_referentie' ] ?? ""; 
				$parseArr [ 'WEB_EMAIL' ] 	= $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ];
				$parseArr [ 'WEB_SITE' ] 	= LEPTON_URL;
				$LocalHulp = str_replace ("//", "/" ,LEPTON_PATH . $oFC->setting [ 'mediadir' ] ."/".$_POST [ 'gsm_ref' ] . "_doc.pdf" );
				$parseArr [ 'GSM_LINK' ] 	=  str_replace( LEPTON_PATH, LEPTON_URL, str_replace ("//", "/" ,$LocalHulp ) );
				$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );	
				$template="mail_orderstatus.php";
				$oFC->description .= $oFC->gsm_mail ( $template, $parseArr [ 'WEB_EMAIL' ] , $parseArr );
				unset ( $_POST [ 'gsm_voortgang' ] );
			}
		case 'Verwerkenf':  //opslaan
			if ($_POST[ 'command' ] == 'Verwerkenf' ) $afgewerkt = true; 
		case 'Verwerken':  //opslaan
			$oFC->page_content [ 'MODE' ] = 9;
			if ( !isset ( $oFC->user[ 'email' ] ) && strlen ( $_POST [ 'gsm_email' ] ) < 8 ) $oFC->description .= "missing e-mail adres ";
			$oFC->page_content [ 'ref' ] = $_POST [ 'gsm_ref' ] ?? "P" . date ( 'U');
			if ( isset ( $_POST [ 'gsm_voortgang' ] ) ) $oFC->page_content [ 'voortgang' ] = $_POST [ 'gsm_voortgang' ] ?? 1 ;
			$FieldArr = array ();
			$message = "";
			/* voortgang */
			$FieldArr [ 'voortgang'] = $_POST [ 'gsm_voortgang' ] ?? $oFC->page_content [ 'voortgang' ];
			if ( $afgewerkt ) {
				if ( $oFC->page_content [ 'voortgang' ] == 1 ) $FieldArr [ 'voortgang'] = 2;
				if ( $oFC->page_content [ 'voortgang' ] == 2 ) $FieldArr [ 'voortgang'] = 5;
				if ( $oFC->page_content [ 'voortgang' ] == 3 ) $FieldArr [ 'voortgang'] = 11;
				if ( $oFC->page_content [ 'voortgang' ] == 4 ) $FieldArr [ 'voortgang'] = 12;
				if ( $oFC->page_content [ 'voortgang' ] == 5 ) $FieldArr [ 'voortgang'] = 6;
				if ( $oFC->page_content [ 'voortgang' ] == 6 ) $FieldArr [ 'voortgang'] = 8;
				if ( $oFC->page_content [ 'voortgang' ] == 8 ) $FieldArr [ 'voortgang'] = 13
				;
			}
			/* adres present ? */
			if ( strlen ( $_POST [ 'gsm_email' ] ) > 7 && strlen ( $_POST [ 'gsm_content_short' ] ) < 10 ) {
				/* try to find adres fitting with e-mail adres*/
				$LocalHulpA = $oFC->gsm_adresFromProces ( $_POST [ 'gsm_email' ] );
				if ( strlen ( $LocalHulpA ) > 10 ) $FieldArr [ 'content_short'] = $LocalHulpA;
			}
			
			/* de rest */
			$oFC->page_content[ 'RECID' ] = $_POST [ 'gsm_id' ];
			
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page" => $oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name );
 			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("Fieldarr" => $FieldArr, "rec" => $oFC->page_content[ 'RECID' ] ), __LINE__ . $module_name );	
			
			$oFC->page_content = array_merge ( $oFC->page_content, $oFC->gsm_accessRec ( $FieldArr, $oFC->page_content[ 'RECID' ], 1, 'proces' ) );
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( "page" => $oFC->page_content ), __LINE__ . $module_name );
			
			$local_content = $oFC->gsm_AccessOrderLine ( $oFC->page_content [ 'payload' ], $oFC->page_content[ 'RECID' ], $oFC->page_content [ 'TOTALAMT' ] , 1);
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("local" => $local_content , "payload"=>$oFC->cal ), __LINE__ . $module_name );

			$oFC->page_content [ 'PAYLOAD' ] = $oFC->cal;
			break;
		default:
			gsm_debug ( array ( $_GET ?? "", $_POST ?? "", $oFC->page_content ), __LINE__ . __FUNCTION__ );
			break;
	}
} elseif ( isset( $_GET[ 'command' ] ) ) {
	$changed=true;
	switch ( $_GET[ 'command' ] ) {
		/* selecting */
		case "zaak":
			if ( $oFC->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $_GET ?? "" ), __LINE__ . __FUNCTION__ );/*debug */ 

			/* get data open proces record */
			$FieldArr = array ();
			$message = '';
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page" => $oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name );
 			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("Fieldarr" => $FieldArr, "rec" => $oFC->page_content[ 'RECID' ] ), __LINE__ . $module_name );	

			$oFC->page_content = array_merge ( $oFC->page_content, $oFC->gsm_accessRec ( $FieldArr, $oFC->page_content[ 'RECID' ], 1, 'proces' ) );
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
	
			/* get entries winkelwagen */
			$local_content = $oFC->gsm_AccessOrderLine ( $oFC->page_content [ 'payload' ], $oFC->page_content[ 'RECID' ], $oFC->page_content [ 'TOTALAMT' ], 1 );
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("local" => $local_content , "payload"=>$oFC->cal ), __LINE__ . $module_name );

			$oFC->page_content [ 'PAYLOAD' ] = $oFC->cal;
			break;
		/* delete */
		case "delete":
			if ( $oFC->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $_GET ?? "" ), __LINE__ . __FUNCTION__ );/*debug */ 
			$database->simple_query ( 
				sprintf ( "DELETE FROM `%s` WHERE `id`= '%s'" , 
				$oFC->file_ref [ 99 ], 
				$_GET [ 'recid' ] ) );
			$oFC->page_content[ 'RECID' ] = 0;	
			$oFC->page_content['P1'] = true;
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page_content"=>$oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name ); 
	
			/* get open records */			
			$localHulp = $oFC->gsm_CreateBestel ( 1, $oFC->file_ref [ 99 ], $oFC->user );
			
			/* get data open proces record */
			$FieldArr = array ();
			$message = '';
			$oFC->page_content = array_merge ( $oFC->page_content, $oFC->gsm_accessRec ( $FieldArr, $oFC->page_content[ 'RECID' ], 1, 'proces' ) );
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug  ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
			
			/* get entries winkelwagen */
			$local_content = $oFC->gsm_AccessOrderLine ( $oFC->page_content [ 'payload' ], $oFC->page_content[ 'RECID' ], $oFC->page_content [ 'TOTALAMT' ], 1 );
			$oFC->page_content [ 'PAYLOAD' ] = $oFC->cal;
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug  ( array ( $oFC->page_content, $oFC->page_content[ 'RECID' ] ), __LINE__ . __FUNCTION__ );/*debug */ 
			break;
		/* mail */
		case "mailstatus":
			// omdat er geen $_post is zal er geen record ontstaan
			$FieldArr = array ();
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page" => $oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name );
		
			$oFC->page_content = array_merge ( $oFC->page_content, $oFC->gsm_accessRec ( $FieldArr, $oFC->page_content[ 'RECID' ], 1, 'proces' ) );
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( "page" => $oFC->page_content ), __LINE__ . $module_name );
			
			$parseArr = array ( );
			$parseArr [ 'GSM_EMAIL' ] 	= $_GET [ 'mail' ] ?? $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ];
			$parseArr [ 'GSM_NAME' ] 	= $page_content [ 'name' ] ?? "";
			$parseArr [ 'WEB_WEBMASTER' ] = $oFC->user [ 'name' ] ?? "webmaster" ;
			$parseArr [ 'GSM_BESTELLING' ] = $oFC->page_content [ 'proces_referentie' ] ?? ""; 
			$parseArr [ 'WEB_EMAIL' ] 	= $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ];
			$parseArr [ 'WEB_SITE' ] 	= LEPTON_URL;
			$LocalHulp = str_replace ("//", "/" ,LEPTON_PATH . $oFC->setting [ 'mediadir' ] ."/".$oFC->page_content [ 'ref' ] . "_doc.pdf" );
			if ( file_exists ( $LocalHulp ) ) {
				$parseArr [ 'GSM_LINK' ] 	=  str_replace( LEPTON_PATH, LEPTON_URL, str_replace ("//", "/" ,$LocalHulp ) );
			}
			$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );	
			$template="mail_orderstatus.php";
			$oFC->description .= $oFC->gsm_mail ( $template, $parseArr [ 'GSM_EMAIL' ] , $parseArr );

			$local_content = $oFC->gsm_AccessOrderLine ( $oFC->page_content [ 'payload' ], $oFC->page_content[ 'RECID' ], $oFC->page_content [ 'TOTALAMT' ], 1 );
			$oFC->page_content [ 'PAYLOAD' ] = $oFC->cal;
			break; 		
			/* end mail */
		default:
			gsm_debug ( array ( $_GET ?? "", $_POST ?? "", $oFC->page_content ), __LINE__ . __FUNCTION__ );
			break; 
	}
} else {
	/* first cycle */
	$oFC->page_content['P1'] = true;
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("page_content"=>$oFC->page_content , "xmode"=>$xmode ), __LINE__ . $module_name ); 
	
	/* get open records */
	$localHulp = $oFC->gsm_CreateBestel ( 1, $oFC->file_ref [ 99 ], $oFC->user );
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $localHulp ), __LINE__ . __FUNCTION__ );/*debug */ 
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
			
	/* get data open proces record */
	$FieldArr = array ();
	$message = '';
	$oFC->page_content = array_merge ( $oFC->page_content, $oFC->gsm_accessRec ( $FieldArr, $oFC->page_content[ 'RECID' ], 1, 'proces' ) );
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug  ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
	
	/* get entries winkelwagen */
	$local_content = $oFC->gsm_AccessOrderLine ( $oFC->page_content [ 'payload' ], $oFC->page_content[ 'RECID' ], $oFC->page_content [ 'TOTALAMT' ], 1 );
	$oFC->page_content [ 'PAYLOAD' ] = $oFC->cal;
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug  ( array ( $oFC->page_content, $oFC->page_content[ 'RECID' ] ), __LINE__ . __FUNCTION__ );/*debug */ 
}

/* attachment creatie */
if ( isset ( $oFC->page_content [ "ref" ] ) && 
	strlen ( $oFC->page_content [ "ref" ] ) > 5 &&
	$oFC->page_content [ 'voortgang' ] > 1 ) {
	$oFC->setting [ 'pdf_filename' ] = $oFC->page_content [ "ref" ] . "_doc.pdf"; 
	$pdflink = $oFC->gsm_print ( "", $oFC->language [ 'stappen' ] [ $oFC->page_content [ 'voortgang' ] ], "", 1, $oFC->setting [ 'mediadir' ]); 
	if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($pdflink ), __LINE__ . $module_name ); 
}

// selection 
switch (  $oFC->page_content [ 'MODE' ] ) {
	default:
		if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
		if (!isset ($oFC->page_content[ 'voortgang' ]) ) $oFC->page_content[ 'voortgang' ] = 1 ; 
		$oFC->page_content [ 'SELECTION' ] .= '<div class="ui basic segment">';
		if ($oFC->user [ 'privileged' ] < 3) {
			/* Knop klant save */
			$oFC->page_content [ 'SELECTION' ] .= ( isset ( $oFC->page_content [ 'voortgang' ] ) && $oFC->page_content [ 'voortgang' ] < 3 )
				? sprintf('<button class="ui submit %3$s button" name="command" value="%1$s" type="submit">%2$s</button>',
					'Verwerken',
					$oFC->language ['tbl_icon'][4], 
					'primary')
				: "";
			/* Knop klant bevestigen */
			$oFC->page_content [ 'SELECTION' ] .= ( isset ( $oFC->page_content [ 'voortgang' ] ) && $oFC->page_content [ 'voortgang' ] < 2 )
				? sprintf('<button class="ui submit %3$s button" name="command" value="%1$s" type="submit">%2$s</button>',
					'VerwerkenB',
					$oFC->language ['tbl_icon'][20], 
					'primary')
				: "";
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );
		}	
		if ($oFC->user [ 'privileged' ] > 2) {		
			/* Knop editor save */
			$oFC->page_content [ 'SELECTION' ] .= ( $oFC->user [ 'privileged' ] > 2 && isset ( $oFC->page_content [ 'voortgang' ] ) 
				&& in_array ( $oFC->page_content [ 'voortgang' ] , array ( 1, 2, 3, 4, 5, 6, 8 ) ) )
				? sprintf('<button class="ui submit %3$s button" name="command" value="%1$s" type="submit">%2$s</button>',
					'Verwerken',
					$oFC->language [ 'tbl_icon' ] [ 4 ], 
					'primary')
				: "";	
			/* Knop editor afwerken */	
			$oFC->page_content [ 'SELECTION' ] .= ( isset ( $oFC->page_content [ 'voortgang' ] ) 
				&& in_array ( $oFC->page_content [ 'voortgang' ] , array ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ) ) )
				? sprintf('<button class="ui submit %3$s button" name="command" value="%1$s" type="submit">%2$s</button>',
					'Verwerkenf',
					$oFC->language [ 'tbl_icon' ] [ 21 ], 
					'primary')
				: "";	
			if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );
		}
		/* select status */
		/* verwijderen niet geaccepteerde statussen */
		$localhulp = $oFC->language [ 'stappen' ];
		unset ( $localhulp [ 0 ] );
		unset ( $localhulp [ 1 ] );
		unset ( $localhulp [ 10 ] );
		unset ( $localhulp [ 11 ] );		
		unset ( $localhulp [ 13 ] );
		unset ( $localhulp [ 14 ] );	
		unset ( $localhulp [ 14 ] );	
		if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ ); 
		$statushulp = Gsm_option ( $localhulp, $oFC->page_content [ 'voortgang' ] ); 
		$oFC->page_content[ 'SELECTION' ] .= ( $oFC->user [ 'privileged' ]  >  2 ) 
			? sprintf( '<br /><br /><select name="%1$s">%2$s</select>',	'gsm_voortgang', $statushulp )
			: "";
		/* display status */
		$oFC->page_content['SELECTION'] .= ($oFC->user [ 'privileged' ]  <  1 && $oFC->page_content [ 'voortgang' ] <9 ) 
			? "<br /><br />".$oFC->language [ 'stappen' ] [ $oFC->page_content [ 'voortgang' ] ]
			: "";						
		/* document */	
		if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );/*debug */ 
		if ( strlen( $oFC->setting [ 'pdf_filename' ] ?? "" ) > 5 ) {
			$LocalHulp = str_replace ("//", "/" ,LEPTON_PATH . $oFC->setting [ 'mediadir' ] ."/".$oFC->setting [ 'pdf_filename' ] );
			if ( file_exists ( $LocalHulp ) ) {
				$LocalHulp = str_replace( LEPTON_PATH, LEPTON_URL, str_replace ("//", "/" ,$LocalHulp ) );
				$oFC->page_content['SELECTION'] .= sprintf('<br /><br /><a target="_'.__LINE__.'" href="%1$s"><i class="big blue file pdf icon"></i></i>%2$s</a>', 
					$LocalHulp, $oFC->setting [ 'pdf_filename' ] );
				/*	mail */	
				if ($oFC->user [ 'privileged' ] > 2) {				
					$oFC->page_content['SELECTION'] .= sprintf ( '<br /><a href="%1$s&module=%2$s&command=mailstatus&recid=%3$s&mail=%4$s" ><i class="big red envelope outline icon"></i> mail to : %4$s</a>',			
						$oFC->page_content [ 'RETURN' ], 
						$module_name,
						$oFC->page_content[ 'RECID' ],
						( isset ($oFC->page_content [ 'email' ]) &&strlen ( $oFC->page_content [ 'email' ]  ) > 10 ) 
							? $oFC->page_content [ 'email' ]  
							: $oFC->user [ 'email' ] ?? $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ]);
					/* einde mail */
				}
			}
		}
		/* eind knoppen */
		$oFC->page_content [ 'SELECTION' ] .= '</div>';
		if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ ); 
		break;
}
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ( $oFC->page_content	), __LINE__ . __FUNCTION__ );

/*  89 output processing */
if ( in_array ( $oFC->page_content [ 'voortgang' ] , array ( 0, 1) ) && $oFC->page_content[ 'PRIVILEGED' ] == 3 ) { 
	$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = 'active'; 
} else { 
	$oFC->page_content [ 'REFERENCE_ACTIVE3' ] = 'active'; 
}
 
/* 95 output processing */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( ); 

/* 96 output processing */

$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 

$oFC->page_content[ 'VERSIE' ] = $oFC->version; 

if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

/* 98 actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 