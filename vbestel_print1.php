<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$this->version ['print'] = "20250203";	

/* preset input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "onderhoud", "overzicht", $project ) ) );
$keuze = strtolower ( $query ) ;
$search_mysql = "";
$subtitel = "Overzicht";
$prmode = 1;

/* elvaluate input * /
if ( !isset ( $this->setting [ 'recid_list' ] ) ) { 
	$sw_eentjes = array();
} else {
	$sw_eentjes = is_array ( $this->setting [ 'recid_list' ] ) ? $this->setting [ 'recid_list' ] : array ( $this->setting [ 'recid_list' ] );
}
$sw_eentje = count ( $sw_eentjes);

if ( strstr (  $selection , 'print' ) ) {
	$selection= str_replace ("print", "", $selection );
	$subtitel = "Overzicht";
	$prmode = 1;
} 
if ( strstr (  $selection , 'plus' ) ) {
	$selection = str_replace ("plus", "", $selection );
	$subtitel = "Detailed overzicht";
	$prmode = 2;
}
if ( strstr ( $selection, 'all' ) ) {
	$selection = str_replace ("all", "", $selection );
	$subtitel = "Detailed overzicht";
	$prmode = 3;
}
if ($sw_eentje > 0 ) {
	$subtitel = sprintf ( 'Overzicht ( %s )', $sw_eentje ) ;
	$prmode = 9;
}

if ( strlen ( $keuze ) > 1 ) $search_mysql .= $this->search_mysql;

/* einde elvaluate query en keywords */

/* debug * / gsm_debug ( array ( 
	"this" => $this,
	"query" => $query,
	"project" => $project,
	"selection" => $selection ), __LINE__ . $this->version ['print'] ); /* einde debug */
/* debug * / gsm_debug ( array ( 
	"naam overzicht"  => $title,
	"file_name" => $this->setting [ 'pdf_filename' ],
	"func" => $func,	
	"loc" => $loc,	
	"query" => $search_mysql,
	"titel" => $subtitel,
	"functie ladder" => $prmode,
	"owner" => $owner,	
	"run" => $run ), __LINE__ . $this->version ['print'] ); /* einde debug */

global $owner;
$owner = $owner;
global $title;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf_data   = array( );
$pdf_text   = '';

/* adres leverancier */
$AdresArr = array ();
$AdresArrSel = array ( 9, 11, 12, 3, 7, 6, 8, 10 );
foreach ( $AdresArrSel as $key) {
	$localHulp = $this->setting [ 'droplet' ] [ LANGUAGE . $key ] ?? "";
	if (strlen ( $localHulp ) > 3 ) $AdresArr [] = $localHulp;
}
$hulpAdres1 = implode ( "\n", $AdresArr );

/* email adres */
$hulpAdres3 = ( strlen( $this->page_content [ 'email' ] ) > 5 ) ? sprintf ( "e-mail: %s " , $this->page_content [ 'email' ] ) : "";	
	
/* adres klant */

$hulpAdres2 = ( strlen( $this->page_content [ 'content_short' ] ) > 5 ) ? $this->page_content [ 'content_short' ] : "contante verkoop / afhalen";
	
/* toelichting klant */	

$LocalHulp = nl2br ( $this->page_content [ 'content_long' ] ?? "" );
$toelichtingArr = explode( '<br />', $LocalHulp );

$pdf->AdresBlock (
	$hulpAdres2,
	"",
	$hulpAdres3,
	"",
	$hulpAdres1,
	sprintf ( "Ref : %s\nDatum: %s", 
		$this->page_content[ 'ref' ], 
		$this->gsm_sanitizeStrings ( date ( "d M Y", time ( ) ), "s{ DATUM }" ) ) );
		
/*	tot zover de kop van het document */

//	$z=0;
	// initialiseren totalen 
$n=0; 							// aantal regels
$sub_prod = 0; 					// aantal producten	
$nix18 = 0;		
$sub_colli = 0;
$sub_return = 0;
$sub_totaal = 0;
$sub_factuur = 0;
$sub_discount = 0;
$sub_btw = array();
$sub_type = array ();
$sub_ex = 0;	
$prmode = $this->page_content [ 'voortgang' ];
$pdf_cols = array( 5, 20, 100, 20, 20, 20 ); 
$pdf_data  = array( );
$pdf_text   = '';

if ( in_array ( $prmode, array ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_header = array( " ", "art", "Producten / Diensten", "aantal", "prijs", "");
	$pdf_cols = isset($pdf_cols) ?  $pdf_cols : array( 90, 5, 45, 0, 0, 0 ); 
} else {
	$pdf_header = array( " ", "art", "Producten / Diensten", " ", " ", "aantal");
	$pdf_cols = isset($pdf_cols) ?  $pdf_cols : array( 90, 5, 45, 0, 0, 0 ); 
}
	
foreach ( $this->cal as $rowp ) {
	$result = array ();
	$database->execute_query ( 
		sprintf ( "SELECT * FROM `%s` WHERE `ref` = '%s'", $this->file_ref [ '98' ], $rowp [ 'art'] ), 
		true, 
		$result ); 
	$rowa = current ( $result );	
	$n++;
	$hulpart = $rowp [ 'art' ] ;  								// artikelnummer
	$hulpnam = $rowa [ 'name' ] ?? $rowp [ 'nam' ] ?? "--"; 	// artikelnaam
	$hulpprice = $rowp [ 'val' ] ?? 1000; 		 				// artikelprijs amt2'
	$hulpqty = $rowp [ 'qty' ] ?? 1; 		 					// artikelaantal
	$hulpreturn = $rowp [ 'ret' ] ?? 0;							// statiegel per collie /amt4'
	$hulptype = $rowa [ 'type' ] ?? 0;							// type
	$hulphuur = "";
	if ( isset ( $rowa [ 'active' ]) && $rowa [ 'active' ] == '3' ) $hulphuur = "(huur)";	// huur
	$hulpvat = $rowa [ 'amt0' ] ?? 0;  							// btw pct
																// amt3'  costprijs ex btw
																// amt2'  onze prijs / verhuurprijs  ex btw
																// amt1' advies verkoopprijs incl btw 
	/* totalen deze regel */
	$hulpomzet = $hulpqty * $hulpprice;  						// bruto omzet
	$hulptot = $hulpqty * $hulpprice;  							// netto omzet incl
	$hulptotex = $hulptot / ( 100 + $hulpvat ) * 100;			// netto omzet excl btw
	$hulpbtw = $hulptot - $hulptotex; 							// btw bedrag over netto omzet
	$hulpstatie =  $hulpqty * $hulpreturn; 						// statiegeld

	/* korting deze regel */
	$hulpvoor = $hulpprice * $hulpqty; 
	$hulpvan = $hulpvoor;
	if ( isset ( $rowa [ 'amt1' ]) ) $hulpvan = $hulpqty * $rowa [ 'amt1' ] ;  		// listprijs * aantallen 
	$hulpvoor = $hulpprice * $hulpqty; 							// actuele prijs *aantallen 
	$hulpdisc = $hulpvan - $hulpvoor;							// regel korting 
	
	/* subtotalen */	
	$sub_ex += $hulptotex;
	$sub_ex += $hulpstatie;
	if ( $hulpqty > 0 ) { 
		$sub_colli += $hulpqty;	
	} else {
		$sub_return -= $hulpqty;	
	}
	$sub_totaal += $hulpomzet;
	$sub_totaal += $hulpstatie;			
//	$sub_factuur += $hulptot;
//	$sub_factuur += $hulpstatie;
	$sub_discount += $hulpdisc;
	
	/* subtotal btw	*/
	if (isset (	$sub_btw [ $hulpvat ] ) ) {
		$sub_btw [ $hulpvat ] += $hulptotex;
	} else {
		$sub_btw [ $hulpvat ] = 0;
		$sub_btw [ $hulpvat ] += $hulptotex;
	}			
	if (isset (	$sub_btw [ 0.0 ] )) {
		$sub_btw [ 0.0 ] += $hulpstatie;
	} else {
		$sub_btw [ 0.0 ] = 0;
		$sub_btw [ 0.0 ] += $hulpstatie;
	}
	
	/* subtotal per type */
	if (isset (	$sub_type [ $hulptype ] ) ) {
		$sub_type [ $hulptype ] += $hulptotex;
	} else {
		$sub_type [ $hulptype ] =0;
		$sub_type [ $hulptype ] += $hulptotex;
	}	

	/* Uitvoeren */	
	if ( $hulpdisc > 0 ) {
		if (in_array($prmode, array ( 16 ) ) ) {
			/* expliciet discount */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				$n,	
				$hulpart, 
				$hulphuur . $this->gsm_sanitizeStrings ($hulpnam, "s{toasc}"),
				$rowp [ 'qty'],
				$this->gsm_sanitizeStrings ( $rowa [ 'amt1' ], "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $rowa [ 'amt1' ] * $rowp [ 'qty'], "s{KOMMA|E128}" ) ) ) );
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',
				'',
				"marge / korting ",
				'',
				'',
				$this->gsm_sanitizeStrings ( $hulpdisc * -1, "s{KOMMA|E128}" ) ) ) );
		} else {
			/* discount niet expliciet */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				$n,	
				$hulpart, 
				$hulphuur . $this->gsm_sanitizeStrings ($hulpnam, "s{toasc}"),
				$rowp [ 'qty'],
				$this->gsm_sanitizeStrings ( $hulpprice, "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $hulpomzet, "s{KOMMA|E128}" ) ) ) );	
		}
	} else {
		if (in_array($prmode, array ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
			/* geen discount */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				$n,	
				$hulpart, 
				$hulphuur . $this->gsm_sanitizeStrings ($hulpnam, "s{toasc}"),
				$rowp [ 'qty'],
				$this->gsm_sanitizeStrings ( $hulpprice, "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $hulpomzet, "s{KOMMA|E128}" ) ) ) );	
		} else {
			/* alleen  aantallen */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				$n,	
				$hulpart, 
				$hulphuur . $this->gsm_sanitizeStrings ($hulpnam, "s{toasc}"),
				'',
				'',
				$rowp [ 'qty'] ) ) );	
		}
	}
	/* Uitvoeren btw regel * /	
	if ( $hulpbtw > 0 ) {
		if (in_array($prmode, array ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',
				'',
				sprintf ( "incl. %s btw", $this->gsm_sanitizeStrings ( $hulpbtw, "s{KOMMA|E128}" ) ) ,
				'',
				'',
				'') ) );
		}
	}
	/* Uitvoeren statiegeld regel */
	if ( $hulpstatie >0 ) {
		if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',	
				'', 
				"Statiegeld ",
				'',
				$this->gsm_sanitizeStrings ( $hulpreturn, "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $hulpstatie, "s{KOMMA|E128}" ) ) ) );	
		} else {
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',	
				'', 
				"Statiegeld ",
				'',
				$this->gsm_sanitizeStrings ( $hulpreturn, "s{KOMMA|E128}" ),
				"" ) ) );	
		}
	}		
}
/* spatieregel */
if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) 
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",'','','','','','' ) ) );

$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data = array( );
$pdf_header = array( " ", "", "Totalen", " ", " ", "");
/* Totalen */
/* korting * /
if ( $sub_discount > 0 ) {
	if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
		$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
			'',
			'',
			"Toegepaste extra Marge / Korting ",
			'',
			$this->gsm_sanitizeStrings ( $sub_discount, "s{KOMMA|E128}" ),
			'' ) ) );
	}
}

/* btw */
krsort ( $sub_btw);
foreach ( $sub_btw as $key => $value ) {
	if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
		if ( ($value * $key) != 0 ) {
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',
				'',
				sprintf ("BTW %s over %s", $this->gsm_sanitizeStrings ( $key, "s{KOMMA}" ), $this->gsm_sanitizeStrings ( $value, "s{KOMMA|E128}" )),
				$this->gsm_sanitizeStrings ( $value * $key / 100, "s{KOMMA|E128}" ),
				'',
				'' ) ) );
		}
	}
}

if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'',
		'',
		'',
		"----------",
		'',
		'' ) ) );
}

if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'',
		'',
		"Totaal BTW ",
		$this->gsm_sanitizeStrings ( $sub_totaal - $sub_ex, "s{KOMMA|E128}" ),
		'',
		'') ) );
}

if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'',
		'',
		"Totaal ex BTW ",
		$this->gsm_sanitizeStrings ( $sub_ex, "s{KOMMA|E128}" ),
		'',
		'') ) );
}

/* te betalen */
if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'',
		'',
		"Totaal te betalen",
		'',
		'',
		$this->gsm_sanitizeStrings ( $sub_totaal, "s{KOMMA|E128}" ) ) ) );
}	

/* spatieregel */
if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) 
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",'','','','','','' ) ) );

/* aantal orderregels */
if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 14, 15 ) ) ) {
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'',
		'',
		"Aantal Orderregels ",
		$n,
		'',
		'' ) ) );
}

/* aantal colli */
if ( $sub_colli > 0 ) { 
	if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) {
		$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
			'',
			'',
			"Aantal Colli ",
			$sub_colli,
			'',
			'' ) ) );
	}
}

/* aantal retouren */
if ( $sub_return > 0 ) { 
	if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) {
		$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
			'',
			'',
			"Aantal Retouren ",
			$sub_return,
			'',
			'' ) ) );
	}
}

/* spatieregel */
if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) 
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",'','','','','','' ) ) );

if (in_array($prmode, array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) ) {
	foreach ( $this->language [ $prmode."stap" ] as $value)  $pdf_text .= sprintf ( $value , 
		$this->setting [ 'droplet' ] [ LANGUAGE . "13" ] ?? "", 
		$this->setting [ 'droplet' ] [ LANGUAGE . "14" ] ?? "",  
		$this->page_content[ 'ref' ],
		$this->gsm_sanitizeStrings ( $sub_totaal, "s{KOMMA|E128}" ) );
}	

if ( strlen ( $this->page_content [ 'content_long' ] ?? "") > 2 ) {
	$pdf_text .= "\n\n" . "Toelichting:" . "\n" ;
	foreach ( $toelichtingArr as $value) {
		if ( strlen ( trim ( $value ) ) > 2 ) $pdf_text .= $value;
	}
}

/* opslaan berekeningsresultaat	*/
if ( in_array( $prmode, array ( 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ) ) )  {
	$updarray = array ();
	/* bedrag ex btw */
	if ( $this->page_content[ 'amt1' ] != $sub_ex ) $updarray [ 'amt1' ] = $sub_ex;  
	/* bedrag te betalen */
	if ( $this->page_content[ 'amt2' ] != $sub_totaal ) $updarray [ 'amt2' ] = $sub_totaal ;  
	/* bedrag te betaald */
	if ( in_array( $prmode, array ( 11, 12 ) ) )  {
		if ( $this->page_content[ 'amt2' ] != $sub_totaal ) $updarray [ 'amt2' ] = $sub_totaal ; 
	}
	if (count ( $updarray ) > 0 ) {
		$database->build_and_execute ( 
			"update",
			$this->file_ref [ '99' ],
			$updarray,
			"`ref` = '" . $this->page_content[ 'ref' ] . "'" );
	}
}

/* end */
$subtitel = "een voorbeeld";
// last pdf output
$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data = array( );
if ( $pdf_text != "" ) $pdf->ChapterBody( $pdf_text );
$pdf_text   = '';

?>