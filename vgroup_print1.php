<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print1'] = "20241117";	
if ($this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( "post" =>$_POST ?? "", "get" =>$_GET ?? "", "this" => $this ), __LINE__ . 'print1' .$this->version ['print1'] ); 

/* preset input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "Onderhoud", "Overzicht", $project ) ) );
$keuze = strtolower ( $query ) ;
$search_mysql = "";
$prmode = 1;

// $this->setting [ 'debug' ] = "yes";

/* welke data */
$prtype = array (); //details op record niveau
$prmode = array (); //details op item niveau

if ( strstr ( $selection, "print" ) ) {
	$prtype = 1; // default
	$prmode = 1; // default
	$subtitel = "Assortiment..";
}
if ( strstr ( $selection, "all" ) )   {	
	$prtype = 2; // details
	$prmode = 2; // detail gegevens
	$subtitel = "Detailed overzicht..";
}
if ( strstr ( $selection, "debug" ) )   {	
	$prtype = 2; // debug material
	$prmode = 3; // debug gegevens
	$subtitel = "Detailed overzicht..";
}

$search_mysql .= $this->search_mysql ?? '';

if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( 
	"naam overzicht"  => $title,
	"file_name" => $this->setting [ 'pdf_filename' ],
	"func" => $func,	
	"loc" => $loc,	
	"query" => $query,
	"project" => $project,
	"mysql" => $search_mysql,
	"titel" => $subtitel,
	"selection" => $selection, 
	"functie ladder rec" => $prmode,
	"type ladder rec" => $prtype,
	"owner" => $owner,	
	"run" => $run ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */

if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( "this" => $this ), __LINE__ . 'print1' .$this->version ['print1'] ); 

$j = 0;
$pdf_text = '';
/* initialise page  */
$pdf->AliasNbPages();
$pdf->AddPage();
$j++;
$pdf->ChapterTitle( $j, $subtitel );
$pdf_cols = array( 10, 50, 90, 30, 10, 0 );
$pdf_header = array( '', '', '', '', '', '' );

$pdf_data = array( );
$pdf_data_2nd = array( );	
$pdf_text = '';


if ( in_array ( $prmode, array ( 1 ) ) ) {
	if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`active` > '0' ", $search_mysql, $this->file_ref [ 99 ] ); 
	} else { 
		$search_mysql = sprintf ( " WHERE `%s`.`active` > '0' ", $this->file_ref [ 99 ] ); 
	}
}

/* single file */
if ( !isset ( $this->file_ref [ 98 ] ) ){
	$query  = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` ";
	$query .= sprintf ( "%s ORDER BY `type`, `zoek` ASC ", $search_mysql );
}

/* grouped * /
if ( isset ( $this->file_ref [ 98 ] ) ) {
	$query  = sprintf ("SELECT `%s`.*, ", $this->file_ref [ 99 ] );
	$query  .= sprintf ("`%s`.`type` AS `grptype`, ", $this->file_ref [ 98 ] );
	$query  .= sprintf ("`%s`.`ref`  AS `grpref`,  ", $this->file_ref [ 98 ] );
	$query  .= sprintf ("`%s`.`amt1` AS `grpamt1`, ", $this->file_ref [ 98 ] );
	$query  .= sprintf ("`%s`.`name` AS `grpname`  ", $this->file_ref [ 98 ] );	
	$query  .= sprintf (" FROM `%s`	LEFT JOIN `%s` ", 
		$this->file_ref [ 99 ], 
		$this->file_ref [ 98 ] );
	$query  .= sprintf (" ON `%s`.`type` = `%s`.`id` ", 
		$this->file_ref [ 99 ], 
		$this->file_ref [ 98 ] );
	$query .= sprintf ( " %s ORDER BY `%s`.`ref`, `%s`.`zoek` ASC ", 
		$search_mysql , 
		$this->file_ref [ 98 ], 
		$this->file_ref [ 99 ] );
}
/* end grouped */        

if ( in_array ( $prtype, array ( 1, 2, 3 ) ) ) {
	/* query for lijst */
	$results = array();
	$database->execute_query( 
		$query, 
		true, 
		$results);
	$PL3 = "=="; 
	
	if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $results , __LINE__ . 'print1' .$this->version ['print1'] ); 
	
	if ( count ( $results ) > 0 ) { 
		foreach ( $results as  $result ) {
			$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
				"|" . $result	[ 'id' ] . "|",
				$result [ 'ref' ], 
				$result [ 'name' ], 
				$result [ 'type' ], 
				$result [ 'active' ],	
				'') ) );
	
			$unset_fields = array ( 'zoek','updated', 'name', 'type', 'ref', 'amt0', 'amt2', 'amt3', 'amt4', 'amt5', 'amt6'); 
			$rename_fields = array (
			);

			if ( in_array ( $prmode, array ( 1 ) ) ) {
				foreach ( $unset_fields as $each )  unset ( $result [ $each ] );
			}
			if ( in_array ( $prmode, array ( 2 ) ) ) {
				$pdf_data_2nd [ ] = explode ( ';', trim ( sprintf ( "%s;%s;%s;%s;%s;%s", 
					$result [ 'type' ], 
					$result [ 'ref' ], 
					$result [ 'name' ], 
					'', '',	'') ) );
				foreach ( $unset_fields as $each )  unset ( $result [ $each ] );
				foreach ( $result as $pay => $load ) {
					$localHulp = strip_tags( html_entity_decode( $load ) );
					if ( $pay == 'type' && isset ( $this->cal ) ) $localHulp = $this->cal [ $load ] ?? $load;		
					if ( $localHulp != "" ) {
						$pdf_data_2nd [ ] = explode( ';', trim ( sprintf( " ;%s;%s;%s;%s;%s", 
							$pay, 
							$localHulp, 
							'', '', '') ) );
					}
				}
				$pdf_data_2nd [ ] = explode ( ';', trim ( sprintf ( " ;%s;%s;%s;%s;%s", '----', '', '', '', '') ) );
			}
			if ( in_array ( $prmode, array ( 3 ) ) ) {
				$pdf_data_2nd [ ] = explode ( ';', trim ( sprintf ( "%s;%s;%s;%s;%s;%s", 
					$result [ 'type' ], 
					$result [ 'ref' ], 
					$result [ 'name' ],  
					'', '',	'') ) );
				foreach ( $result as $pay => $load ) {

					$localHulp = strip_tags( html_entity_decode( $load ) );
					if ( $pay == 'type' && isset ( $this->cal ) ) $localHulp = $this->cal [ $load ] ?? $load;		
					$pdf_data_2nd [ ] = explode( ';', trim ( sprintf( " ;%s;%s;%s;%s;%s", 
						$pay, 
						$localHulp, 
						'', '', '') ) );
				}
				$pdf_data_2nd [ ] = explode ( ';', trim ( sprintf ( " ;%s;%s;%s;%s;%s", '----', '', '', '', '') ) );
			}
		}	
	}
	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data = array( );
	if ( in_array ( $prtype, array ( 2, 3 ) ) ) {
		$pdf->AddPage();
		$j++;
		$subtitel = "Details";
		$pdf->ChapterTitle( $j, $subtitel );
		$pdf_cols = array( 20, 50, 90, 30, 0, 0 );
		$pdf_header = array( '', '', '', '', '', '' );
		$pdf->DataTable( $pdf_header, $pdf_data_2nd, $pdf_cols );
		$pdf_data_2nd = array( );
	}
	$pdf_data = array( );
}

$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
if ( strlen ( $selection ) > 1 ) $pdf_text .= sprintf ( "\n" . "Options %s : %s " , $project, $selection );

if ( $this->setting [ 'debug' ] == "yes" ){
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}

// pdf output
$pdf->ChapterBody( $pdf_text );

?>