<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$this->version ['print1'] = "20240108";	

/* preset input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "onderhoud", "overzicht", $project ) ) );
$subtitel = "Dag Overzicht";
$subtitel2 = "Maand Overzicht";
$prmode = 1;

/* date range */
$cleanedSelection = "today";
$dateString = strtotime ( $cleanedSelection );
$monthStart = date( 'Y-m-01 00:00:00',  $dateString );
$month = 'P'. date_format ( date_create ( $monthStart ), 'U');
$monthEnd = date( date ( "Y-m-d", $dateString ).' 23:59:59');
$monthE = 'P'.date_format(date_create($monthEnd), 'U');
$title .= sprintf ( '  %s - %s.' , substr ( $monthStart, 0, 10) , substr ( $monthEnd, 0, 10) );

/* debug * / gsm_debug ( array ( "post" =>$_POST ?? "", "get" =>$_GET ?? "", "this" => $this ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */
/* debug * / gsm_debug ( array ( 
	"naam overzicht"  => $title,
	"file_name" => $this->setting [ 'pdf_filename' ],
	"func" => $func,	
	"loc" => $loc,	
	"selection" => $selection,
	"titel" => $subtitel,
	"functie ladder" => $prmode,
	"project" => $project,
	"owner" => $owner,	
	"run" => $run ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */

global $owner;
$owner = $owner;
global $title;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf_data   = array( );
$pdf_text   = '';

/* get process records */
$proces_result = array ();
$sql = sprintf ("SELECT * FROM `%s` WHERE `ref` > '%s'  AND `ref` < '%s' ORDER BY `ref` DESC " ,
		$this->file_ref [ 99 ],
		'P' . date_format ( date_create ( $monthStart ), 'U' ) -1,
		'P' . date_format ( date_create ( $monthEnd ), 'U' ) +1 );
$database->execute_query ( 
	$sql,		
	true, 
	$proces_result ); 
$regelcount = count ( $proces_result );
if ( $regelcount < 1 ) $pdf_text .= "\n\n" .  $this->language [ 'pdf' ][9];

/* ophalen artikel groepen */
$results = array ( );
$GroepArr = array( 99 => "Emballage" );
$GroepBTWArr = array( 99 =>  "0.00" );
$database->execute_query ( 
	sprintf ("SELECT * FROM `%s` ORDER BY `id`", $this->file_ref  [ 97 ]),
	true, 
	$results );
if ( count($results) > 0 ) {
	foreach ( $results as $row ) {
	$GroepArr [ $row [ 'id' ] ] = $row [ 'name' ];
	$GroepBTWArr [ $row [ 'id' ] ] = $row [ 'amt1' ];
} 	}
/* debug * / gsm_debug ( array ( sprintf ("SELECT * FROM `%s` ORDER BY `id`", $this->file_ref  [ 97 ]), $GroepArr , $GroepBTWArr), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */

/*Initialisering L0*/

$arr_doet_niet_mee = array ( 0, 1, 2, 14, 15 );
$arr_factuur = array ( 5, 6, 7, 8, 9, 10, 13 );
$arr_proforma = array ( 5, 7, 9, 13 );
$arr_bank = array ( 4, 12  );
$arr_kas = array ( 3, 11   );
$this->cols[ 'L0' ] = true;

/* cycle through the process records */
foreach ( $proces_result as  $rowp ) {
	if ( $rowp [ 'active' ] < 1 ) continue; // skip not active records
	if ( in_array ( $rowp [ 'voortgang' ], $arr_doet_niet_mee ) ) continue; // skip certain voortgang types
	/* **************** Initialisering L0*/
	if ( $this->cols[ 'L0' ] ) {
		$pdf_header = array( "Bon", "Uitgifte", "Omschrijving", "Factuur", "Bank", "Kas");
		$pdf_cols = isset($pdf_cols) ?  $pdf_cols : array( 25, 35, 55, 25, 25, 25 ); 
		$pdf_header2 = array( "", "", "", "Bedrag", "", "");
		$pdf_cols2 = isset($pdf_cols2) ?  $pdf_cols : array( 25, 65, 35, 25, 15, 25 ); 
		$this->cols[ 'L0' ] = false;
		$this->cols[ 'L1' ] = "--";
		$this->cols[ 'L2' ] = "--";
	}
	/* **************** Afsluiten L2 */
	$L2_break = date ( "d M Y", substr ( $rowp [ 'ref' ] ,1 ) );
	if ( $this->cols[ 'L2' ] != $L2_break ) {
		if ( count ($pdf_data) > 0 ) {
		
			/* factuur */
			$LocalAmt1 = 0;		
			foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
				if ( in_array ( $pay, array ( 5, 7, 9, 13 ) ) ) $LocalAmt1 += $load; 
			}

			/* bank */
			$LocalAmt2 = 0;		
			foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
				if ( in_array ( $pay, array ( 4, 12  ) ) ) $LocalAmt2 += $load; 
			}

			/* kas */
			$LocalAmt3 = 0;		
			foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
				if ( in_array ( $pay, array (  3, 11   ) ) ) $LocalAmt3 += $load; 
			}

			/* L2 = dag totaal gesplitst naar factuur, bank of kas */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'',	
				'', 
				"Totaal (". $this->gsm_sanitizeStrings ( $LocalAmt1 + $LocalAmt2 +$LocalAmt3, "s{KOMMA|E128}" ).")",
				$this->gsm_sanitizeStrings ( $LocalAmt1, "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $LocalAmt2, "s{KOMMA|E128}" ),
				$this->gsm_sanitizeStrings ( $LocalAmt3, "s{KOMMA|E128}" ) ) ) );
			/* L1 = month totaal 	*/
			foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) $this->cols[ 'L1_PT' ] [ $pay ] += $this->cols[ 'L2_DT' ] [ $pay ];
			for ($x = 0; $x <= 16; $x++) $this->cols[ 'L2_DT' ] [ $x ] = 0;		

			/* output */
			$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
			$pdf_data = array( );			
			$pdf_text .= "\n";
		}
	}
	/* **************** Afsluiten L1 */
	$L1_break = date ( "M Y ", substr ( $rowp [ 'ref' ] ,1 ) );	
	if ( $this->cols[ 'L1' ] !=  $L1_break ) { //if ( trim($L1) != trim($L1_break) ) {
		if ( isset ( $this->cols [ 'L1_KG' ] ) ) {
			$pdf->AddPage();
			$pdf->ChapterBody( $pdf_text );
			$pdf_text .= " ";
			$pdf->ChapterTitle( $this->cols[ 'L1' ], $subtitel2 );
			$PeriodeTotaal = 0;
			
			/* contante betalingen */
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'Kontant',	
				'in Kas', 
				'',
				'',
				'',
				'') ) );
			if ( count ( $this->cols [ 'L1_KG' ] ) > 1) ksort ( $this->cols[ 'L1_KG' ] );
			foreach ( $this->cols [ 'L1_KG' ] as $pay => $load )
				if ( $load != 0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
						'',	
						substr ($GroepArr [ $pay ] ?? "", 0, 40),
						'',
						$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
						'',
						'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
				}
			foreach ( $this->cols [ 'L1_KB2' ] as $pay => $load )				
				if ( $load != 0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
						'',	
						'BTW:  ', 
						$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
						$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
						'over',
						$this->gsm_sanitizeStrings ( $this->cols[ 'L1_KB' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
				}	
			$sub_totaal	= array_sum ( $this->cols [ 'L1_KG' ] ) + array_sum ( $this->cols [ 'L1_KB2' ] );
			$PeriodeTotaal += $sub_totaal;
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				'',	
				'Totaal', 
				$this->gsm_sanitizeStrings ( $sub_totaal, "s{KOMMA|E128}" ),
				'',
				'' ) ) );
				
			/* bank betalingen */
			$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
			$pdf_data = array( );
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'Bank',
				'Pin/Tikkie',	
				'', 
				'',
				'',
				'') ) );
			if ( count ( $this->cols [ 'L1_BG' ] ) > 1) ksort ( $this->cols[ 'L1_BG' ] );
			foreach ( $this->cols [ 'L1_BG' ] as $pay => $load )
				if ( $load != 0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
						'',	
						substr ($GroepArr [ $pay ] ?? "", 0, 40),
						'',
						$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
						'',
						'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
				}
			foreach ( $this->cols [ 'L1_BB2' ] as $pay => $load )				
			if ( $load != 0 ) {
				$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
					'',	
					'BTW:  ', 
					$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
					$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
					'over',
					$this->gsm_sanitizeStrings ( $this->cols [ 'L1_BB2' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
			}	
			$sub_totaal	= array_sum ( $this->cols [ 'L1_BG' ] ) + array_sum ( $this->cols [ 'L1_BB2' ] );
			$PeriodeTotaal += $sub_totaal;
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				'',	
				'Totaal', 
				$this->gsm_sanitizeStrings ( $sub_totaal , "s{KOMMA|E128}" ),
				'',
				'' ) ) );
					
			/* pro-forma */
			$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
			$pdf_data = array( );
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				'Pro-forma',
				'(te factureren)',	
				'', 
				'',
				'',
				'') ) );
			if ( count ( $this->cols [ 'L1_FG' ] ) > 1) ksort ( $this->cols[ 'L1_FG' ] );
			foreach ( $this->cols [ 'L1_FG' ] as $pay => $load ) 
				if ( $load != 0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
						'',	
						substr ($GroepArr [ $pay ] ?? "", 0, 40),
						'',
						$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
						'',
						'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
				}
			foreach ( $this->cols [ 'L1_FB2' ] as $pay => $load )				
				if ( $load != 0 ) {
					$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
						'',	
						'BTW:  ', 
						$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
						$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
						'over',
						$this->gsm_sanitizeStrings ( $this->cols [ 'L1_FB' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
				}	
			$sub_totaal	= array_sum ( $this->cols [ 'L1_FG' ] ) + array_sum ( $this->cols [ 'L1_FB2' ] );
			$PeriodeTotaal += $sub_totaal;
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				'',	
				'Totaal', 
				$this->gsm_sanitizeStrings ( $sub_totaal , "s{KOMMA|E128}" ),
				'',
				'' ) ) );	
				
			$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
			$pdf_data = array( );
				
			$pdf_header3 = array( "", 
				"Periode Totaal", 
				"", 
				$this->gsm_sanitizeStrings ( $PeriodeTotaal , "s{KOMMA|E128}" ),
				"", 
				"");
			$pdf->DataTable( $pdf_header3, $pdf_data, $pdf_cols2 );
			$pdf_data = array( );
			$pdf->AddPage();
		}
	}
	/* **************** Initialisering L1 */	
	if ( $this->cols[ 'L1' ] !=  $L1_break ) {
		$this->cols[ 'L1' ] =  $L1_break ;
		/* openen maand */
		$this->cols [ 'L1_FB' ] = array();
		$this->cols [ 'L1_FG' ] = array();
		$this->cols [ 'L1_FB2' ] = array();	
		$this->cols [ 'L1_KB' ] = array();
		$this->cols [ 'L1_KG' ] = array();
		$this->cols [ 'L1_KB2' ] = array();
		$this->cols [ 'L1_BB' ] = array();
		$this->cols [ 'L1_BG' ] = array();
		$this->cols [ 'L1_BB2' ] = array();
		$this->cols [ 'L1_PT' ] = array();
		for ($x = 0; $x <= 16; $x++) $this->cols [ 'L1_PT' ] [ $x ] = 0;
		$this->cols [ 'L2_DT'] = array();
		for ($x = 0; $x <= 16; $x++) $this->cols [ 'L2_DT' ] [ $x ] = 0;
		$this->cols[ 'L2' ] = "--";
	}
	/* Initialisering L2 */
	if ( $this->cols[ 'L2' ] != $L2_break ) {
		$this->cols[ 'L2' ] = $L2_break;
		/* openen dag */
		$pdf->ChapterBody( $pdf_text );
		$pdf_text = '';
		$pdf->ChapterTitle( $L2_break, $subtitel );
	} 
	/* adres gegevens bij pro forma */
	if ( in_array ( $rowp [ 'voortgang' ], $arr_proforma ) ) {
		if ( strlen ( $rowp [ 'email' ] ) > 5 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				$rowp [ 'ref' ],	
				'',
				$rowp [ 'email' ],
				'', 
				'',
				'' ) ) );
		}
		if ( strlen ( $rowp [ 'content_short' ] ) > 5 ) {
			$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
				$rowp [ 'ref' ],	
				'',
				substr ( $rowp [ 'content_short' ],0 ,40),
				'', 
				'',
				'' ) ) );
		}
	}

	/* order regel processing */
	if ( in_array ( $rowp [ 'voortgang' ], array ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ,15 ) ) ) {
		if ( strlen ( $rowp [ 'payload' ] ) > 6 ) {
			$this->cal = array();
			$this->cal = json_decode ( $rowp [ 'payload'] , true );		
			/* debug * / gsm_debug ( array ( $this->cal ), __LINE__ . 'data1' .$this->version ['data1'] ); /* einde debug */
			foreach ( $this->cal as $regel) {
				$products = array ();
				$database->execute_query ( 
					sprintf ( "SELECT * FROM `%s` WHERE `ref`= '%s'", $this->file_ref  [ 98 ], $regel [ 'art' ] ), 
					true, 
					$products );
				if ( count ( $products ) > 0 ) {
					$product = current ( $products );
				} 
				$hulpexprice = round ( $regel [ 'val' ] * 100 / ( 100 + $product [ 'amt0' ] ), 2 ); 	// artikelexprijs
				$hulpbtw = $regel [ 'val' ] - $hulpexprice;  	// btw bedrag				
				/* totalen deze regel */
				/* debug * / gsm_debug ( array (  
					"ref" => $rowp [ 'ref' ],
					"voortgang" => $rowp [ 'voortgang' ],
					"artikelnummer" => $regel [ 'art' ],
					"artikelaantal" => $regel [ 'qty' ],
					"artikelnaam" => $regel [ 'nam' ],
					"productprijs incl" => $regel [ 'val' ],
					"productreturn" => $regel [ 'ret' ],
					"productprijs exl" => $hulpexprice,
					"productbtw" => $hulpbtw,
					"btw" => $product [ 'amt0' ],
					"groep" => $product [ 'type' ],
					"omzet"  => ( $regel [ 'val' ] + $regel [ 'ret' ] ) *  $regel [ 'qty' ],
					"omzetin" => $regel [ 'val' ] *  $regel [ 'qty' ],
					"omzetex" => $hulpexprice *  $regel [ 'qty' ], 
					"omzetbtw" => $hulpbtw *  $regel [ 'qty' ],
					"omzetret" => $regel [ 'ret' ] *  $regel [ 'qty' ]
					), __LINE__ . 'data1' .$this->version ['data1'] ); /* einde debug */		
				/* opmaak product regel */
				$Local1 = $this->gsm_sanitizeStrings ( $regel [ 'val' ] + $regel [ 'ret' ]  , "s{KOMMA|E128}" );
				$Local2 = ($regel [ 'ret' ] != 0 ) 
					? " emb: " . $this->gsm_sanitizeStrings ( $regel [ 'ret' ], "s{KOMMA}" ) 
					: "";
				$Local3 = ( $hulpbtw != 0 ) 
					? " btw: " . $this->gsm_sanitizeStrings ( $hulpbtw, "s{KOMMA}" ) 
					: "";				
				if ( in_array ( $rowp [ 'voortgang' ], $arr_proforma ) )  {	
				
					if ( !isset ( $this->cols [ 'L1_FG' ] [ $product [ 'type' ] ] ) ) $this->cols [ 'L1_FG' ] [ $product [ 'type' ] ] = 0;
					if ( !isset ( $this->cols [ 'L1_FG' ] [ '99' ] ) ) $this->cols [ 'L1_FG' ] [ '99' ] = 0;
					if ( !isset ( $this->cols [ 'L1_FB' ] [ '0.00' ] ) )$this->cols [ 'L1_FB' ] [ '0.00' ] = 0;					
					if ( !isset ( $this->cols [ 'L1_FB' ] [ $product [ 'amt0' ] ] ) )  $this->cols [ 'L1_FB' ] [ $product [ 'amt0' ] ] = 0;
					if ( !isset ( $this->cols [ 'L1_FB2' ] [ $product [ 'amt0' ] ] ) )  $this->cols [ 'L1_FB2' ] [ $product [ 'amt0' ] ] = 0;
					$this->cols [ 'L1_FG' ] [ $product [ 'type' ] ] += $hulpexprice * $regel [ 'qty' ];
					$this->cols [ 'L1_FB' ] [ $product [ 'amt0' ] ] += $hulpexprice * $regel [ 'qty' ];
					$this->cols [ 'L1_FB2' ] [ $product [ 'amt0' ] ] += $hulpbtw * $regel [ 'qty' ];
					$this->cols [ 'L1_FB' ] [ '0.00' ] = $regel [ 'ret' ] *  $regel [ 'qty' ];
					$this->cols [ 'L1_FG' ] [ '99' ] += $regel [ 'ret' ] * $regel [ 'qty' ];
					
					if ( in_array ( $rowp [ 'voortgang' ], $arr_proforma  ) ) 
						$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							$rowp [ 'ref' ],	
							sprintf ( "%s x ( %s )", $regel [ 'qty' ], $regel [ 'art' ] ),
							sprintf ( "%s (%s %s )", $Local1, $Local2 , $Local3 ),
							'', 
							'',
							$regel [ 'nam' ] ) ) );
				}
				if ( in_array ( $rowp [ 'voortgang' ], $arr_kas ) ) {
					if ( !isset ( $this->cols [ 'L1_KG' ] [ $product [ 'type' ] ] ) ) $this->cols [ 'L1_KG' ] [ $product [ 'type' ] ] = 0;
					if ( !isset ( $this->cols [ 'L1_KG' ] [ '99' ] ) ) $this->cols [ 'L1_KG' ] [ '99' ] = 0;
					if ( !isset ( $this->cols [ 'L1_KB' ] [ '0.00' ] ) ) $this->cols [ 'L1_KB' ] [ '0.00' ] = 0;	
					if ( !isset ( $this->cols [ 'L1_KB' ] [ $product [ 'amt0' ] ] ) )  $this->cols [ 'L1_KB' ] [ $product [ 'amt0' ] ] = 0;
					if ( !isset ( $this->cols [ 'L1_KB2' ]  [ $product [ 'amt0' ] ] ) )  $this->cols [ 'L1_KB2' ] [ $product [ 'amt0' ] ] = 0;
					$this->cols [ 'L1_KG' ] [ $product [ 'type' ] ] += $hulpexprice * $regel [ 'qty' ];
					$this->cols [ 'L1_KB' ] [ $product [ 'amt0' ] ] += $hulpexprice * $regel [ 'qty' ];
					$this->cols [ 'L1_KB2' ] [ $product [ 'amt0' ] ] += $hulpbtw * $regel [ 'qty' ];
					$this->cols [ 'L1_KG' ] [ '99' ] += $regel [ 'ret' ] * $regel [ 'qty' ];
					$this->cols [ 'L1_KB' ] [ '0.00' ] += $regel [ 'ret' ] * $regel [ 'qty' ];
					/*
					if ( in_array ( $rowp [ 'voortgang' ], array ( 3, 11 ) ) ) 
						$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							$rowp [ 'ref' ],	
							sprintf ( "%s x ( %s )", $regel [ 'qty' ], $regel [ 'art' ] ),
							sprintf ( "%s (%s %s )", $Local1, $Local2 , $Local3 ),
							'', 
							'',
							$regel [ 'nam' ] ) ) );
					*/
				}
				if ( in_array ( $rowp [ 'voortgang' ], $arr_bank ) ) {
					if ( !isset (  $this->cols [ 'L1_BG' ] [ $product [ 'type' ] ] ) )  $this->cols [ 'L1_BG' ] [ $product [ 'type' ] ] = 0;
					if ( !isset (  $this->cols [ 'L1_BG' ] [ '99' ] ) )  $this->cols [ 'L1_BG' ] [ '99' ] = 0;
					if ( !isset (  $this->cols [ 'L1_BB' ] [ '0.00' ] ) )  $this->cols [ 'L1_BB' ] [ '0.00' ] = 0;
					if ( !isset (  $this->cols [ 'L1_BB' ] [ $product [ 'amt0' ] ] ) )   $this->cols [ 'L1_BB' ] [ $product [ 'amt0' ] ] = 0;
					if ( !isset (  $this->cols [ 'L1_BB2' ] [ $product [ 'amt0' ] ] ) )   $this->cols [ 'L1_BB2' ] [ $product [ 'amt0' ] ] = 0;
					 $this->cols [ 'L1_BG' ] [ $product [ 'type' ] ] += $hulpexprice * $regel [ 'qty' ];
					 $this->cols [ 'L1_BB' ] [ $product [ 'amt0' ] ] += $hulpexprice * $regel [ 'qty' ];
					 $this->cols [ 'L1_BB2' ] [ $product [ 'amt0' ] ] += $hulpbtw * $regel [ 'qty' ];
					 $this->cols [ 'L1_BG' ] [ '99' ] += $regel [ 'ret' ] * $regel [ 'qty' ];
					 $this->cols [ 'L1_BB' ] [ '0.00' ] += $regel [ 'ret' ] * $regel [ 'qty' ];
				}
			}
			if ( in_array ( $rowp [ 'voortgang' ], $arr_proforma ) ) {
				$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$rowp [ 'ref' ],	
					date ( "d M Y H:i", substr ( $rowp [ 'ref' ] ,1 ) ) , 
					$this->language [ 'stappen' ] [ $rowp [ 'voortgang' ] ] ?? "--" . $rowp [ 'voortgang' ] . "--",
					$this->gsm_sanitizeStrings ( $rowp [ 'amt2' ], "s{KOMMA|E128}" ),
					'',
					'' ) ) );
					$this->cols [ 'L2_DT' ] [ $rowp [ 'voortgang' ] ] += $rowp [ 'amt2' ];
			}
			if ( in_array ( $rowp [ 'voortgang' ], array ( 3, 11 ) ) ) {
				$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$rowp [ 'ref' ],	
					date ( "d M Y H:i", substr ( $rowp [ 'ref' ] ,1 ) ) , 
					$this->language [ 'stappen' ] [ $rowp [ 'voortgang' ] ] ?? "--" . $rowp [ 'voortgang' ] . "--",
					'',
					'',
					$this->gsm_sanitizeStrings ( $rowp [ 'amt2' ], "s{KOMMA|E128}" ) ) ) );
					$this->cols [ 'L2_DT' ] [ $rowp [ 'voortgang' ] ] += $rowp [ 'amt2' ];
			}	
			if ( in_array ( $rowp [ 'voortgang' ], $arr_bank ) ) {
				$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$rowp [ 'ref' ],	
					date ( "d M Y H:i", substr ( $rowp [ 'ref' ] ,1 ) ) , 
					$this->language [ 'stappen' ] [ $rowp [ 'voortgang' ] ] ?? "--" . $rowp [ 'voortgang' ] . "--",
					'',
					$this->gsm_sanitizeStrings ( $rowp [ 'amt2' ], "s{KOMMA|E128}" ),
					'' ) ) );
					$this->cols [ 'L2_DT' ] [ $rowp [ 'voortgang' ] ] += $rowp [ 'amt2' ];
			}	
		}	
	}
}
if ( $regelcount > 0 ) {
	/* **************** Afsluiten L2 */
	if ( count ($pdf_data) > 0 ) {
	
		/* factuur */
		$LocalAmt1 = 0;		
		foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
			if ( in_array ( $pay, array ( 5, 7, 9, 13 ) ) ) $LocalAmt1 += $load; 
		}

		/* bank */
		$LocalAmt2 = 0;		
		foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
			if ( in_array ( $pay, array ( 4, 12  ) ) ) $LocalAmt2 += $load; 
		}

		/* kas */
		$LocalAmt3 = 0;		
		foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) { 
			if ( in_array ( $pay, array (  3, 11   ) ) ) $LocalAmt3 += $load; 
		}

		/* L2 = dag totaal gesplitst naar factuur, bank of kas */
		$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
			'',	
			'', 
			"Totaal (". $this->gsm_sanitizeStrings ( $LocalAmt1 + $LocalAmt2 +$LocalAmt3, "s{KOMMA|E128}" ).")",
			$this->gsm_sanitizeStrings ( $LocalAmt1, "s{KOMMA|E128}" ),
			$this->gsm_sanitizeStrings ( $LocalAmt2, "s{KOMMA|E128}" ),
			$this->gsm_sanitizeStrings ( $LocalAmt3, "s{KOMMA|E128}" ) ) ) );
		/* L1 = month totaal 	*/
		foreach ( $this->cols[ 'L2_DT' ] as $pay => $load ) $this->cols[ 'L1_PT' ] [ $pay ] += $this->cols[ 'L2_DT' ] [ $pay ];
		for ($x = 0; $x <= 16; $x++) $this->cols[ 'L2_DT' ] [ $x ] = 0;		

		/* output */
		$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array( );			
		$pdf_text .= "\n";
	}
}
/* **************** Afsluiten L1*/
if ( isset ( $this->cols [ 'L1_KG' ] ) ) {
	$pdf->AddPage();
	$pdf->ChapterBody( $pdf_text );
	$pdf_text .= " ";
	$pdf->ChapterTitle( $this->cols[ 'L1' ], $subtitel2 );
	$PeriodeTotaal = 0;
	
	/* contante betalingen */
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'Kontant',	
		'in Kas', 
		'',
		'',
		'',
		'') ) );
	if ( count ( $this->cols [ 'L1_KG' ] ) > 1) ksort ( $this->cols[ 'L1_KG' ] );
	foreach ( $this->cols [ 'L1_KG' ] as $pay => $load )
		if ( $load != 0 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				substr ($GroepArr [ $pay ] ?? "", 0, 40),
				'',
				$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
				'',
				'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
		}
	foreach ( $this->cols [ 'L1_KB2' ] as $pay => $load )				
		if ( $load != 0 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				'BTW:  ', 
				$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
				$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
				'over',
				$this->gsm_sanitizeStrings ( $this->cols[ 'L1_KB' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
		}	
	$sub_totaal	= array_sum ( $this->cols [ 'L1_KG' ] ) + array_sum ( $this->cols [ 'L1_KB2' ] );
	$PeriodeTotaal += $sub_totaal;
	$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
		'',	
		'',	
		'Totaal', 
		$this->gsm_sanitizeStrings ( $sub_totaal, "s{KOMMA|E128}" ),
		'',
		'' ) ) );
		
	/* bank betalingen */
	$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
	$pdf_data = array( );
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'Bank',
		'Pin/Tikkie',	
		'', 
		'',
		'',
		'') ) );
	if ( count ( $this->cols [ 'L1_BG' ] ) > 1) ksort ( $this->cols[ 'L1_BG' ] );
	foreach ( $this->cols [ 'L1_BG' ] as $pay => $load )
		if ( $load != 0 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				substr ($GroepArr [ $pay ] ?? "", 0, 40),
				'',
				$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
				'',
				'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
		}
	foreach ( $this->cols [ 'L1_BB2' ] as $pay => $load )				
	if ( $load != 0 ) {
		$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
			'',	
			'BTW:  ', 
			$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
			$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
			'over',
			$this->gsm_sanitizeStrings ( $this->cols [ 'L1_BB2' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
	}	
	$sub_totaal	= array_sum ( $this->cols [ 'L1_BG' ] ) + array_sum ( $this->cols [ 'L1_BB2' ] );
	$PeriodeTotaal += $sub_totaal;
	$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
		'',	
		'',	
		'Totaal', 
		$this->gsm_sanitizeStrings ( $sub_totaal , "s{KOMMA|E128}" ),
		'',
		'' ) ) );
			
	/* pro-forma */
	$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
	$pdf_data = array( );
	$pdf_data[ ] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
		'Pro-forma',
		'(te factureren)',	
		'', 
		'',
		'',
		'') ) );
	if ( count ( $this->cols [ 'L1_FG' ] ) > 1) ksort ( $this->cols[ 'L1_FG' ] );
	foreach ( $this->cols [ 'L1_FG' ] as $pay => $load ) 
		if ( $load != 0 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				substr ($GroepArr [ $pay ] ?? "", 0, 40),
				'',
				$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
				'',
				'btw: '.$this->gsm_sanitizeStrings ( $GroepBTWArr [ $pay ] ?? 0.00, "s{SCHEMA}" ) ) ) );
		}
	foreach ( $this->cols [ 'L1_FB2' ] as $pay => $load )				
		if ( $load != 0 ) {
			$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				'',	
				'BTW:  ', 
				$this->gsm_sanitizeStrings ( $pay , "s{KOMMA}" )."%",
				$this->gsm_sanitizeStrings ( $load, "s{KOMMA|E128}" ),
				'over',
				$this->gsm_sanitizeStrings ( $this->cols [ 'L1_FB' ] [ $pay ], "s{KOMMA|E128}" ) ) ) );
		}	
	$sub_totaal	= array_sum ( $this->cols [ 'L1_FG' ] ) + array_sum ( $this->cols [ 'L1_FB2' ] );
	$PeriodeTotaal += $sub_totaal;
	$pdf_data[ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
		'',	
		'',	
		'Totaal', 
		$this->gsm_sanitizeStrings ( $sub_totaal , "s{KOMMA|E128}" ),
		'',
		'' ) ) );	
		
	$pdf->DataTable( $pdf_header2, $pdf_data, $pdf_cols2 );
	$pdf_data = array( );
		
	$pdf_header3 = array( "", 
		"Periode Totaal", 
		"", 
		$this->gsm_sanitizeStrings ( $PeriodeTotaal , "s{KOMMA|E128}" ),
		"", 
		"");
	$pdf->DataTable( $pdf_header3, $pdf_data, $pdf_cols2 );
	$pdf_data = array( );
	$pdf->AddPage();
}

/* end */

// footer
$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
if ( isset ( $regelcount ) && $regelcount > 0 ) $pdf_text .= sprintf ( "\nVerwerkte Transacties : %s\n" , $regelcount ) ;
$pdf_text .= sprintf ( "\n%s %s - %s\n", "Periode :" , $monthStart, $monthEnd ) ;
if ( strlen( $query ) > 1 ) $pdf_text .= sprintf ( "\n %s %s :\n %s %s" . "\n"  ,  $this->language [ 'pdf' ][ 2 ], $project, $selection, $query );

if ($this->setting [ 'debug' ] == "yes" ){
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}

//  last pdf output
if ( count ($pdf_data) >0 )	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data  = array( );
if ( $pdf_text != "" ) $pdf->ChapterBody( $pdf_text );
$pdf_text   = '';

?>