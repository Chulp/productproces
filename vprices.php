<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* 1_module id */ 
$module_name = 'vprices';
$version='v20241117';
$main_file = "product";
$unique = 'ref';
$sub_file = "group";
$project="Onderhoud Prijzen";
$default_template = '/' . 'prices' . '.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_" . $main_file;
$oFC->file_ref  [ 98 ] = LOAD_DBBASE . "_" . $sub_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;
$oFC->page_content [ 'STOCK' ] = 0;

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( );

/* 12 selection functions */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "print" => "print" , "all" => "all", "debug" => "debug", "nul" => "nul") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

if ( strstr ( $xmode, "debug" ) ) $oFC->setting [ 'debug' ] = "yes";
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST ?? "", "get" => $_GET ?? "", "setting" => $oFC->setting, "content" => $oFC->page_content, $selection, $xmode ), __LINE__ . $module_name ); 

/* 13 selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );


/* 14 Group list for selection */
$cal = array();
$results = array ( );
$database->execute_query ( sprintf ("SELECT * FROM `%s` WHERE `active` = 1 ORDER BY `ref`", $oFC->file_ref  [ 98 ] ),
	true, 
	$results );
if ( count( $results ) > 0 ) {
	foreach ( $results as $row ) 
		$cal [ $row[ 'id' ] ] = $row[ 'ref' ] ;
}
$groepsArr = $cal;

/* 18 Print function needed */
if ( isset ( $xmode ) && strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_Prijzen_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );

/* 19 sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* 20 some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
			
		case "Save": //    1=>'Wijzigen', 
			$job = array ();
			$TEMPLATE = "UPDATE " . $oFC->file_ref  [ 99 ] . " SET `%s` = '%s' WHERE `id` = '%s'"; 
			$TEMPLATE1 = "SELECT * FROM `" . $oFC->file_ref  [ 99 ] . "` WHERE `id` = '%s'"; 
			$TEMPLATE2 = "aanpassing %s  %s naar %s voor %s <br />";
			$input = array ("a", "b", "c", "d", "e", "f", "g" );
			$LocalHulp_n0 = $_POST[ 'n0' ] ?? 1;
			$oFC->paging [ 'POSITION' ] = $LocalHulp_n0 - 1 ; // back to the page 
			foreach ($_POST as $key => $value ) {  // cycle door input
				$localHulpA = explode ("|", $key);	
				if ( isset ( $localHulpA [ 0 ] ) 
					&& isset ( $localHulpA [ 1 ] ) 
					&& $localHulpA [ 0 ] == "inp" 
					&& in_array ( $localHulpA [ 1 ], $input )  
					&& $localHulpA [ 3 ]  !=  str_replace ( array ( " ", "." ), "_", $value ) ) {
					switch ( $localHulpA[1] ) {
						case "a":
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref [ 99 ], "name", $value , $localHulpA[2]  );
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "omschrijving", str_replace ( "_", " ", $localHulpA[3] ), $value );
							break;
						case "b":
							if ( in_array( $value , array ( 0, 1, 2, 3) ) ) {
								$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref [ 99 ], "active", $value , $localHulpA[2] );
								$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "active", $localHulpA[3], $value );
							}
							break;
						case "c":
							$localHulp = $oFC->gsm_sanitizeStringv ( $value, "v{0;0;300}" );
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref  [ 99 ], "amt1", $localHulp , $localHulpA[2]  );	
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "Advies prijs incl ", $localHulpA[3], $localHulp );	
							break;
						case "d":
							$localHulp = $oFC->gsm_sanitizeStringv ( $value, "v{0;0;300}" );
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref  [ 99 ], "amt3", $localHulp , $localHulpA[2]  );	
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "Kostprijs ex ", $localHulpA[3], $localHulp );	
							break;
						case "e":
							$localHulp = $oFC->gsm_sanitizeStringv ( $value, "v{0;0;300}" );
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref  [ 99 ], "amt2", $localHulp , $localHulpA[2]  );	
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "Onze prijs ex ", $localHulpA[3], $localHulp );	
							break;
						case "f":
							$localHulp = $oFC->gsm_sanitizeStringv ( $value, "v{0;0;300}" );
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref  [ 99 ], "amt4", $localHulp , $localHulpA[2]  );	
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "Statiegeld ", $localHulpA[3], $localHulp );	
							break;
						case "g":
							$localHulp = $oFC->gsm_sanitizeStringv ( $value, "v{0;0;300}" );
							$job [] =  sprintf ( "UPDATE %s SET `%s` = '%s' WHERE `id` = '%s'", $oFC->file_ref  [ 99 ], "amt0", $localHulp , $localHulpA[2]  );	
							$oFC->description .= sprintf ( "(%s) aanpassing %s  %s naar %s <br />", $localHulpA[2], "BTW % ", $localHulpA[3], $localHulp );	
							break;
						default: 
							gsm_debug ( array ( $key, $value, $localHulpA, $localHulpA [3], str_replace ( array( " ", "." ), "_", $value )), __LINE__ . __FUNCTION__ );
							break;
					}
				}
			}
			if ( isset ( $job ) && count ( $job ) > 0 ) { 
				foreach ( $job as $key => $query ) $database->simple_query ( $query );
			} 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		default:
		$oFC->page_content [ 'MODE' ] = 9;
		break;
	} 
} else { 
	/* initial run */
}
/* end which job to do */

/* 31_Additional function */ 
if ( isset ( $oFC->setting [ 'pdf_filename' ] )  && strlen( $oFC->setting [ 'pdf_filename' ] ) > 5 ) {
	$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 1 );
} 

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	default: 
		$pageok = true;
		
		/* 33 bepaal aantal records */
		$result = array ( );
		$database->execute_query ( sprintf ( 
			"SELECT count(`id`) FROM `%s` %s", 
			$oFC->file_ref [ 99 ], 
			$oFC->search_mysql ), true, $result );
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];

		/* end skip active 0 * /
		if ( strlen ( $oFC->search_mysql ) > 15 ) {
			$oFC->search_mysql = sprintf ( "%s AND `" . $oFC->file_ref [ 99 ] . "`.`active` > '0' ", $oFC->search_mysql ); 
		} else { 
			$oFC->search_mysql = " WHERE `" . $oFC->file_ref [ 99 ] . "`.`active` > '0' ";
		}
		/* end skip */
		$limit_sql = $oFC->gsm_pagePosition ( "sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );

		$query = "SELECT `" . $oFC->file_ref[ 99 ] . "`.* ,";
		$query .= " `" . $oFC->file_ref[ 98 ] . "`.`amt1` AS `amtvat` ,";
		$query .= " `" . $oFC->file_ref[ 98 ] . "`.`name` AS `refgrp` ";
		$query .= "	FROM `" . $oFC->file_ref[ 99 ] . "` ";
		$query .= "	LEFT JOIN `" . $oFC->file_ref[ 98 ] . "` ";
		$query .= "	ON `" . $oFC->file_ref[ 98 ] . "`.`id` = `" . $oFC->file_ref[ 99 ] . "`.`type` "; 
		$query .= sprintf ( "%s ORDER BY `%s`.`type`, `%s`.`active` DESC, `%s`.`zoek` ASC %s ",
			$oFC->search_mysql,
			$oFC->file_ref[ 99 ],
			$oFC->file_ref[ 99 ],
			$oFC->file_ref[ 99 ],
 			$limit_sql );

		$job = array ();
		$results = array ();
		$database->execute_query ( 
			$query, 
			true, 
			$results );
		if ( count ( $results ) > 0 ) {
			/* debug * /  Gsm_debug ($results,  __LINE__ . __FUNCTION__ ); /* debug */
			foreach ( $results as $row ) {
				$updateArr = array(); 
				/* $row [ 'refgrp' ] artikelgroep tekst */
				if ( !isset ( $row [ 'refgrp' ] ) ) $row [ 'refgrp' ] = $groepsArr [ $row [ 'type' ] ] ?? " onbekend " ;
				/* $row [ 'amtvat' ] btw percentage */
				/* $row [ 'amt0' ] btw percentage */
				if ( !isset ( $row [ 'amtvat' ] ) ) $row [ 'amtvat' ] = $row [ 'amt0' ] ;
				if ( $row [ 'amtvat' ] != $row [ 'amt0' ] ) {
					$row [ 'amt0' ] = $row [ 'amtvat' ];
					$updateArr [ 'amt0' ] = $row [ 'amtvat' ];
				}
				/* $row [ 'ref' ] artikelnummer */
				/* $row [ 'name' ] artikel naam */
				/* $row [ 'active' ] active */
				/* $row [ 'amt0' ] btw percentage */
				/* $row [ 'amt1' ] adviesprijs incl btw exl emballage*/
				/* $row [ 'amt2' ] onze prijs / verhuurprijs excl VAT and emballage */
				/* $row [ 'amt3' ] kost prijs excl VAT and emballage*/
				/* $row [ 'amt4' ] emballage  */			
				/* $row [ 'amt5' ] voor de voorraad admin */
				$lowest = 0.05;
				/* controles op onze prijs 
				*/
				if ( $row [ 'amt2' ] < $lowest ) {  
					$highest = round ( $row [ 'amt1' ]  / ( 100 + $row [ 'amt0' ] ) * 100 , 2 );
					if ( $row [ 'amt3' ] > $highest ) $highest = $row [ 'amt3' ];
					if ( $row [ 'amt2' ] > $highest ) $highest = $row [ 'amt2' ];
					$LocalHulpA = round ( $highest, 2 );
					if ( $LocalHulpA != $row [ 'amt2' ]) {
						$row [ 'amt2' ] = $LocalHulpA;
						$updateArr [ 'amt2' ] = $row [ 'amt2' ];
					}
				}
				if ( $row [ 'amt3' ] < $lowest ) { 
				if ( $row [ 'amt3' ] != $row [ 'amt2' ]) {
						$row [ 'amt3' ] = $row [ 'amt2' ];
						$updateArr [ 'amt3' ] = $row [ 'amt2' ];
					}
				}
				if ( $row [ 'amt1' ] < $lowest ) { 
					$LocalHulpA = round ( $row [ 'amt2' ]  * ( 100 + $row [ 'amt0' ] ) / 100 , 2 );
					if ( $LocalHulpA != $row [ 'amt1' ]) {
						$row [ 'amt1' ] = $LocalHulpA;
						$updateArr [ 'amt1' ] = $row [ 'amt1' ];
					}
				}
				/* opmaak  en calculaties */
				/* onze prijs inclusief btw excl emballage */
				$LocalHulpA = round ( $row [ 'amt2' ]  * ( 100 + $row [ 'amt0' ] ) / 100 , 2 ) ;
				$row [ 'amt2b' ] = $oFC->gsm_sanitizeStringS ( $LocalHulpA, "s{KOMMA|EURO}" );
				/* marge */
				if ($row [ 'amt1' ] < $lowest || $row [ 'amt3' ] < $lowest ) {
					$row [ 'amt1b' ] = "--";
					$row [ 'amt3b' ] = "--";
				} else {
					/* korting */
					$row [ 'amt1b' ] = $oFC->gsm_sanitizeStringS ( round ( ( 1- ( $LocalHulpA / $row [ 'amt1' ] ) ) * 100 , 2), "s{KOMMA}" );
					/* marge */
					$row [ 'amt3b' ] = $oFC->gsm_sanitizeStringS ( round ( ( ( $row [ 'amt2' ] / $row [ 'amt3' ] ) -1 ) * 100 , 2), "s{KOMMA}" );
				}
				$oFC->page_content [ 'RESULTS' ] [ $row [ 'type'] ] [] = $row;
				
				if ( count ( $updateArr ) > 0 ) $job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $row [ 'id' ] . "'"; 
			}
		}
		if ( isset ( $job ) && count( $job ) > 0 ) {
			foreach ( $job as $key => $query ) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
				$database->simple_query ( $query  ) ;  
			}
			$oFC->description .= ' Upgrade function : ' . count( $job ). NL;
		}
		break;
}
 
/* the opmaak en selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'TOEGIFT' ] = ""; 
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;
	case 9:
	default: 
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 10 ), "-", "-" ,"0", "-", "-" , "print" );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 2, 6, 11) , '-', $pdflink ?? "-" );
		break;
} 

/* 98 memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( ); 
	
/* 99 output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id;

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 