<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* 1_module id */ 
$module_name = 'vsocial';
$version='20240613';
$main_file = "social";
$unique = 'id';
$default_template = '/' . $main_file . '.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , $oFC-> language [ 'TXT_MAINTENANCE' ], strtoupper ( $main_file )) ;

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_" . $main_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "sgroep" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "swijk" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );;

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* extra */
$oFC->page_content [ 'GROEP' ] = "";
$oFC->page_content [ 'WIJK' ] = "";
$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = "active";
$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = "";
$oFC->page_content [ 'REFERENCE_ACTIVE3' ] = "";	

if ( isset ($oFC->setting [  'sgroep' ] ) ) {
	foreach ( $oFC->setting [  'sgroep' ]  as $key => $value ) 
		$oFC->page_content [ 'GROEP' ] .= sprintf ('<div class="item" data-value="%s">%s</div>' , $key, substr ($key,0,4) . " " . $value);
	foreach ( $oFC->setting [  'swijk' ]  as $key => $value ) {
		if ($key !="all" ) $oFC->page_content [ 'WIJK' ] .= sprintf ('<div class="item" data-value="%s">%s</div>' , $key, $value);
	}
}
/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( );

if ( $oFC->setting [ 'debug' ] == "yes" ) 	Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ), __LINE__ . $module_name ); 

/* 12 selection functions */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "print" => "print" , 
		"all" => "all", 
		"debug" => "debug", 
		"extra" => "extra") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

/* 13 selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}

$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );


/* 18 Print function needed */
if ( isset ( $xmode ) && strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Social_Overzicht" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );


/* 19 sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* 20 some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "Select":
		case "View":
			if ( !isset( $_POST[ 'vink' ][ 0 ] ) ) 	break;
			$oFC->page_content [ 'MODE' ] = 8;
			$oFC->recid = $_POST[ 'vink' ][ 0 ];
			$FieldArr = array ();
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
			if ($oFC->setting [ 'debug' ] == "yes" ) 
				Gsm_debug (array ("post"=> $_POST, $oFC->page_content [ 'RESULT'] ), __LINE__ . $module_name ); 
			break;
		case "Save":
			$oFC->page_content [ 'MODE' ] = 8;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			/*  10_specific handling */
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file, "gsm_", $unique );
 			/* attachment */
			$oFC->setting [ 'allowed' ] = array ( "jpg" , "png", "pdf");
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ] . "/",'%5$s%2$s', "social", $oFC->recid  );
			/* end attachment */
			break;
		case "New":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			/*  11_specific handling */
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 2, $main_file, "gsm_" , $unique );
 			/* 12_attachment */
			$oFC->setting [ 'allowed' ] = array ( "jpg" );
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ]. "/",'%5$s%2$s', "social", $oFC->recid );
			if ($oFC->setting [ 'debug' ] == "yes" ) 
				Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", $oFC->page_content ), __LINE__ . $module_name ); 
			/* end attachment */
			break;
		case "Print": 
		case "Delete":
		case "Reset":
			$oFC->recid = '';
			$oFC->search_mysql = "";
			$selection= "";
			$oFC->page_content [ 'PARAMETER' ] = $selection;
			$oFC->page_content [ 'SUB_HEADER' ]= "____";
			break;
		default:
			/* escape route */
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		/* is a record selected */ 
		case 'select': 
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 8;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
			}
			break;
		/* Delete a record */
		case 'remove': //    
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 9;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 3, $main_file );
				$oFC->recid = "";
			}
			break;
		/* End Delete a record */
		default:
			/* escape route */
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	/* initial run */
	$oFC->page_content [ 'P1' ] = true;
	
	/* Default settings */
	$job = array ();
	
	foreach ( $oFC->file_ref as $LocalHulp ) {
		$payload = $place[ 'includes'] . str_replace ( TABLE_PREFIX, "setup_", $LocalHulp ) . ".php";
		if ( file_exists ( $payload ) ) { 
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade checked ' . $LocalHulp .  NL; 
			require_once ( $payload ); 
		} else {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' No Upgrade function ' . $LocalHulp .  NL; 
		}
	}
}

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ($oFC->page_content, __LINE__ . $template_name );
	
/* 31_Additional function */ 
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' ); 
if ( isset ( $oFC->setting [ 'pdf_filename' ] )  && strlen( $oFC->setting [ 'pdf_filename' ] ) > 5 ) {
	$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 1 );
} 

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ($oFC->page_content, __LINE__ . $template_name );

/* 40 display preparation */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 8:
		break;
	default: 
		$pageok = true;
		/* 33 bepaal aantal records */
		$result = array ( );
		$database->execute_query ( sprintf ( 
			"SELECT count(`id`) FROM `%s` %s", 
			$oFC->file_ref [ 99 ], 
			$oFC->search_mysql ), true, $result );
		$row = current ( $result );
		
		/* 34 lege file */
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		if ( $oFC->page_content [ 'aantal' ] == 0 && strlen ($oFC->search_mysql) < 15 ) {
			// leeg record toevoegen
			$updatearr = array( 'name' => "initial" );
			$database->build_and_execute ( 
				"insert",
				$oFC->file_ref [ 99 ],
				$updatearr );
		}
		
		/* 35 paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ( "sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		$fields = array( "id",
			"type",		
			"ref", 
			"name", 
			"content_short", 	
			"content_long", 
			"comment",
			"content_tel",
			"content_email",
			"content_url",
			"content_wijk",
			"content_groep",
			"zoek", 
			"active",
			"updated");
		
		$query  = "SELECT `" . implode ("`, `", $fields) . "` FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= sprintf ( "%s ORDER BY `name`, `zoek` ASC %s", 
			$oFC->search_mysql, 
			$limit_sql );

		/* read records */
		$results = array ( );
		$resultPL0 = array ( );	
		$resultPL1 = array ( );
		$n = $oFC->page_content [ 'POSITION' ];
		$job = array ( );
		$vorig_unique= "-----";
		$database->execute_query ( 
			$query, 
			true, 
			$results );
		if (count ( $results ) > 0 ) { 
			$PL0 = "=="; //levelbreaks
			$PL1 = "=="; 	
			foreach ( $results as  $row ) {
				
				/* subtotaals accordeon */
				if ( $PL0 != $row [ 'name' ] ) {
					if ( count ( $resultPL1 ) > 0 ) $resultPL0 [ $PL0 ] = $resultPL1 ; 
					$resultPL1 = array ( );
					$PL0 = $row [ 'name' ];
					$PL1 = "=="; 
				}
				
				$updateArr = array(); 	
				
				/* make zoek unique * /
				if ( $PL1 == $row [ 'ref' ] )	{	
					$updateArr [ 'ref' ] = $row [ 'ref' ] . "-"; 
					$updateArr [ 'zoek' ] =  "|" . $row [ 'type' ] . "|" . $updateArr [ 'ref' ] . "|" ; 
				} else {
					/* recreate zoek */	
					$PL1 = $row [ 'ref' ];
					$arout = $oFC->setting [ 'zoek' ] [ $main_file ];
					foreach ( $row as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
					$arout = str_replace ( "||", "|", $arout );	
					$arout = str_replace ( " ", "", $arout );						
					$arout = substr ( strtolower ( $oFC->gsm_sanitizeStrings ( $arout, 's{STRIP|TOASC|LOWER}' ) ), 0, 2000 ); 
					if ( $row [ 'zoek' ] != $arout ) $updateArr ['zoek'] = $arout;
//				}
				$date0 = date_create ( date ( "Y-m-d", time() ) );
				$date1 = date_create ( $row['updated'] );
				$diff = date_diff( $date0, $date1);
				$row ['datetxt'] = $diff->format("%R%a days");
				if ( isset ( $updateArr ) && count ( $updateArr ) > 0 ) 
					$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $row [ 'id' ] . "'"; 		
				/* output */
				$resultPL1 [] = $row;
			}
		
			/* end subtotaal type accordeon */
			if ( count ( $resultPL1 ) > 0 ) $resultPL0 [ $PL0 ] = $resultPL1 ; 
			
			/* repairs */
			if ( isset ( $job ) && count ( $job ) > 0 ) {
				foreach ( $job as $key => $query ) $database->simple_query ( $query ) ; 
				$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . " Aantal records aangepast : ". count ( $job ). NL;
			}
		} else {
			$oFC->page_content [ 'RECID' ] = "";
			$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_DATA' ].NL;
		}
		$oFC->page_content [ 'RESULTS' ] = $resultPL0;
		break;
}

/* the opmaak en selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'TOEGIFT' ] = ""; 
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;
	case 8:
		/* active */
		$oFC->page_content [ 'RESULT' ] [ 'REF' ] = $oFC->gsm_selectOption ( $oFC->setting [  'swijk' ], $oFC->page_content [ 'RESULT' ] [ 'ref' ], 1);
		$LocalHulp = $oFC->language [ 'active' ]; 
		unset ($LocalHulp [3]);
		$oFC->page_content [ 'RESULT' ] [ 'ACTIVE' ] = $oFC->gsm_selectOption ( $LocalHulp, $oFC->page_content [ 'RESULT' ] [ 'active' ], 1);

		/* opmaak */
		foreach ( $oFC->page_content [ 'RESULT'] as $pay => $load ) { 
			if ( substr ( $pay, 0, 3 ) == "amt") {
				$oFC->page_content [ 'RESULT'] [ $pay ] = $oFC->gsm_sanitizeStringS ( $load, "s{KOMMA}");
			} elseif ( substr ( $pay, 0, 3 ) == "aan"){
			} elseif ( substr ( $pay, 0, 3 ) == "dat") {
			} elseif ( substr ( $pay, 0, 3 ) == "tim") {
			}
		}		
		
		/* attachment */
		$imageref = sprintf ( "%s%s/social%s.jpg", LEPTON_PATH, $oFC->setting [ 'mediadir' ], $oFC->recid );
		if ( file_exists( $imageref ) ) {
			if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ($imageref, __LINE__ . __FUNCTION__ );
			$oFC->page_content [ 'RESULT'] [ "IMAGE" ] = '<img class="ui medium image" src="'.str_replace ( LEPTON_PATH, LEPTON_URL, $imageref ).'">';
		} else {
			$imageref = sprintf ( "%s%s/social%s.png", LEPTON_PATH, $oFC->setting [ 'mediadir' ], $oFC->recid );
			if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ($imageref, __LINE__ . __FUNCTION__ );
			if ( file_exists( $imageref ) ) {
				$oFC->page_content [ 'RESULT'] [ "IMAGE" ] = '<img class="ui medium image" src="'.str_replace ( LEPTON_PATH, LEPTON_URL, $imageref ).'">';
			}
		}
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 2, 6, 8, 11), '-', $pdflink ?? "-" );
		break;
		
	case 9:
	default: 
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 10 ), "-", "-" ,"0", "-", "-" , "zoeken op" );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6, 11) , '-', $pdflink ?? "-" );
		break;
} 

/* 98 memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( ); 
	
/* 99 output processing */

// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id;

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 