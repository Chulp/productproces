<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print1'] = "20241021";	

$prmode = array (); 
$printOK = true;
if ( strstr ( $selection, "print" ) ) 	$prmode [ ] = 1; //basic
if ( strstr ( $selection, "all" ) )		$prmode [ ] = 2; // alles */
if ( strstr ( $selection, "extra" ) ) 	$prmode [ ] = 3; // groep / text
if ( strstr ( $selection, "debug" ) ) 	$prmode [ ] = 4; // groep / text
if ( strstr ( $selection, "debug" ) ) 	$prmode [ ] = 5; // groep / text

if ( count ( $prmode ) < 1 ) $prmode = array ( 1 ); 

$oMD [ "ref_table" ] = $this->file_ref  [ 99 ];
$oMD [ "ref_table2" ] = $this->file_ref  [ 1 ];

$TEMP1  = "SELECT `ref`, `name` FROM `%s` WHERE `type` LIKE '%s' AND `active` = 1 ORDER BY `ref` ASC ";
	
/* ophalen wijktabel */
$oMD [ "SWIJK" ] = array();
$results = array ( );
$database->execute_query ( 
	sprintf ($TEMP1, $oMD [ "ref_table2" ], "swijk" ), 
	true, 
	$results );
foreach ( $results as $row ) {
	$oMD [ "SWIJK" ] [ substr ( $row [ 'ref' ], 0, 2 ) ] = $row [ 'name' ];
}


/* ophalen groeptabel */
$oMD [ "SGROEP" ] = array();
$results = array ( );
$database->execute_query ( 
	sprintf ($TEMP1, $oMD [ "ref_table2" ], "sgroep" ), 
	true, 
	$results );
foreach ( $results as $row ) {
	$oMD [ "SGROEP" ] [ substr ( $row [ 'ref' ], 0, 4 ) ] = $row [ 'name' ];
}	

// $this->setting [ 'debug' ] = "yes" ;

if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( $oMD ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */
if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( $this ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */
if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug (array (
	'query' => $query,
	'project' => $project,
	'selection' => $selection,
	'func' => $func,
	'run' => $run,
	'mode' => $prmode), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */
$query = strtoupper( trim ( $query ) );
$qsql="";
if (isset ($oMD [ "SGROEP" ] [ $query ] )) {
	$qsql .= " AND t1.`content_groep` LIKE  '%" . $par. "%'";
	$prmode [ ] = 2;
	$prmtxt = $oMD [ "SGROEP" ] [ $query ];	
}
if (isset ($oMD [ "SWIJK" ] [ $query ] )) {
	if ( $query == "AA") {
		$qsql .= " AND t1.`ref` LIKE  'AA%'";
	} else 	{
		$qsql .= " AND ( t1.`ref` LIKE  'AA%' OR t1.`ref` LIKE '%" . $query . "%' OR t1.`content_wijk` LIKE '%" . $query . "%' ) ";
	}
	$prmode [ ] = 2;
	$prmtxt = $oMD [ "SWIJK" ] [ $query ]	;
}

$j = 0;
$pdf_text = '';
/* initialise page  */
$pdf->AliasNbPages();
$pdf->AddPage();


if ( $printOK ) {
	if ( in_array ( 1, $prmode ) ) {
		$oMD [ "active_arr" ] = array (2);
		$j++;
		$subtitel = sprintf ( "%s ", "belangrijke nummers" );
		$pdf->ChapterTitle( $j, $subtitel );
		$pdf_cols = array( 10, 60, 120, 0, 0, 0 );
		$pdf_header = array( '1', '2', '3', '', '', '' );
		$pdf_data = array ( );
		$pdf_text = '';
		$sql = "SELECT t1.* FROM `" . $oMD [ "ref_table" ] . "` as t1 "; 	
		if ( strlen ( $query ) > 2) {
			$sql .= " WHERE t1.`zoek` LIKE '%" . strtolower ( $query ) . "%'";
			$sql .= " AND t1.`active` IN (" . implode ( ',', $oMD [ "active_arr" ] ) . ")";
		} else {
			$sql .= " WHERE t1.`active` IN (" . implode ( ',', $oMD [ "active_arr" ] ) . ")";
		}
		$sql .= " ORDER BY t1.`name` ASC ";
		$results = array ( );
		$database->execute_query ( 
			$sql, 
			true, 
			$results );
		if ( count 	($results ) > 0 ) {
			foreach ( $results as $row ) {
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					$row [ 'id' ],
					$this->Gsm_truncate ($row [ 'name' ], 34 , '...', true ),
					$row [ 'type'],
					'', '',	'') ) );
				if ( in_array ( 3, $prmode ) ) {
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						$row [ 'ref' ], 
						$row [ 'content_wijk' ],
						$row [ 'content_groep' ],
						'', '',	'') ) );
				}
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					"", 
					$row [ 'content_email' ],
					$row [ 'content_tel' ], 
					'', '',	'') ) );
				if (strlen( $row [ 'content_url' ]. $row [ 'content_adres' ]) > 2 )
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						$row [ 'content_url' ],
						$row [ 'content_adres' ], 
						'', '',	'') ) );
				if ( count ( $pdf_data ) > 0 ) {
					$pdf_header = array( '', '', '', '', '', '' );
					$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
					$pdf_data = array ( );
				}
				if ( strlen ( $pdf_text ) > 3 ) {
					$pdf->ChapterBody( $pdf_text );
					$pdf_text ="";
				}	
				if ( in_array ( 3, $prmode ) ) {				
					if (strlen( $row [ 'content_short' ]) > 2 )	{
						$pdf->ChapterBody( "\n\n" );
						$pdf->WriteHTML( html_entity_decode ($row [ 'content_short' ]) );	
						$pdf->ChapterBody( "\n\n" );
					}
					if (strlen( $row [ 'content_long' ]) > 2 )	{
						$pdf->WriteHTML( html_entity_decode ($row [ 'content_long' ]) );
						$pdf->ChapterBody( "\n\n" );		
					}
				}					
			}
		}	
	}
}

if ( $printOK ) {
	if ( in_array ( 2, $prmode ) ) {
		$oMD [ "active_arr" ] = array ( 1, 2);
		$j++;
		$pdf->AddPage();
		$subtitel = sprintf ( "%s ( %s )", "Overzicht Sociale Kaart", $query );
		$pdf->ChapterTitle( $j, $subtitel );
		$pdf_cols = array( 10, 60, 120, 0, 0, 0 );
		$pdf_header = array( '1', '2', '3', '', '', '' );
		$pdf_data = array ( );
		$pdf_text = '';
		$sql = "SELECT t1.* FROM `" . $oMD [ "ref_table" ] . "` as t1 "; 	
		if ( strlen ( $query ) > 2) {
			$sql .= " WHERE t1.`zoek` LIKE '%" . strtolower ( $query ) . "%'";
			$sql .= " AND t1.`active` IN (" . implode ( ',', $oMD [ "active_arr" ] ) . ")";
		} else {
			$sql .= " WHERE t1.`active` IN (" . implode ( ',', $oMD [ "active_arr" ] ) . ")";
		}
		$sql .= $qsql;
		$sql .= " ORDER BY t1.`name`, t1.`type`";
		$results = array ( );
		$database->execute_query ( 
			$sql, 
			true, 
			$results );
		if ( count 	($results ) > 0 ) {
			foreach ( $results as $row ) {
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					$row [ 'id' ],
					$this->Gsm_truncate ($row [ 'name' ], 34 , '...', true ),
					$row [ 'type'],
					'', '',	'') ) );
				if ( in_array ( 3, $prmode ) ) {
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						$row [ 'ref' ], 
						$row [ 'content_wijk' ],
						$row [ 'content_groep' ],
						'', '',	'') ) );
				}
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					"", 
					$row [ 'content_email' ],
					$row [ 'content_tel' ], 
					'', '',	'') ) );
				if (strlen( $row [ 'content_url' ]. $row [ 'content_adres' ]) > 2 )
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						$this->Gsm_truncate ($row [ 'content_url' ], 34 , '...', true ),
						$row [ 'content_adres' ], 
						'', '',	'') ) );
				if ( count ( $pdf_data ) > 0 ) {
					$pdf_header = array( '', '', '', '', '', '' );
					$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
					$pdf_data = array ( );
				}
				if ( strlen ( $pdf_text ) > 3 ) {
					$pdf->ChapterBody( $pdf_text );
					$pdf_text ="";
				}	
				if ( in_array ( 3, $prmode ) ) {				
					if (strlen( $row [ 'content_short' ]) > 2 )	{
						$pdf->ChapterBody( "\n\n" );
						$pdf->WriteHTML( html_entity_decode ($row [ 'content_short' ]) );	
						$pdf->ChapterBody( "\n\n" );
					}
					if (strlen( $row [ 'content_long' ]) > 2 )	{
						$pdf->WriteHTML( html_entity_decode ($row [ 'content_long' ]) );
						$pdf->ChapterBody( "\n\n" );		
					}
				}					
			}
		}
	}
}

if ( $printOK ) {
	if ( in_array ( 5, $prmode ) ) {
		$j++;
		$pdf->AddPage();
		$subtitel = "afsluiting";
		$pdf->ChapterTitle( $j, $subtitel );
		/* wat blijft er nog over */
		$sql = "SELECT t1.* FROM `" . $oMD [ "ref_table" ] . "` as t1 "; 	
		if ( strlen ( $query ) > 2) {
			$sql .= " WHERE t1.`zoek` LIKE '%" . strtolower ( $query ) . "%'";
			$sql .= " AND t1.`active` = '0'" ; 
		} else {
			$sql .= " WHERE t1.`active` = '0'" ; 
		}
		$sql .= " ORDER BY t1.`name` ASC ";
		// $pdf_text .= "\n\n" . $sql;
		$results = array ( );
		$database->execute_query ( 
			$sql, 
			true, 
			$results );
		if ( count 	( $results ) > 0 ) {
			$pdf_text .= "\n\n" . "Niet toegewezen";
			foreach ( $results as $row ) $pdf_text .= "\n" . $row [ 'name'] . " | " . $row [ 'type'];
			if ( strlen ( $pdf_text ) > 3 ) {
				$pdf->ChapterBody( $pdf_text );
				$pdf_text ="";
			}
		}
	}
}

if ( $printOK ) {
	if ( in_array ( 4, $prmode ) ) {
		$pdf->AddPage();
		$pdf_text .= "\n\n" . "Groepen";
		foreach ( $oMD [ "SGROEP" ] as $key => $value ) $pdf_text .= "\n" .  $key . " | " . $value;
		if ( strlen ( $pdf_text ) > 3 ) {
			$pdf->ChapterBody( $pdf_text );
			$pdf_text ="";
		}
		$pdf->AddPage();
		$pdf_text .= "\n\n" . "wijken";
		foreach ( $oMD [ "SWIJK" ] as $key => $value ) $pdf_text .= "\n" .  $key . " | " . $value;
		if ( strlen ( $pdf_text ) > 3 ) {
			$pdf->ChapterBody( $pdf_text );
			$pdf_text ="";
		}
	}
}

if ( $this->setting [ 'debug' ] == "yes" ){
	$pdf->AddPage();
	$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
	$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
	$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
	if ( strlen ( $selection ) > 1 ) $pdf_text .= sprintf ( "\n" . "Options %s : %s " , $project, $selection );
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}

if ( count ( $pdf_data ) > 0 ) {
	$pdf_header = array( '', '', '', '', '', '' );
	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data = array ( );
}

if ( strlen ( $pdf_text ) > 3 ) {
	$pdf->ChapterBody( $pdf_text );
	$pdf_text ="";
}
?>