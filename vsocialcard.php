<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* 1_module id */ 
$module_name = 'vsocialcard';
$version='20240616';
$main_file = "social";
$default_template = '/socialcard.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= "Sociale Kaart";

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_" . $main_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "sgroep" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "swijk" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* extra */
$oFC->page_content [ 'GROEP' ] = array();
$oFC->page_content [ 'WIJK' ] = array();
//$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = "active";
//$oFC->page_content [ 'REFERENCE_ACTIVE2' ] = "";
//$oFC->page_content [ 'REFERENCE_ACTIVE3' ] = "";	

foreach ( $oFC->setting [  'sgroep' ]  as $key => $value ) {
	$oFC->page_content [ 'GROEP' ] [ $key ] =  substr ($key, 0, 4) . "  " . $value ;
}

foreach ( $oFC->setting [  'swijk' ]  as $key => $value ) {
	$oFC->page_content [ 'WIJK' ] [ $key ] = $value;
}
$oFC->page_content [ 'WIJK' ] [ "AA" ] = "Woensel Noord";

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( 0 );

if ( $oFC->setting [ 'debug' ] == "yes" ) 	Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC ), __LINE__ . $module_name ); 

/* 20 some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
		case "down":
		case "Select":
		case "View":
		case "Save":
		case "New":
		case "Print": 
		case "Delete":
		case "Reset":
		default:
			/* escape route */
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
}
if ( isset( $_GET[ 'wijk' ] ) ) {
			$oFC->memory [ 1 ] = $_GET [ 'wijk' ];
}
if ( isset( $_GET[ 'rubr' ] ) ) {
			$oFC->memory [ 2 ] = $_GET [ 'rubr' ];
}
if ( $oFC->setting [ 'debug' ] == "yes" ) 	Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "memory"=>$oFC->memory ), __LINE__ . $module_name ); 


/* noodnummers voor  card1 */
$sql = sprintf ("SELECT t1.* FROM `%s` as t1  WHERE t1.`active` = '2' AND t1.`ref` LIKE  '%s' ORDER BY t1.`name` ASC", $oFC->file_ref  [ 99 ], 'AA%');
		$oFC->page_content [ 'cardnood' ] = array();
		$localArray = array();

		/* reading data */ 	
		LEPTON_database::getInstance()->execute_query(
			$sql,
			true,
			$localArray,
			true
		);
		
		/* processing data */
		foreach ( $localArray as $row) {
			$localHulpA = explode ( ',', $row [ 'content_groep' ] );
			$rowx = array ();
			$rowx [ 'id' ] = $row [ 'id' ];
			$rowx [ 'ref' ] = $row [ 'ref' ];
			$rowx [ 'type' ] = $row [ 'type' ];
			$rowx [ 'active' ] = $row [ 'active' ];
			$rowx [ 'name' ] = $row [ 'name' ];
			$rowx [ 'content_adres' ] = $row [ 'content_adres' ];
			$rowx [ 'content_url' ] = $row [ 'content_url' ];
			$rowx [ 'content_email' ] = $row [ 'content_email' ];
			$rowx [ 'content_tel' ] = $row [ 'content_tel' ];
			$rowx [ 'content_groep' ] = substr ( $row [ 'content_groep' ], 0, 4 );
			$rowx [ 'content_wijk' ] = sprintf ( "%s, %s" , $row [ 'ref' ], $row [ 'content_wijk' ] );
			$rowx [ 'content_groeptxt' ] = ucwords ("Meest " . str_replace ( $localHulpA [ 0 ] , "", $oFC->page_content [ 'GROEP' ] [ $localHulpA [ 0 ] ] ?? ""));		
			$rowx [ 'content_short' ] = $oFC->gsm_procesContent ( $row [ 'content_short' ] );
			$rowx [ 'content_long' ] = $oFC->gsm_procesContent ( $row [ 'content_long' ] ) ;
			$LocalHulpA = "";

			/* opmaak voor kopregel */
			if (strlen ( $row [ 'content_tel' ]) >2 ) {
				$LocalHulpA .= sprintf ( "Tel: %s", $row [ 'content_tel' ]);
			} elseif ( strlen ( $row [ 'content_url' ] ) >2 ) { 
				$LocalHulpA .= sprintf ( "Web: %s", $row [ 'content_url' ] );
			} elseif ( strlen ( $row [ 'content_email' ] ) >2 ) { 
				$LocalHulpA .= sprintf ( "Email: %s", $row [ 'content_email' ] );
			}

			/* verwijder het deel tussen () */
			$LocalHulpAarr = explode ( "(", $LocalHulpA );
			$rowx [ 'payload' ] = $LocalHulpAarr [ 0 ];
//			$rowx [ 'post_image' ] = $oFC->gsm_procesImage ( $row ['id'] , $row [ 'name' ] );
			$oFC->page_content [ 'cardnood' ] [ ] = $rowx;
		}

$active_arr = array (1,2);
$wijk_code = "" ;
$rubr_code = "" ;
/* wijkselectie 2e card */
$oFC->page_content [ 'cardwijk' ] =  $oFC->page_content [ 'WIJK' ];


/* sociale card groepen en met rubriek links */
$oFC->page_content [ 'cardrubr' ] = array ();
/* rubriek links */
$oFC->page_content [ 'carddata' ] = array ();

$wijk_code = $oFC->memory [1];
if ($wijk_code == "0" ) $wijk_code = "AA";
$oFC->page_content [ 'wijk_code'] = $wijk_code;
$oFC->page_content [ 'wijk_text'] = $oFC->page_content [ 'WIJK' ] [ $wijk_code ];

$rubr_code = $oFC->memory [2];
if ($rubr_code == "0" ) $rubr_code = "R203";
$oFC->page_content [ 'rubr_code'] = $rubr_code;
$oFC->page_content [ 'rubr_text'] = $oFC->page_content [ 'GROEP' ] [ $rubr_code ];

$oFC->page_content [ 'SUB_HEADER'] = ucwords ( $oFC->page_content [ 'rubr_text'] . " " . $oFC->page_content [ 'wijk_text'] );

foreach ( $oFC->page_content [ 'WIJK' ] as $code => $wijknaam) {
	if ( $wijk_code != $code ) continue;
	foreach ( $oFC->page_content [ 'GROEP' ] as $par => $groepnaam ) {
		$sql = sprintf ( "SELECT t1.* FROM `%s` as t1 ", $oFC->file_ref  [ 99 ] ); 	
		$sql .= " WHERE t1.`active` IN (" . implode ( ',', $active_arr ) . ")";
		if ( $code == "AA") {
			$sql .= " AND t1.`ref` LIKE  'AA%'";
		} else 	{
			$sql .= " AND ( t1.`ref` LIKE  'AA%' OR t1.`ref` LIKE '%" . $wijk_code . "%' OR t1.`content_wijk` LIKE '%" . $wijk_code . "%' ) ";
		}
		$sql .= " AND t1.`content_groep` LIKE  '%" . $par. "%'";
		$sql .= " ORDER BY t1.`name` ASC ";
		if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ( $sql ), __LINE__ . $template_name ); 
		$localArray = array();
		$database->execute_query ( 
			$sql, 
			true, 
			$localArray );
		if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ( $localArray ), __LINE__ . $template_name ); 	
		/* test op selectable rubriek */	
		if ( count 	( $localArray ) == 0 ) continue;
		/* dus in selectable rubrieken */
		$oFC->page_content [ 'cardrubr' ] [ $par ] = ucwords ( $groepnaam );
		/* test te tonen */
		if ( $oFC->page_content [ 'rubr_code'] != $par ) continue;
		$n = strlen ( $rubr_code );
		if ($n < 2 ) continue;
		/* dus in te tonen entries */
		foreach ( $localArray as $row) {
			$localHulpA = explode ( ',', $row [ 'content_groep' ] );
			$sw = false; 
			foreach ( $localHulpA as $index ) { 
				if ( substr ( $index, 0, $n ) == $rubr_code ) $sw = $index;
			}
			if ( $sw == false ) continue;
			$rowx = array ();
			$rowx [ 'id' ] = $row [ 'id' ];
			$rowx [ 'ref' ] = $row [ 'ref' ];
			$rowx [ 'type' ] = $row [ 'type' ];
			$rowx [ 'active' ] = $row [ 'active' ];
			$rowx [ 'name' ] = $row [ 'name' ];
			$rowx [ 'content_adres' ] = $row [ 'content_adres' ];
			$rowx [ 'content_url' ] = $row [ 'content_url' ];
			$rowx [ 'content_email' ] = $row [ 'content_email' ];
			$rowx [ 'content_tel' ] = $row [ 'content_tel' ];
			$rowx [ 'content_groep' ] = substr ( $row [ 'content_groep' ], 0, 4 );
			$rowx [ 'content_wijk' ] = sprintf ( "%s, %s" , $row [ 'ref' ], $row [ 'content_wijk' ] );
			$rowx [ 'content_groeptxt' ] = ucwords ( str_replace ( $localHulpA [ 0 ] , "", $oFC->page_content [ 'GROEP' ] [ $localHulpA [ 0 ] ] ?? "") );		
			$rowx [ 'content_short' ] = $oFC->gsm_procesContent ( $row [ 'content_short' ] );
			$rowx [ 'content_long' ] = $oFC->gsm_procesContent ( $row [ 'content_long' ] ) ;
			$LocalHulpA = "";

			/* opmaak voor kopregel */
			if (strlen ( $row [ 'content_tel' ]) >2 ) {
				$LocalHulpA .= sprintf ( "Tel: %s", $row [ 'content_tel' ]);
			} elseif ( strlen ( $row [ 'content_url' ] ) >2 ) { 
				$LocalHulpA .= sprintf ( "Web: %s", $row [ 'content_url' ] );
			} elseif ( strlen ( $row [ 'content_email' ] ) >2 ) { 
				$LocalHulpA .= sprintf ( "Email: %s", $row [ 'content_email' ] );
			}

			/* verwijder het deel tussen () */
			$LocalHulpAarr = explode ( "(", $LocalHulpA );
			$rowx [ 'payload' ] = $LocalHulpAarr [ 0 ];
			$oFC->page_content [ 'carddata' ] [ ] = $rowx;
		}
	}
}

/* 98 memory save */
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 3 ); 
	
/* 99 output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id;

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 