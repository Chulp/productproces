<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$this->version ['print1'] = "20241214";	
if ($this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( "post" =>$_POST ?? "", "get" =>$_GET ?? "", "this" => $this ), __LINE__ . 'print1' .$this->version ['print1'] ); 

/* preset input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "Onderhoud", "Overzicht", $project ) ) );
$prmode = $func ?? "1";

/* date range */
$cleanedSelection = $query;
if ( strlen ( $cleanedSelection  ) < 4 )  $cleanedSelection = "today";
$dateString = strtotime ( $cleanedSelection );
$monthStart = date( 'Y-m-01 00:00:00',  $dateString );
$month = 'P'. date_format ( date_create ( $monthStart ), 'U');
$monthEnd = date ( date ( "Y-m-d", $dateString ).' 23:59:59');
$monthE = 'P'.date_format ( date_create ( $monthEnd ), 'U');
//$title .= sprintf ( ' %s - %s .' , substr ( $monthStart, 0, 10) , substr ( $monthEnd, 0, 10) );
$title .= sprintf ( '  %s .' , substr ( $monthEnd, 0, 10) );

if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array(  
	"title"  => $title,
	"query" => $query,
	"selection" => $selection,
	"parameter" => $this->page_content [ 'PARAMETER' ],
	"func" => $prmode,
	"loc" => $loc,	
	"project" => $project,
	"owner" => $owner,	
	"run" => $run, 
	"search_mysql" => $this->search_mysql,
	"data" => $this->page_content,
	"setting" => $this->setting),
	__LINE__ . 'print1' .$this->version ['print1'] ); 

$this->setting [ 'all' ] = false;
$this->setting [ 'active' ] = false;
$this->setting [ 'huur' ] = false;
$this->setting [ 'nul' ] = false;
$lowest = 0.25;
if ( strstr ( $selection, "all" ) ) $this->setting [ 'all' ] = true;
if ( strstr ( $selection, "active" ) ) $this->setting [ 'active' ] = true;
if ( strstr ( $selection, "huur" ) ) $this->setting [ 'huur' ] = true;
if ( strstr ( $selection, "nul" ) ) { 
	$this->setting [ 'nul' ] = true;
	$lowest = 0.05;
}

global $owner;
$owner = $owner;
global $title;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf_data   = array( );
$pdf_text   = '';

/* end search construct */
$search_mysql = $this->search_mysql;

$sql = "SELECT `" . $this->file_ref[ 99 ] . "`.* ,";
$sql .= " `" . $this->file_ref[ 98 ] . "`.`amt1` AS `amtvat` ,";
$sql .= " `" . $this->file_ref[ 98 ] . "`.`name` AS `refgrp` ";
$sql .= "	FROM `" . $this->file_ref[ 99 ] . "` ";
$sql .= "	LEFT JOIN `" . $this->file_ref[ 98 ] . "` ";
$sql .= "	ON `" . $this->file_ref[ 98 ] . "`.`id` = `" . $this->file_ref[ 99 ] . "`.`type` "; 
$sql .= sprintf ( "%s ORDER BY `%s`.`name`, `%s`.`active` DESC, `%s`.`name` ASC ",
	$search_mysql,
	$this->file_ref [ 98 ],
	$this->file_ref [ 99 ],
	$this->file_ref [ 99 ] );
$results = array ();
$database->execute_query ( 
	$sql, 
	true, 
	$results );

$regelcount = count ( $results );
if ( $regelcount < 1 ) $pdf_text .= "\n\n" .  $this->language [ 'pdf' ][9];

/* create article list */
$cal = array();
foreach ( $results as $row ) $cal [ $row[ 'ref' ]  ] = 0 ;
/* create stocklist */ 
$temp = $this->gsm_StockLine ( 1, $cal);

if ($this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( $sql, "cal" =>$cal ?? "", "stock" =>$temp ?? "", $results ), __LINE__ . 'print1' .$this->version ['print1'] ); 

/* debug * / gsm_debug ( array ( sprintf ("SELECT * FROM `%s` ORDER BY `id`", $this->file_ref  [ 97 ]), $GroepArr , $GroepBTWArr), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */

/*Initialisering L0*

/* cycle through the process records */
$this->cols[ 'L0' ] = true;

$n = 0;
foreach ( $results as  $rowp ) {
	if ( $rowp [ 'active' ] < 1 && $this->setting [ 'active' ] != true ) continue; // skip not active records
	/* **************** Initialisering L0*/
	if ( $this->cols[ 'L0' ] ) {
		$pdf_header = array( "ref", "Omschrijving", "Aantal", "Bedrag", "Tarief", "");
		$pdf_cols = array( 20, 80, 25, 35, 30, 0 ); 
		$this->cols[ 'L0' ] = false;
		$this->cols[ 'L1' ] = "--";
		$this->cols[ 'L2' ] = "--";
		$this->cols [ 'L0_T1' ] = array();
		$this->cols [ 'L1_T1' ] = array();
		$this->cols [ 'L2_T1' ] = array();	
	}
	
	/* **************** Afsluiten L2 */
	$L2_break = sprintf ( "%s ( %s - %s ) ", $rowp [ 'refgrp' ], $rowp [ 'type' ], $this->language [ 'active' ] [ $rowp [ 'active' ] ] );
	if ( $this->cols[ 'L2' ] != $L2_break ) {
		if ( count ($pdf_data) > 0  ) {
			/* L2 eind processing	*/
			/* L2 totaal processing	*/
			if ( !$this->setting [ 'nul' ] ){
				$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
				$pdf_data = array( );
				$pdf_text .= "\n";
				$pdf->ChapterBody( $pdf_text );
				$pdf_text   = '';
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					$this->cols[ 'L2' ],
					"",
					"",
					"",
					"") ) );	
				foreach ( $this->cols [ 'L2_T1' ] as $key => $value) {
					if ( !isset ( $this->cols [ 'L1_T1' ] [ $key ] ) ) $this->cols [ 'L1_T1' ] [ $key ] = 0;
					$this->cols [ 'L1_T1' ] [ $key ] = $this->cols [ 'L1_T1' ] [ $key ] + $value;
					if ($key == 99 ) {
						$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
							"", 
							"cum aantallen",
							$this->gsm_sanitizeStringS (  $value , "s{WHOLE}" ) ,
							"",
							"",
							"") ) );
					} elseif ($key == 98 ) {
						$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
							"", 
							"cum statiegeld (product gebonden)",
							"",
							$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ),
							"",
							"") ) );
					} else {
						$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
							"", 
							"cum total",
							"btw% " . $key,
							$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ) ,
							"",
							"") ) );
					}
				}
			}
			$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
			$pdf_data = array( );	
			$pdf_text .= "\n";
			$pdf->ChapterBody( $pdf_text );
			$pdf_text .= " ";
		}
	}
	/* **************** Afsluiten L1 */
	$L1_break = sprintf ( "%s ( %s )", $rowp [ 'refgrp' ],  $rowp [ 'type' ]  );
	if ( $this->cols [ 'L1' ] !=  $L1_break ) { 
		/* L1 eind processing	*/
		foreach ( $this->cols [ 'L1_T1' ] as $key => $value) {
			if ( !isset ( $this->cols [ 'L0_T1' ] [ $key ] ) ) $this->cols [ 'L0_T1' ] [ $key ] = 0;
			$this->cols [ 'L0_T1' ] [ $key ] = $this->cols [ 'L0_T1' ] [ $key ] + $value;
		}
		/* L1 totaal processing	*/		
		if ( isset ($this->cols [ 'L1_T1' ] [ 99 ] ) && $this->cols [ 'L1_T1' ] [ 99 ] > $lowest ) { 
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"", 
				$this->cols[ 'L1' ],
				"",
				"",
				"",
				"") ) );
			foreach ( $this->cols [ 'L1_T1' ] as $key => $value) {
				if ($key == 99 ) {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum groep aantallen",
						$this->gsm_sanitizeStringS (  $value , "s{WHOLE}" ) ,
						"",
						"",
						"") ) );
				} elseif ($key == 98 ) {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum groep statiegeld (product gebonden)",
						"",
						$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ),
						"",
						"") ) );
				} else {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum groep total",
						"btw% " . $key,
						$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ) ,
						"",
						"") ) );
				}
			}
			$this->cols [ 'L1_T1' ] = array();
			$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
			$pdf_data = array( );
		}
	}
	/* **************** Initialisering L1 */	
	if ( $this->cols[ 'L1' ] !=  $L1_break ) {
		$this->cols[ 'L1' ] =  $L1_break ;
		$this->cols[ 'L2' ] = "--";	
		/* L1 begin processing */
		$this->cols [ 'L1_T1' ] = array();	
	}
	/* L2 Initialisering */
	if ( $this->cols[ 'L2' ] != $L2_break ) {
		$this->cols[ 'L2' ] = $L2_break;
		$this->cols [ 'L2_T1' ] = array();	
		/* l2 begin processing */
		$pdf_text .= "\n";
		$pdf->ChapterBody( $pdf_text );
		$pdf_text   = '';
		if ( $n > 0 ) $pdf->AddPage();
		$n++;
		$pdf->ChapterTitle( $n, $L2_break );
	} 

	$LocalHulpA = round ( $rowp [ 'amt2' ]  * ( 100 + $rowp [ 'amt0' ] ) / 100 , 2 ) ;
	$rowp [ 'amt2b' ] = $this->gsm_sanitizeStringS ( $LocalHulpA, "s{KOMMA|EURO}" );
	/* marge */
	if ($rowp [ 'amt1' ] < $lowest || $rowp [ 'amt3' ] < $lowest ) {
		$rowp [ 'amt1b' ] = "--";
		$rowp [ 'amt3b' ] = "--";
	} else {
		/* korting */
		$rowp [ 'amt1b' ] = $this->gsm_sanitizeStringS ( round ( ( 1- ( $LocalHulpA / $rowp [ 'amt1' ] ) ) * 100 , 2), "s{KOMMA}" );
		/* marge */
		$rowp [ 'amt3b' ] = $this->gsm_sanitizeStringS ( round ( ( ( $rowp [ 'amt2' ] / $rowp [ 'amt3' ] ) -1 ) * 100 , 2), "s{KOMMA}" );
	}
	/* voorraad */
	$rowp [ 'amt5b' ] = $rowp [ 'amt5' ];  // voorraad rekenveld
	$rowp [ 'amt10b' ] = 0;   // aantallen van de facturen die niet meedoen
	$rowp [ 'amt11b' ] = 0;	 // aantallen van de voorraad
	$rowp [ 'amt12b' ] = 0;   // aantallen in uitlevering
	$rowp [ 'amt13b' ] = 0;	 // aantallen gereserveerd
	foreach ( $temp [ $rowp [ 'ref' ] ] as $key => $value ) {
		if (in_array ( $key, $this->setting [ 'stock0'] ) ) { // verwerkt
			$rowp [ 'amt10b' ] = $rowp [ 'amt10b' ] + $value;
			$rowp [ 'amt5b' ] = $rowp [ 'amt5b' ] - $value;
		} 
		if (in_array ( $key, $this->setting [ 'stock1'] ) ) { // verwerkt
			$rowp [ 'amt11b' ] = $rowp [ 'amt11b' ] + $value;
			$rowp [ 'amt5b' ] = $rowp [ 'amt5b' ] - $value;
		} 

		if (in_array ( $key, $this->setting [ 'stock2'] ) ) { // uitleveren
			$rowp [ 'amt12b' ] = $rowp [ 'amt12b' ] + $value;
		} 
		if (in_array ( $key, $this->setting [ 'stock3'] ) ) { // projectie
			$rowp [ 'amt13b' ] = $rowp [ 'amt13b' ] + $value;
		} 
	}
	if ( $rowp [ 'amt5b' ] < $lowest ) $rowp [ 'amt5b' ]  = 0;
	$rowp [ 'amt6b' ] = round ( (   $rowp [ 'amt5b' ] * $rowp [ 'amt3' ] ), 2);
	$rowp [ 'amt7b' ] = round ( (   $rowp [ 'amt5b' ] * $rowp [ 'amt4' ] ), 2);
	/* Totalen per btw , statiegeld en aantallen */
	if ( $rowp [ 'active' ] != 3 ) {
		if ( isset ($this->cols [ 'L2_T1' ] [ $rowp [ 'amt0' ] ] ) ) {
			$this->cols [ 'L2_T1' ] [ $rowp [ 'amt0' ] ] = $this->cols [ 'L2_T1' ] [ $rowp [ 'amt0' ] ] + $rowp [ 'amt6b' ];
		} else { 
			$this->cols [ 'L2_T1' ] [ $rowp [ 'amt0' ] ] = $rowp [ 'amt6b' ];
		}

		if ( isset ($this->cols [ 'L2_T1' ] [ 98 ] ) ) {
			$this->cols [ 'L2_T1' ] [ 98 ] = $this->cols [ 'L2_T1' ] [ 98 ] + $rowp [ 'amt7b' ];
		} else { 
			$this->cols [ 'L2_T1' ] [ 98 ]  = $rowp [ 'amt7b' ];
		}
	}
	if ( isset ($this->cols [ 'L2_T1' ] [ 99 ] ) ) {
		$this->cols [ 'L2_T1' ] [ 99 ]  = $this->cols [ 'L2_T1' ] [ 99 ] + $rowp [ 'amt5b' ];
	} else { 
		$this->cols [ 'L2_T1' ] [ 99 ]  = $rowp [ 'amt5b' ];
	}
	if ( $this->setting [ 'all' ] == true ) {	
		if ( $rowp [ 'active' ] == 3 ) {
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				$rowp [ 'ref' ],  
				$rowp [ 'name' ],
				( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt5b' ] , "s{WHOLE}" ) : "",
				"",
				"",
				"") ) );
		} else {
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				$rowp [ 'ref' ], 
				$rowp [ 'name' ], 
				( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt5b' ] , "s{WHOLE}" ) : "",
				( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt6b' ] , "s{EURT|KOMMA}" ) : "",
				( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt3' ] , "s{KOMMA}" ) : "",
				'') ) );
		} 
		$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
			"",
			"verkoop ex",
			'',
			'',
			$this->gsm_sanitizeStringS (  $rowp [ 'amt2' ] , "s{KOMMA}" ),
			'') ) );
		$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
			"",
			"statiegeld",
			'',
			( $rowp [ 'amt7b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt7b' ] , "s{EURT|KOMMA}" ) : "",
			$this->gsm_sanitizeStringS (  $rowp [ 'amt4' ] , "s{KOMMA}" ),
			'') ) );
		if ( $rowp [ 'amt10b' ] > 0 ) 
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"",
				"historie",
				$this->gsm_sanitizeStringS (  $rowp [ 'amt10b' ] , "s{WHOLE}" ),
				'',
				'',
				'') ) );	
		if ( $rowp [ 'amt11b' ] > 0 ) 
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"",
				"gefactureerd",
				$this->gsm_sanitizeStringS (  $rowp [ 'amt11b' ] , "s{WHOLE}" ),
				'',
				'',
				'') ) );	
		if ( $rowp [ 'amt12b' ] > 0 ) 
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"",
				"wacht op levering",
				$this->gsm_sanitizeStringS (  $rowp [ 'amt12b' ] , "s{WHOLE}" ),
				'',
				'',
				'') ) );
		if ( $rowp [ 'amt13b' ] > 0 ) 
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				__LINE__,
				"wacht op levering",
				$this->gsm_sanitizeStringS (  $rowp [ 'amt13b' ] , "s{WHOLE}" ),
				'',
				'',
				'') ) );
	} else {
		if ( $rowp [ 'active' ] == 3 && $rowp [ 'amt5b' ] > $lowest ) {
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				$rowp [ 'ref' ], 
				$rowp [ 'name' ],
				( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt5b' ] , "s{WHOLE}" ) : "",
				"",
				"",
				"") ) );
		} elseif ( $rowp [ 'amt5b' ] > $lowest ) {
			if ( $this->setting [ 'nul' ] ) {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					$rowp [ 'ref' ], 
					$rowp [ 'name' ], 
					"",
					"",
					( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt3' ] , "s{KOMMA}" ) : "",
					"") ) );
			} else {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					$rowp [ 'ref' ], 
					$rowp [ 'name' ], 
					( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt5b' ] , "s{WHOLE}" ) : "",
					( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt6b' ] , "s{EURT|KOMMA}" ) : "",
					( $rowp [ 'amt5b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt3' ] , "s{KOMMA}" ) : "",
					"") ) );
			}
			if 	( $rowp [ 'amt7b' ] > $lowest ) {
				if ( $this->setting [ 'nul' ] ) {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"",
					"statiegeld",
					'',
					"",
					$this->gsm_sanitizeStringS (  $rowp [ 'amt4' ] , "s{KOMMA}" ),
					'') ) );
				} else {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"",
						"statiegeld",
						'',
						( $rowp [ 'amt7b' ] > $lowest ) ? $this->gsm_sanitizeStringS (  $rowp [ 'amt7b' ] , "s{EURT|KOMMA}" ) : "",
						$this->gsm_sanitizeStringS (  $rowp [ 'amt4' ] , "s{KOMMA}" ),
						'') ) );
				}
			}
				
		} 
	}
}
if ( $regelcount > 0 ) {
	/* **************** Afsluiten L1 */
	if ( count ($pdf_data) > 0 ) {
		/* L2 eind processing	*/
		/* L2 totaal processing	*/
		if ( !$this->setting [ 'nul' ] ) {
			$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
			$pdf_data = array( );
			$pdf_text .= "\n";
			$pdf->ChapterBody( $pdf_text );
			$pdf_text   = '';
			$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"", 
				$this->cols[ 'L2' ],
				"",
				"",
				"",
				"") ) );	
			foreach ( $this->cols [ 'L2_T1' ] as $key => $value) {
				if ( !isset ( $this->cols [ 'L1_T1' ] [ $key ] ) ) $this->cols [ 'L1_T1' ] [ $key ] = 0;
				$this->cols [ 'L1_T1' ] [ $key ] = $this->cols [ 'L1_T1' ] [ $key ] + $value;
				if ($key == 99 ) {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum aantallen",
						$this->gsm_sanitizeStringS (  $value , "s{WHOLE}" ) ,
						"",
						"",
						"") ) );
				} elseif ($key == 98 ) {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum statiegeld (product gebonden)",
						"",
						$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ),
						"",
						"") ) );
				} else {
					$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
						"", 
						"cum total",
						"btw% " . $key,
						$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ) ,
						"",
						"") ) );
				}
			}
		}
		$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array( );	
		$pdf_text .= "\n";
		$pdf->ChapterBody( $pdf_text );
		$pdf_text .= " ";
	}
	/* **************** Afsluiten L1 */
	foreach ( $this->cols [ 'L1_T1' ] as $key => $value) {
		if ( !isset ( $this->cols [ 'L0_T1' ] [ $key ] ) ) $this->cols [ 'L0_T1' ] [ $key ] = 0;
		$this->cols [ 'L0_T1' ] [ $key ] = $this->cols [ 'L0_T1' ] [ $key ] + $value;
	}
	/* L1 totaal processing	*/		
	if ( isset ($this->cols [ 'L1_T1' ] [ 99 ] ) && $this->cols [ 'L1_T1' ] [ 99 ] > $lowest ) { 
		$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
			"", 
			$this->cols[ 'L1' ],
			"",
			"",
			"",
			"") ) );
		foreach ( $this->cols [ 'L1_T1' ] as $key => $value) {
			if ($key == 99 ) {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 

					"cum groep aantallen",
					$this->gsm_sanitizeStringS (  $value , "s{WHOLE}" ) ,
					"",
					"",
					"") ) );
			} elseif ($key == 98 ) {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					"cum groep statiegeld (product gebonden)",
					"",
					$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ),
					"", 
					"") ) );
			} else {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					"cum groep total",
					"btw% " . $key,
					$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ) ,
					"",
					"") ) );
			}
		}
		$this->cols [ 'L1_T1' ] = array();
		$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array( );
		$pdf->ChapterBody( $pdf_text );
		$pdf_text .= " ";
		$pdf->AddPage();
	}
/* **************** Afsluiten L0 */
/* L0 totaal processing	*/	
	if ( !$this->setting [ 'nul' ] ){	
		$n++;
		$pdf->AddPage();
		$pdf->ChapterTitle( $n, "Totaal Generaal" );
		$generaal=0;
		foreach ( $this->cols [ 'L0_T1' ] as $key => $value) {
			if ($key == 99 ) {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					"Totaal aantallen",
					$this->gsm_sanitizeStringS (  $value , "s{WHOLE}" ) ,
					"",
					"",
					"") ) );
			} elseif ($key == 98 ) {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					"Statiegeld ( product gebonden )",
					"btw% " . "--",
					$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ),
					"",
					"") ) );
				$generaal = $generaal + $value;	
			} else {
				$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
					"", 
					"Totalen",
					"btw% " . $key,
					$this->gsm_sanitizeStringS (  $value , "s{EURT|KOMMA}" ) ,
					"",
					"",
					"") ) );
				$generaal = $generaal + $value;
			}
		}

		$pdf_data[ ] = explode( ';', trim( sprintf ( __LINE__ ." %s;%s;%s;%s;%s;%s",
				"", 
				"Totaal Generaal",
				"",
				$this->gsm_sanitizeStringS (  $generaal , "s{EURT|KOMMA}" ) ,
				"",
				"",
				"") ) );
	}
	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data = array( );

}

/* end */

// footer
$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
if ( strlen( $query ) > 1 ) $pdf_text .= sprintf ( "\n %s %s :\n %s %s" . "\n"  ,  $this->language [ 'pdf' ][ 2 ], $project, $selection, $query );

if ($this->setting [ 'debug' ] == "yes" ){
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}

//  last pdf output
if ( count ($pdf_data) >0 )	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data  = array( );
if ( $pdf_text != "" ) $pdf->ChapterBody( $pdf_text );
$pdf_text   = '';

?>