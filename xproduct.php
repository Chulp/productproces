<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* 1_module id */ 
$module_name = 'xproduct';
$version='v20241117';
$main_file = "product";
$unique = 'ref';
$sub_file = "group";
$default_template = '/' . $main_file . '.lte';

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , $oFC-> language [ 'TXT_MAINTENANCE' ], strtoupper ( $main_file )) ;

/* 4 file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_" . $main_file;
$oFC->file_ref  [ 98 ] = LOAD_DBBASE . "_" . $sub_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );


/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 9 wysiwyg editor */
if ( LOAD_MODE == "x" ) {
	if (!defined('WYSIWYG_EDITOR') || 
		WYSIWYG_EDITOR=="none" || 
		!file_exists(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php')) {
		function show_wysiwyg_editor($name,$id,$content,$width,$height, $promp) {
			$sHTMLCode = '<textarea name="'.$name.'" id="'.$id.'" rows="10" cols="1" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
			if(true === $promp)
			{
				echo $sHTMLCode;
			} else {
				return $sHTMLCode;
			}
		}
	} else {
		$id_list=["gsm_content_short","gsm_content_long"];
		require_once LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php';
	}
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( );

/* 12 selection functions */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "print" => "print" , "all" => "all", "debug" => "debug") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

if ( strstr ( $xmode, "debug" ) ) $oFC->setting [ 'debug' ] = "yes";
if ( $oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST ?? "", "get" => $_GET ?? "", "setting" => $oFC->setting, "content" => $oFC->page_content, $selection, $xmode ), __LINE__ . $module_name ); 

/* 13 selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );


/* 14 Group list for selection */
$cal = array();
$results = array ( );
$database->execute_query ( sprintf ("SELECT * FROM `%s` WHERE `active` = 1 ORDER BY `ref`", $oFC->file_ref  [ 98 ] ),
	true, 
	$results );
if ( count( $results ) > 0 ) {
	foreach ( $results as $row ) 
		$cal [ $row[ 'id' ] ] = $row[ 'ref' ] ;
}
$groepsArr = $cal;

/* 18 Print function needed */
if ( isset ( $xmode ) && strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_product_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );

/* 19 sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* 20 some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "Select":
		case "View":
			if ( !isset( $_POST[ 'vink' ][ 0 ] ) ) 	break;
			$oFC->page_content [ 'MODE' ] = 8;
			$oFC->recid = $_POST[ 'vink' ][ 0 ];
			$FieldArr = array ();
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
			break;
		case "Save":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			/*  10_specific handling */
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file, "gsm_", $unique );
 			/* attachment */
			$oFC->setting [ 'allowed' ] = array ( "jpg" );
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ] . "/",'%5$s%2$s', "image", $oFC->recid  );
			/* end attachment */
			break;
		case "New":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			/*  11_specific handling */
			$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 2, $main_file, "gsm_" , $unique );
 			/* 12_attachment */
			$oFC->setting [ 'allowed' ] = array ( "jpg" );
			$oFC->description .= $oFC->gsm_uploadFile ( $oFC->setting [ 'mediadir' ]. "/",'%5$s%2$s', "image", $oFC->recid  );
			/* end attachment */
			break;
		case "Print": 
		case "Delete":
		case "Reset":
			$oFC->recid = '';
			$oFC->search_mysql = "";
			$selection= "";
			$oFC->page_content [ 'PARAMETER' ] = $selection;
			$oFC->page_content [ 'SUB_HEADER' ]= "____";
			break;
		default:
			/* escape route */
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		/* is a record selected */ 
		case 'select': 
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 8;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
			}
			break;
		/* Delete a record */
		case 'remove': //    
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 9;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 3, $main_file );
				$oFC->recid = "";
			}
			break;
		/* End Delete a record */
		default:
			/* escape route */
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	$oFC->page_content [ 'P1' ] = true;
}

/* 31_Additional function */ 
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' ); 
if ( isset ( $oFC->setting [ 'pdf_filename' ] )  && strlen( $oFC->setting [ 'pdf_filename' ] ) > 5 ) {
	$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 1 );
} 

/* 40 display preparation */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 8:
		break;
	default: 
		$pageok = true;
		
		/* 33 bepaal aantal records */
		$result = array ( );
		$database->execute_query ( sprintf ( 
			"SELECT count(`id`) FROM `%s` %s", 
			$oFC->file_ref [ 99 ], 
			$oFC->search_mysql ), true, $result );
		$row = current ( $result );
		
		/* 34 lege file */
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		if ( $oFC->page_content [ 'aantal' ] == 0 && strlen ($oFC->search_mysql) < 15 ) {
			// leeg record toevoegen
			$updatearr = array( 'ref' => "initial", 'name' => "initial" );
			$database->build_and_execute ( 
				"insert",
				$oFC->file_ref [ 99 ],
				$updatearr );
		}
		
		/* 35 paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ( "sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		$fields = array( "id", 
			"type",
			"ref", 
			"name", 
			"content_short", 
			"zoek", 
			"active" );
		$query  = "SELECT `" . implode ("`, `", $fields) . "` FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= sprintf ( "%s ORDER BY `type`, `zoek` ASC %s", 
			$oFC->search_mysql, 
			$limit_sql );

		/* read records */
		$results = array ( );
		$resultPL0 = array ( );	
		$resultPL1 = array ( );
		$n = $oFC->page_content [ 'POSITION' ];
		$job = array ( );
		$vorig_unique= "-----";
		$database->execute_query ( 
			$query, 
			true, 
			$results );
		if (count ( $results ) > 0 ) { 

			$PL0 = "=="; //levelbreaks
			$PL1 = "=="; 	
			foreach ( $results as  $row ) {
				
				/* subtotaals type accordeon */
				if ( $PL0 != $row [ 'type' ] ) {
					if ( count ( $resultPL1 ) > 0 ) $resultPL0 [ $PL0 ] = $resultPL1 ; 
					$resultPL1 = array ( );
					$PL0 = $row [ 'type' ];
					$PL1 = "=="; 
				}
				$updateArr = array(); 	
				
				if ( count( $groepsArr ) > 0 ) {
					$row [ 'TYPE' ] = $groepsArr [ $row [ 'type' ] ] ;
				} else {			
					if ( strlen ( $row [ 'type' ] ) > 18 ) 
						$updateArr [ 'type' ] = substr ( $row [ 'type' ], 0, 15 );
				}
				
				/* make zoek unique */
				if ( $PL1 == $row [ 'ref' ] )	{	
					$updateArr [ 'ref' ] = $row [ 'ref' ] . "-"; 
					$updateArr [ 'zoek' ] =  "|" . $row [ 'type' ] . "|" . $updateArr [ 'ref' ] . "|" ; 
				} else {
					/* recreate zoek */	
					$PL1 = $row [ 'ref' ];
					$arout = $oFC->setting [ 'zoek' ] [ $main_file ];
					foreach ( $row as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
					$arout = str_replace ( "||", "|", $arout );			
					$arout = substr (strtolower ( $oFC->gsm_sanitizeStrings ( $arout, 's{TOASC|LOWER}' ) ), 0, 225 ); 
					if ( $row [ 'zoek' ] != $arout ) $updateArr ['zoek'] = $arout;
				}
				/* group */
				if ( count( $groepsArr ) > 0 ) 
				$row [ 'TYPE' ] = $groepsArr [ $row [ 'type' ] ] ?? "--??--";
				/* opmaak */
				foreach ( $row as $pay => $load ) { 
					if ( substr ( $pay, 0, 3 ) == "amt") {
						$row [ $pay ] = $oFC->gsm_sanitizeStringS ( $load, "s{KOMMA}");
					} elseif ( substr ( $pay, 0, 3 ) == "aan"){
//						$row [ $pay ] = $oFC->gsm_sanitizeStringV ( $load, $AANfilter );
					} elseif ( substr ( $pay, 0, 3 ) == "dat") {
//						$row [ $pay ] = $oFC->gsm_sanitizeStringD ( $load, $DATfilter );
					} elseif ( substr ( $pay, 0, 3 ) == "tim") {
//						$row [ $pay ] = $oFC->gsm_sanitizeStringD ( $load, $TIMfilter );
					}
				}
				/* attachment */
				$imageref = sprintf ( "%s%s/image%s.jpg", LEPTON_PATH, $oFC->setting [ 'mediadir' ], $row [ 'id' ] );
				if ( file_exists( $imageref ) ) $row [ "IMAGE" ] = '<img class="ui medium image" src="'.str_replace ( LEPTON_PATH, LEPTON_URL, $imageref ).'">';
				/* update */			
				if ( isset ( $updateArr ) && count ( $updateArr ) > 0 ) $job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $updateArr, 2 ) . " WHERE `id` = '" . $row [ 'id' ] . "'"; 
				/* output */
				$resultPL1 [] = $row;
			}
		
			/* end subtotaal type accordeon */
			if ( count ( $resultPL1 ) > 0 ) $resultPL0 [ $PL0 ] = $resultPL1 ; 
			
			/* repairs */
			if ( isset ( $job ) && count ( $job ) > 0 ) {
				foreach ( $job as $key => $query ) $database->simple_query ( $query ) ; 
				$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . " Aantal records aangepast : ". count ( $job ). NL;
			}
		} else {
			$oFC->page_content [ 'RECID' ] = "";
			$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_DATA' ].NL;
		}
		$oFC->page_content [ 'RESULTS' ] = $resultPL0;
		break;
}

/* the opmaak en selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		$oFC->page_content [ 'TOEGIFT' ] = ""; 
		foreach ($oFC->language [ 'DUMMY' ] as $pay => $load ) $oFC->page_content [ 'TOEGIFT' ] .=  $load . NL; 
		break;

	case 8:
		/* active */
		$oFC->page_content [ 'RESULT' ] [ 'ACTIVE' ] = $oFC->gsm_selectOption ( $oFC->language [ 'active' ], $oFC->page_content [ 'RESULT' ] [ 'active' ], 1);
		/* group */
		if ( count( $groepsArr ) > 0 ) 
			$oFC->page_content [ 'RESULT' ] [ 'TYPE' ] = $oFC->gsm_selectOption ($groepsArr, $oFC->page_content [ 'RESULT' ] [ 'type' ], 1);
		/* opmaak */
		foreach ( $oFC->page_content [ 'RESULT'] as $pay => $load ) { 
			if ( substr ( $pay, 0, 3 ) == "amt") {
				$oFC->page_content [ 'RESULT'] [ $pay ] = $oFC->gsm_sanitizeStringS ( $load, "s{KOMMA}");
			} elseif ( substr ( $pay, 0, 3 ) == "aan"){
//				$oFC->page_content [ 'RESULT'] [ $pay ] = $oFC->gsm_sanitizeStringV ( $load, $AANfilter );
			} elseif ( substr ( $pay, 0, 3 ) == "dat") {
//				$oFC->page_content [ 'RESULT'] [ $pay ] = $oFC->gsm_sanitizeStringD ( $load, $DATfilter );
			} elseif ( substr ( $pay, 0, 3 ) == "tim") {
//				$oFC->page_content [ 'RESULT'] [ $pay ] = $oFC->gsm_sanitizeStringD ( $load, $TIMfilter );
			}
		}
		
		/* backend use editor */
		if (LOAD_MODE == "x" ) {		
			$oFC->page_content [ 'show_wysiwyg_editor_short' ] = show_wysiwyg_editor('gsm_content_short','gsm_content_short', $oFC->page_content [ 'RESULT' ] ['content_short'],'100','200', false);
			$oFC->page_content [ 'show_wysiwyg_editor_long' ] = show_wysiwyg_editor('gsm_content_long','gsm_content_long', $oFC->page_content [ 'RESULT' ] ['content_long'],'100','400', false);    
		}
		
		/* attachment */
		$imageref = sprintf ( "%s%s/image%s.jpg", LEPTON_PATH, $oFC->setting [ 'mediadir' ], $oFC->recid );
		if ( file_exists( $imageref ) ) $oFC->page_content [ 'RESULT'] [ "IMAGE" ] = '<img class="ui medium image" src="'.str_replace ( LEPTON_PATH, LEPTON_URL, $imageref ).'">';
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 2, 6, 8, 11), '-', $pdflink ?? "-" );
		break;
		
	case 9:
	default: 
		/* knoppen */
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 10 ), "-", "-" ,"0", "-", "-" , "print" );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6, 11) , '-', $pdflink ?? "-" );
		break;
} 
 
/* 98 memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( ); 
	
/* 99 output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id;

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 