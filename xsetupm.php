<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* 1 module id */ 
$module_name = 'xsetupm';
$version = '20241124';
$main_file = "product";
$sub_file = "group";
$sub2_file = "proces";
$sub3_file = "adres";
$sub4_file = "agenda";
$sub5_file = "events";
$sub6_file = "social";

$default_template = '/display.lte';
$project= "Product proces";

/* 2 start initialize module */
global $oLEPTON;
$oFC = gsmoffm::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* 3 version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* 4 file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = LOAD_DBBASE . "_" .$sub2_file;
$oFC->file_ref [ 96 ] = LOAD_DBBASE . "_" .$sub3_file;
$oFC->file_ref [ 95 ] = LOAD_DBBASE . "_" .$sub4_file;
$oFC->file_ref [ 94 ] = LOAD_DBBASE . "_" .$sub5_file;
$oFC->file_ref [ 93 ] = LOAD_DBBASE . "_" .$sub6_file;

/* 5 settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "room" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "toegang" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "swijk" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "sgroep" );

/* 6 other default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* 8 Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* 10 create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* 11 saved values */ 
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC , $selection ), __LINE__ . $module_name ); 

//if ( !isset ( $xmode ) ) $xmode = "";

/* 12 selection functions *
/
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "print" => "print" ) as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

/* 13 selection * /
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );

/* 14 Print function needed * /
if ( isset ( $xmode ) && strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_Mes_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );

/* 15 specific for initial setup to force install function*/
//if ( !isset ( $FC_SET [ 'SET_txt_menu' ] [ 'xsetupm' ] ) ) $xmode .= "INSTALIMAGEDETAILEXPL";

/* 19 sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* 20 some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		default:
			// escape route
			$oFC->page_content [ 'MODE' ] = 9;
			 break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
	default:
		// escape route 
		$oFC->page_content [ 'MODE' ] = 9;
		break;
	} 
} else {
	/* initial run */
	$oFC->page_content [ 'P1' ] = true;
	
	/* Default settings */
	$job = array ();
	
	foreach ( $oFC->file_ref as $LocalHulp ) {
		$payload = $place[ 'includes'] . str_replace ( TABLE_PREFIX, "setup_", $LocalHulp ) . ".php";
		if ( file_exists ( $payload ) ) { 
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade checked ' . $LocalHulp .  NL; 
			require_once ( $payload ); 
		} else {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' No Upgrade function ' . $LocalHulp .  NL; 
		}
	}

	
	/* 31A create databases  1 * /
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function started ' . $oFC->file_ref [ 99 ] .  NL; 
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 99 ] );
	/* which fields are present in the main file * /	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 99 ] ),
		true, 
		$result );
	if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 99 ] );
	
	/* 31B_add /change fields not present  * /
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

	/* 31C wijzigen * /
	if ( isset ( $localHulpA [ 'reference' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", 
			$oFC->file_ref [ 99 ] );
		$localHulpA[ 'content_short' ] = true;
	}
	
	/* 31D toevoegen * /
	if ( !isset ( $localHulpA['amt5' ] ) ) {  // voor voorraad admin
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt5` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}
	if ( !isset ( $localHulpA['amt4' ] ) ) {  // voor het statiegeld bedrag
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt4` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}
	if ( !isset ( $localHulpA['amt3' ] ) ) {  //  voor de costprijs ex btw
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt3` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}
	if ( !isset ( $localHulpA['amt2' ] ) ) {  // onze prijs / verhuurprijs  ex btw
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt2` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}
	if ( !isset ( $localHulpA['amt1' ] ) ) {  // voor de advies verkoopprijs incl btw 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt1` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}
	if ( !isset ( $localHulpA['amt0' ] ) ) {  // voor het btw percentage
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt0` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 99 ] );}	
	if ( !isset ( $localHulpA['content_long' ] ) ) {  // de lange tekst
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(2000) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 99 ] );}

	/* 31E achtual upgrade * /
	if ( isset ( $job ) && count( $job ) > 0 ) {
		foreach( $job as $key => $query ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query );
	}	}
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 99 ] .  NL; 
	$job = array ();
	/* upgraded */
	
	/* 32 create databases  2 * /
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function started ' . $oFC->file_ref [ 98 ] .  NL; 
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 98 ] );

	/* 32A check which fields are present in the table * /	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 98 ] ),
		true, 
		$result );
	if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 98 ] ); 

	/* 32B add /change fields not present  * /
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

	/* 32C wijzigen * /
	if ( isset ( $localHulpA [ 'reference' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", 
			$oFC->file_ref [ 98 ] );
		$localHulpA[ 'content_short' ] = true;
	}

	/* 32D toevoegen * /	
	if ( !isset ( $localHulpA [ 'amt1' ] ) ) {
		// add one amount field for vat %
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt1` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", 
			$oFC->file_ref [ 98 ] );
	}	

	/* 32E achtual upgrade * /
	if ( isset ( $job ) && count( $job ) > 0 ) {
		foreach( $job as $key => $query ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query ); 
	}	}
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 98 ] .  NL; 
	$job = array ();
	/* upgraded */

	/* 33 create databases  3 * /
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function started ' . $oFC->file_ref [ 97 ] .  NL; 
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 97 ] );

	/* 33A check which fields are present in the table * /	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 97 ] ),
		true, 
		$result );
	if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 97 ] );

	/* 33B add /change fields not present  * /
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];
	
	/* 33C wijzigen * /
	if ( isset ( $localHulpA [ 'reference' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", 
			$oFC->file_ref [ 97 ] );
		$localHulpA[ 'content_short' ] = true;
	}
	
	/* 33D toevoegen * /
	if ( !isset ( $localHulpA [ 'amt3' ] ) ) {  //  amount3
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt3` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'amt2' ] ) ) {  // amount2
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt2` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'amt1' ] ) ) {  // amount 1 
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amt1` decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'email' ] ) ) {  // email
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `email` varchar(255) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'payload' ] ) ) {  // payload
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `payload` text AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'content_long' ] ) ) {  // payload
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` text AFTER `content_short`", $oFC->file_ref [ 97 ] );}
	if ( !isset ( $localHulpA [ 'voortgang' ] ) ) {  // voortgang
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `voortgang` int(11) NOT NULL DEFAULT '0' AFTER `content_short`", $oFC->file_ref [ 97 ] );}

	/* 33E achtual upgrade * /
	if ( isset ( $job ) && count( $job ) > 0 ) {
		foreach( $job as $key => $query ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query );
	}	}
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 97 ] .  NL; 
	$job = array ();
	/* upgraded */
	
	/* Default settings * /
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
	if ( !isset ( $oFC->setting [ 'zoek' ] [ $main_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			$main_file, '|type|ref|name|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $main_file . NL;
	}
	
	if ( !isset ( $oFC->setting [ 'zoek' ] [ $sub_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			$sub_file, '|type|ref|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $sub_file . NL;
	}

	if ( !isset ( $oFC->setting [ 'zoek' ] [ $sub2_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			$sub2_file, '|type|name|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $sub2_file . NL;
	}
	
	if ( !isset ( $oFC->setting [ 'datadir' ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			'datadir|gsmoffm', '/media/kassa');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'datadir' . NL;
	}

	if ( !isset ( $oFC->setting [ 'mediadir'  ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			'mediadir|gsmoffm', '/media/product' );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
	}

/*
	if ( !isset ( $oFC->setting [ 'eventdir'  ] ) ) {
		$job [] = sprintf ( "INSERT INTO `lep_mod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			'eventdir|gsmoffm', '/media/events' );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'eventdir' . NL;
	}

	/* 34 achtual upgrade * /
	if ( isset ( $job ) && count( $job ) > 0 ) {
		foreach( $job as $key => $query ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query ); 
		} 
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
	}
	
	/* upgraded */
	$job = array ();
	
		/* directory creation */
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Directory creation function started ' .  NL; 
	$oFC->gsm_existDir ( sprintf ( '%s%s/%s/pdf/' ,LEPTON_PATH , MEDIA_DIRECTORY , LOAD_MODULE) );
	$oFC->gsm_existDir ( sprintf ( '%s%s/%s/backup/' ,LEPTON_PATH , MEDIA_DIRECTORY , LOAD_MODULE) );
	$oFC->gsm_existDir ( sprintf ( '%s%s' ,LEPTON_PATH , MEDIA_DIRECTORY , $oFC->setting [ 'datadir'  ]) );	
	$oFC->gsm_existDir ( sprintf ( '%s%s' ,LEPTON_PATH , MEDIA_DIRECTORY , $oFC->setting [ 'mediadir'  ]) );			
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Directory creation function completed' .  NL; 
	
}
/* end which job to do */

/* 38 additional functions */
if ( isset ( $oFC->setting [ 'pdf_filename' ] )  && strlen( $oFC->setting [ 'pdf_filename' ] ) > 5 ) {
	$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 1 );
} else {
	/* 39 Additional functions */
	if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' );
}
/* debug * / Gsm_debug (array ("this"=>$oFC  ), __LINE__ . __FUNCTION__ ); /* debug */ 


/* 40 display preparation */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
		$oFC->page_content [ 'aantal' ] = 0;
		break;
	default:
		break;	
}

/* 97 end actions */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
	default: 
		$oFC->page_content  [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 10 ), "-", "-" ,"0", "-", "-" , "printrefall" );
		$oFC->page_content  [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6, 11) , '-', $pdflink  ?? "-"  );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'position' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		break;
}

/* 98 memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 2 ); 
	
/* 99 output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?> 