<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print1'] = "20240609";	

/* preset input */
$title = ucfirst ( str_replace ( "_", " ", str_replace ( "onderhoud", "overzicht", $project ) ) );
$keuze = strtolower ( $query ) ;
$search_mysql = "";
$prtype = 1;
$prmode = 1;
$subtitel = "Overzicht..";

/* welke data */
if ( strstr ( $selection, "print" ) ) {
	$prtype = 1; // default  active 1 of 2 zoek op zoek
	$prmode = 1; // default
	$subtitel = "Algemeen overzicht..";
}
if ( strstr ( $selection, "all" ) )   {	
	$prtype = 2; // details active 1 of 2 zoek op ref en wijk en active 0 ok  
	$prmode = 2; // detail gegevens
	$subtitel = "Detailed Wijk overzicht..";
}
if ( strstr ( $selection, "debug" ) )   {	
	$prtype = 3; // debug material
	$prmode = 3; // debug gegevens
	$subtitel = "Detailed overzicht..";
}
if ( strstr ( $selection, "extra" ) )   {	
	$prtype = 4; // debug material
	$prmode = 4; // debug gegevens
	$subtitel = "Detailed overzicht..";
}


if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( 
	"naam overzicht"  => $title,
	"file_name" => $this->setting [ 'pdf_filename' ],
	"func" => $func,	
	"loc" => $loc,	
	"keuze" => $keuze,
	"project" => $project,
	"titel" => $title,
	"subtitel" => $subtitel,
	"selection" => $selection, 
	"functie mode" => $prmode,
	"functie mode" => $prtype,	
	"owner" => $owner,	
	"run" => $run ), __LINE__ . 'print1' .$this->version ['print1'] ); /* einde debug */

if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( array ( "this" => $this ), __LINE__ . 'print1' .$this->version ['print1'] ); 

$j = 0;
$pdf_text = '';
/* initialise page  */
$pdf->AliasNbPages();
$pdf->AddPage();
$j++;
$pdf->ChapterTitle( $j, $subtitel );
$pdf_cols = array( 20, 50, 120, 0, 0, 0 );
$pdf_header = array( '', '', '', '', '', '' );
$pdf_data_2nd = array ( );
$pdf_data = array ( );
$pdf_text = '';

/* evaluate sql */

/* selectie op action */
if ( in_array ( $prtype, array ( 1 ) ) ) {
	if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`active` > '0' ", 
			$search_mysql, $this->file_ref [ 99 ] ); 
	} else { 
		$search_mysql = sprintf ( " WHERE `%s`.`active` > '0' ", 
			$this->file_ref [ 99 ] ); 
	}
	if ( strlen ( trim($keuze ) ) > 2 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`zoek` = '%s' ", $search_mysql, $this->file_ref [ 99 ] ,	"%". $keuze ."%" );
	}
}

if ( in_array ( $prtype, array ( 2 ) ) ) {
	if ( strlen ( trim($keuze )) > 2 ) {
		if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
			$search_mysql = sprintf ( "%s AND `%s`.`active` < '5' ", 
				$search_mysql, 
				$this->file_ref [ 99 ] ); 

		} else { 
			$search_mysql = sprintf ( " WHERE `%s`.`active` < '5' ", $this->file_ref [ 99 ] ); 
		}
		$search_mysql = sprintf ( "%s AND ( `%s`.`ref` = 'all' OR `%s`.`ref` LIKE '%s' OR `%s`.`ref` LIKE '%s' )", 
			$search_mysql, 
			$this->file_ref [ 99 ], 
			$this->file_ref [ 99 ],
			"%". $keuze ."%",
			"%". $keuze ."%");
	} else {
		if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
			$search_mysql = sprintf ( "%s AND `%s`.`active` > '0' ", 
				$search_mysql, 
				$this->file_ref [ 99 ] ); 
		} else { 
			$search_mysql = sprintf ( " WHERE `%s`.`active` > '0' ", 
				$this->file_ref [ 99 ] ); 
		}
		$search_mysql = sprintf ( "%s AND `%s`.`ref` = 'all' ", 
			$search_mysql, 
			$this->file_ref [ 99 ] );
	}
}

if ( in_array ( $prtype, array ( 3 ) ) ) {
	if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`active` < '5' ", 
			$search_mysql, 
			$this->file_ref [ 99 ] ); 
	} else { 
		$search_mysql = sprintf ( " WHERE `%s`.`active` < '5' ", 
			$this->file_ref [ 99 ] ); 
	}
	if ( strlen ( trim($keuze ) ) > 2 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`zoek` = '%s' ", $search_mysql, $this->file_ref [ 99 ] ,	"%". $keuze ."%" );
	}
}
if ( in_array ( $prtype, array ( 4 ) ) ) {
	if ( isset ( $search_mysql ) && strlen ( $search_mysql ) > 15 ) {
		$search_mysql = sprintf ( "%s AND `%s`.`active` > '1' ", 
			$search_mysql, 
			$this->file_ref [ 99 ] ); 
	} else { 
		$search_mysql = sprintf ( " WHERE `%s`.`active` > '1' ", 
			$this->file_ref [ 99 ] ); 
	}
	if ( strlen ( trim($keuze ) ) > 2 ) {
		$search_mysql = sprintf ( "%s AND ( `%s`.`ref` = 'all' OR `%s`.`ref` LIKE '%s' OR `%s`.`ref` LIKE '%s' )", 
			$search_mysql, 
			$this->file_ref [ 99 ], 
			$this->file_ref [ 99 ],
			"%". $keuze ."%",
			"%". $keuze ."%");
	}
}

/* singel file */
$query  = "SELECT * FROM `" . $this->file_ref [ 99 ] . "` ";
$query .= sprintf ( "%s ORDER BY `type`, `zoek` ASC ", $search_mysql );

if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $query , __LINE__ . 'print1' .$this->version ['print1'] );
if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $prtype, __LINE__ . 'print1' .$prtype );
if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $prmode, __LINE__ . 'print1' .$prmode );

if ( in_array ( $prtype, array ( 1, 2, 3, 4 ) ) ) {
	if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $query , __LINE__ . 'print1' .$this->version ['print1'] );

	/* query for lijst */
	$results = array();
	$database->execute_query( 
		$query, 
		true, 
		$results);
	$PL3 = "=="; 
	
	if ( $this->setting [ 'debug' ] == "yes" ) gsm_debug ( $results , __LINE__ . 'print1' .$this->version ['print1'] ); 
	
	if ( count ( $results ) > 0 ) { 
		foreach ( $results as  $result ) {
			if ( $PL3 != $result [ 'type' ] ) {
				if ($PL3 != "==" ) $pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', '', '', '') ) );
				$PL3 = $result [ 'type' ] ;
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					"", 
					$result [ 'type' ], 
					"", 
					'', '',	'') ) );
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					$result [ 'id' ], 
					$result [ 'ref' ], 
					$this->Gsm_truncate ($result [ 'name' ], 65 , '...', false ),
					'', '',	'') ) );
				if ( strlen ( $result [ 'content_tel' ]) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_tel' ], 
						'', '',	'') ) );
				}
				if ( strlen (	$result [ 'content_email' ] ) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"", 
						$result [ 'content_email' ],
						'', '',	'') ) );
				}
				if ( strlen (	$result [ 'content_url' ] ) > 8 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_url' ],
						'', '',	'') ) );
				}
				if ( strlen ( $result [ 'content_adres' ]) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_adres' ], 
						'', '',	'') ) );
				}
			} else {
				$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					$result [ 'id' ], 
					$result [ 'ref' ],
					$this->Gsm_truncate ($result [ 'name' ], 65 , '...', false ),
					'', '',	'') ) );
				if ( strlen ( $result [ 'content_tel' ]) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_tel' ], 
						'', '',	'') ) );
				}
				if ( strlen (	$result [ 'content_email' ] ) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"", 
						$result [ 'content_email' ],
						'', '',	'') ) );
				}
				if ( strlen (	$result [ 'content_url' ] ) > 8 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_url' ],
						'', '',	'') ) );
				}
				if ( strlen ( $result [ 'content_adres' ]) > 2 ) { 
					$pdf_data [ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						"", 
						"",
						$result [ 'content_adres' ], 
						'', '',	'') ) );
				}
			}	
			$unset_fields1 = array ( 'zoek', 'updated'); 
			$unset_fields2 = array ( 'zoek', 'updated'); 
			$unset_fields3 = array ( ); 
			$unset_fields4 = array ( 'zoek', 'updated'); 

			if ( in_array ( $prmode, array ( 1 ) ) ) {
				foreach ( $unset_fields1 as $each )  unset ( $result [ $each ] );
				if ( $result [ 'ref' ] == 'all') unset ( $result [ 'content wijk' ] );
			}
			if ( in_array ( $prmode, array ( 2 ) ) ) {
				foreach ( $unset_fields2 as $each )  unset ( $result [ $each ] );
				if ($result [ 'ref' ] == 'all') unset ( $result [ 'content wijk' ] );
				foreach ( $result as $pay => $load ) {
					$localHulp = strip_tags( html_entity_decode( $load ) );
					if ( trim ( $localHulp ) != "" ) {
						$n1= true;
						do {
							$printdeel = $this->Gsm_truncate ($localHulp, 65 , '', false );
							$localHulp = substr ( $localHulp, strlen ( $printdeel ));
							$n2 = strlen ( $localHulp );
							$pdf_data_2nd [ ] = explode( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
								($pay == "id") ? $load : "", 
								($n1) ? $pay : "", 
								($pay == "id") ? "" : $printdeel, 
								'', '', '') ) );
								$n1 = false;
						} while ( $n2 > 0  );
					}
				}
			}
			if ( in_array ( $prmode, array ( 3 ) ) ) {
				foreach ( $unset_fields3 as $each )  unset ( $result [ $each ] );
				foreach ( $result as $pay => $load ) {
					$localHulp = strip_tags( html_entity_decode( $load ) );
					if ( trim ( $localHulp ) != "" ) {
						$n1= true;
						do {
							$printdeel = $this->Gsm_truncate ($localHulp, 65 , '', false );
							$localHulp = substr ( $localHulp, strlen ( $printdeel ));
							$n2 = strlen ( $localHulp );
							$pdf_data_2nd [ ] = explode( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s", 
								($pay == "id") ? $load : "", 
								($n1) ? $pay : "", 
								($pay == "id") ? "" : $printdeel, 
								'', '', '') ) );
								$n1 = false;
						} while ( $n2 > 0  );
					}
				}
			}
			if ( in_array ( $prmode, array ( 4 ) ) ) {
				foreach ( $unset_fields4 as $each )  unset ( $result [ $each ] );
				if ($result [ 'ref' ] == 'all') unset ( $result [ 'content wijk' ] );
				foreach ( $result as $pay => $load ) {
					$localHulp = strip_tags( html_entity_decode( $load ) );
					if ( trim ( $localHulp ) != "" ) {
						$n1= true;
						do {
							$printdeel = $this->Gsm_truncate ($localHulp, 65 , '', false );
							$localHulp = substr ( $localHulp, strlen ( $printdeel ));
							$n2 = strlen ( $localHulp );
							$pdf_data_2nd [ ] = explode( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s", 
								($pay == "id") ? $load : "", 
								($n1) ? $pay : "", 
								($pay == "id") ? "" : $printdeel, 
								'', '', '') ) );
								$n1 = false;
						} while ( $n2 > 0  );
					}
				}
			}
		}	
	}
	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data = array( );
	if ( in_array ( $prtype, array ( 2, 3 ) ) ) {
		$pdf->AddPage();
		$j++;
		$subtitel = "Details";
		$pdf->ChapterTitle( $j, $subtitel );
		$pdf_cols = array( 20, 50, 120, 0, 0, 0 );
		$pdf_header = array( '', '', '', '', '', '' );
		$pdf->DataTable( $pdf_header, $pdf_data_2nd, $pdf_cols );
		$pdf_data_2nd = array( );
	}
	$pdf_data = array( );
}

$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
if ( strlen ( $selection ) > 1 ) $pdf_text .= sprintf ( "\n" . "Options %s : %s " , $project, $selection );

if ( $this->setting [ 'debug' ] == "yes" ){
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}

// pdf output
$pdf->ChapterBody( $pdf_text );

?>
