<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print5'] = "20240816";


/* welke overzichten */
$prmode = array (); 
$printOK = false;
$partOK = true;
if ( strstr ( $selection, "print1" ) ) 	$prmode [ ] = 1; /* nog niet ondersteund */
if ( strstr ( $selection, "print2" ) ) 	$prmode [ ] = 2; /* nog niet ondersteund */
if ( strstr ( $selection, "print3" ) ) 	$prmode [ ] = 3; /* nog niet ondersteund */
if ( strstr ( $selection, "part1" ) ) 	$prmode [ ] = 5; 
if ( strstr ( $selection, "part2" ) ) 	$prmode [ ] = 6; 
if ( strstr ( $selection, "part3" ) ) 	$prmode [ ] = 7; 
if ( strstr ( $selection, "part4" ) ) 	$prmode [ ] = 8;
if ( strstr ( $selection, "part5" ) ) 	$prmode [ ] = 9;
if ( strstr ( $selection, "all" ) )		$prmode = array ( 0 ); /* alles */
if ( count ( $prmode ) < 1) $prmode = array ( 1 ); 

// $this->setting [ 'debug' ] = "yes";
// input
if ($this->setting [ 'debug' ] == "yes") gsm_debug (array (
	'query' => $query,
	'project' => $project,
	'selection' => $selection,
	'func' => $func,
	'run' => $run,
	'this' => $this ), __LINE__ . __FUNCTION__ ); 
	
$title = ucfirst ( $project );
$regelcount  = 0;
$chaptercount = 0; //amount of chapters
$pdf_data   = array( );
$pdf_text   = '';

/* processing */
$sql1 = "SELECT `" . $this->file_ref [ 99 ] . "`.* FROM `" . $this->file_ref [ 99 ] . "`";

if ( strlen ( $query ) > 1 ) {
	$zoekstring = "%". $query . "%";
	$sqla = sprintf ( " WHERE (`%s`.`active`='1' OR  `%s`.`active`='2') AND `%s`.`ref` LIKE '%s' ORDER BY `%s`.`type`, `%s`.`ref`", 
		$this->file_ref [ 99], 
		$this->file_ref [ 99], 
		$this->file_ref [ 99], 
		$zoekstring, 
		$this->file_ref [ 99],
		$this->file_ref [ 99]);
	$sql1 .= $sqla; 
} else {
	$sqla = sprintf ( " WHERE `%s`.`active`='1' OR  `%s`.`active`='2' ORDER BY `%s`.`type`, `%s`.`ref`", 
		$this->file_ref [ 99], 
		$this->file_ref [ 99], 
		$this->file_ref [ 99],
		$this->file_ref [ 99]);
	$sql1 .= $sqla; 
}

if ($this->setting [ 'debug' ] == "yes") gsm_debug (array (	'sql' => $sql1 ), __LINE__ . __FUNCTION__ ); 
	
$results = array();
$database->execute_query( 
	$sql1, 
	true, 
	$results);
$LocalHulp = count ($results);

if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ( 'sql' => $sql1, 'results' => $results ), __LINE__ . __FUNCTION__ ); 

/* this function works with label layout  */
$printOK = false;
/* end what is supported 
$ledenAryOk = array ();
$ledenAryNok = array ();
if ( $LocalHulp >0 ) $printOK = true;
*/


if ( $partOK ) {
	if ( in_array ( 0, $prmode ) ) {
		$i = 0;
		do {
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				sprintf ( "regel %s veld %s", $i, 1),
				sprintf ( "regel %s veld %s", $i, 2),
				sprintf ( "regel %s veld %s", $i, 3),
				sprintf ( "regel %s veld %s", $i, 4),
				sprintf ( "regel %s veld %s", $i, 5) ) ) );
			$i++;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelBlock ( $pdf_data );
				$pdf_data = array( );
			}
		} while ( $i < 60);
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelBlock ( $pdf_data );
			$pdf_data = array( );
		}
		$i = 0;
		do {
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				sprintf ( "regel %s veld %s ", $i, 1),
				sprintf ( "regel %s veld %s", $i, 2),
				sprintf ( "regel %s veld %s", $i, 3),
				sprintf ( "regel %s veld %s", $i, 4),
				sprintf ( "regel %s veld %s", $i, 5) ) ) );
			$i=$i+2;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelAll ( $pdf_data );
				$pdf_data = array( );
			}
		} while ( $i < 24);
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelAll ( $pdf_data );
			$pdf_data = array( );
		}
	}
}
if ( $partOK ) {	
	if ( in_array ( 5, $prmode ) ) {
		$i = 0;
		foreach ( $results as $row ) {
			/*skip  */
			/* opmaak naam */
			$row ['short'] = $this->gsm_sanitizeStrings( $row [ 'content_short' ], "s{STRIP|TOASC|CLEAN}");
			/* opmaak adres */
			$link = strlen ( $row ['content_tel'] ) < 3 
				? $row ['content_url']  
				: $row ['content_tel'];
			/*  output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				substr ( $row ['type'],0,30),
				substr ( $row ['name'],0,30),
				substr ( $link,0,30),
				substr ( $row ['short'],0,30),
				sprintf ("%s %s, %s, %s", $row ['id'], $row ['ref'], $row ['content_wijk'], $row ['content_groep'] ) ) ) );
			$i++;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelVeel ( $pdf_data );
				$pdf_data = array( );
			}
		} 
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelVeel ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

?>
